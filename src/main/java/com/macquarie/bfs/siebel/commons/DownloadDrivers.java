package com.macquarie.bfs.siebel.commons;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by vthaduri on 10/02/2017.
 */
public class DownloadDrivers
{
    private WebDriver webDriver;

    private String browser = "";
    static Logger logger = LoggerFactory.getLogger(DownloadDrivers.class);

    /* FUNCTION FOR DOWNLOADING OS & BROWSER SPECIFIC DRIVERS */
    private String downloadDrivers(String browser)
    {
        URL url = null;
        String driver = "";
        File driverZipFile = null;
        File driverFile = null;
        this.browser = browser;
        String project = System.getProperty("user.dir") + "\\";
        try
        {
            if(browser.equals("chrome"))
            {
                String osName = System.getProperty("os.name");
                String osDriver = "";
                if(osName.toLowerCase().contains("windows")) {
                    osDriver = "https://chromedriver.storage.googleapis.com/2.27/chromedriver_win32.zip";
                    driver = "chromedriver.exe";
                }else if(osName.contains("linux")) {
                    osDriver = "https://chromedriver.storage.googleapis.com/2.27/chromedriver_linux64.zip";
                    driver = "chromedriver";
                }
                url = new URL(osDriver);
                driverZipFile = new File("chromedriver.zip");
                driverFile = new File(project + driver);
            }
            else if (browser.equals("ie"))
            {
                url = new URL("https://download.microsoft.com/download/1/4/1/14156DA0-D40F-460A-B14D-1B264CA081A5/MicrosoftWebDriver.exe");
                driver = "IEDriverServer.exe";
                driverFile = new File(project + driver);
                driverZipFile = driverFile;
            }
            if(driverFile.exists())
            {
                logger.info("Driver file already exists on the local machine");
                return driverFile.getAbsolutePath();
            }

            /*DOWNLOADING DRIVER SPECIFIC TO BROWSER*/

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream;
            if(httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
                inputStream = httpURLConnection.getInputStream();
            else
                inputStream = httpURLConnection.getErrorStream();

            OutputStream outputStream = new FileOutputStream(driverZipFile);
            BufferedReader bufferedReader;
            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, bytesRead);
            }

            logger.info("File downloaded: " + driverZipFile.getAbsolutePath());
            outputStream.close();
            inputStream.close();

            /**UNZIP FILE INTO DESIRED LOCATION**/
             if(browser.equalsIgnoreCase("chrome")) {
                 ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(driverZipFile.getAbsolutePath()));
                 ZipEntry zipEntry = zipInputStream.getNextEntry();
                 while (zipEntry != null) {
                     String fileName = zipEntry.getName();
                     File newFile = new File(project + File.separator + fileName);
                     logger.info("Unzipping driver folder : " + newFile.getAbsoluteFile());
                     new File(newFile.getParent()).mkdirs();
                     FileOutputStream fos = new FileOutputStream(newFile);
                     int length;
                     while ((length = zipInputStream.read(buffer)) > 0) {
                         fos.write(buffer, 0, length);
                     }
                     fos.close();
                     zipEntry = zipInputStream.getNextEntry();
                 }
                 zipInputStream.closeEntry();
                 zipInputStream.close();
                 logger.info("Finished downloading of drivers");
                 driverZipFile.delete();
             }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return driverFile.getAbsolutePath();
    }

    /* Initialize chrome web driver instance */
    private WebDriver initChromeDriver() {
        logger.info("Launching Google Chrome with new profile..");
        System.setProperty("webdriver.chrome.driver", downloadDrivers("chrome"));
        ChromeOptions options = new ChromeOptions();
        //options.setExperimentalOption("excludeSwitches", Arrays.asList("enable-automation"));
        options.addArguments("--disable-extensions");
        options.addArguments("--start-maximized");
        options.addArguments("--test-type");
        //options.addArguments("user-data-dir=C:\\Selenium\\BrowserProfile");

        DesiredCapabilities desiredCapabilities = new DesiredCapabilities().chrome();

        options.addArguments("--disable-infobars");
        desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
        desiredCapabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        webDriver = new ChromeDriver(desiredCapabilities);
        //webDriver.manage().window().maximize();
        return webDriver;
    }

    /* Initialize Internet Explorer web driver instance */
    private WebDriver initIEDriver() {
        logger.info("Launching Internet Explorer...");
        //WebDriver driver = null;
                // Set the capabilities of Internet explorer
        try {
        DesiredCapabilities desiredCapabilities = DesiredCapabilities.internetExplorer();
        desiredCapabilities.setCapability("requireWindowFocus", true);
        desiredCapabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, false);
        desiredCapabilities.setCapability("ie.ensureCleanSession", false);
        desiredCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        desiredCapabilities.setCapability("initialBrowserUrl", "https://ntsydwbu117.pc.internal.macquarie.com:444/finsOUI_enu");//desiredCapabilities.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
        //desiredCapabilities.setCapability("ie.browserCommandLineSwitches", true);
        //desiredCapabilities.setCapability("ie.usePerProcessProxy", true);
        //desiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        //desiredCapabilities.setCapability("ignoreProtectedModeSettings",true);

        System.setProperty("webdriver.ie.driver", downloadDrivers("ie"));
        webDriver = new InternetExplorerDriver(desiredCapabilities);
        if(webDriver == null){
            logger.info("**** IE driver failed to launch ****");
        }

        }catch (Exception e){
            e.getMessage();
            e.printStackTrace();
        }
        return webDriver;
    }
    /* Return web driver instance */
    public WebDriver getDriver(String browser)
    {
        switch (browser)
        {
            case "chrome":
                this.webDriver = initChromeDriver();
                break;
            case "ie":
                this.webDriver = initIEDriver();
                break;
            default:
                this.webDriver = initChromeDriver();
        }
        return this.webDriver;
    }

}
