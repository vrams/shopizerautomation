package com.macquarie.bfs.siebel.commons;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by vthaduri on 17/10/2017.
 */
public class WindowDriver {

    private WebDriver windowDriver;
    private WiniumDriver winiumDriver = null;

    public WiniumDriver getWiniumDriver()
    {
        DesktopOptions desktopOptions = new DesktopOptions();
        desktopOptions.setApplicationPath("C:\\Program Files (x86)\\BSPIVOT_UAT\\Execute.vbs");

        try {
            winiumDriver = new WiniumDriver(new URL("http://localhost:9999"), desktopOptions);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return winiumDriver;
    }

}
