package com.macquarie.bfs.siebel.commons;


/**
 * Created by vthaduri on 23/01/2017.
 */
public class SiebelException extends Exception
{
    public SiebelException(String message)
    {
        super(message);
    }
}
