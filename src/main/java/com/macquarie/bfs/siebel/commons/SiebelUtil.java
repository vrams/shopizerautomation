package com.macquarie.bfs.siebel.commons;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * Created by rkurian on 3/08/2017.
 */
public class SiebelUtil {

    public static String generateRandomText(String prefix, int txtLength){

        Random random = new Random();
        String addStr = "";
        StringBuilder builder = new StringBuilder();
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        while (builder.length() < txtLength) {
            int index = (int) (random.nextFloat() * chars.length());
            builder.append(chars.charAt(index));
        }
        if(prefix == null){
            addStr =  builder.toString();
        }else {
            addStr = prefix + "_" + builder.toString();
        }
        return addStr;
    }

    public static String generateRandomText(int txtLength) {
        Random random = new Random();
        String addStr = "";
        StringBuilder builder = new StringBuilder();
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        while (builder.length() < txtLength) {
            int index = (int) (random.nextFloat() * chars.length());
            builder.append(chars.charAt(index));
        }
        addStr = builder.toString();
        return addStr;
    }

    public static String generateRandomNumber(int maxRange) {
        Random random = new Random();
        int num = random.nextInt(maxRange);
        return String.valueOf(num);
    }

    public static String generateRandonDOB() {
        GregorianCalendar gc = new GregorianCalendar();
        String dayOfMonth = null;
        int year = randBetween(1940, 1990);
        gc.set(gc.YEAR, year);
        int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
        gc.set(gc.DAY_OF_YEAR, dayOfYear);
        int day = gc.get(gc.DAY_OF_MONTH);
        String dob = gc.get(gc.DAY_OF_MONTH)+"/"+(gc.get(gc.MONTH) + 1)+"/"+gc.get(gc.YEAR);
        return dob;
    }

    private static  int randBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public static String generatePhoneNumber() {
        Random rand = new Random();
        int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(743);
        int num3 = rand.nextInt(10000);

        DecimalFormat df3 = new DecimalFormat("000");
        DecimalFormat df4 = new DecimalFormat("0000");

        return df4.format(num1) + " " + df3.format(num2) + " " + df3.format(num3);
    }

    public static String generateEmail() {
        Random r = new Random();
        return Long.toString(Math.abs(r.nextLong()), 36) + "@macquarie.com";
    }

    public static String generateRandomNumbers(){
        Random rnd = new Random();
        int number = 1000000 + rnd.nextInt(9999999);
         return "+6146" +String.valueOf(number);
    }

    public static String formatDate(String dateValue, String dateFormat, String expectedFormat) {
        Date date = null;
        SimpleDateFormat formatter = null;
        String formattedDate = "";
        try {
            formatter = new SimpleDateFormat(dateFormat);
            date = formatter.parse(dateValue);
            formatter = new SimpleDateFormat(expectedFormat);
            formattedDate = formatter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

}
