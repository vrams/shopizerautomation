package com.macquarie.bfs.siebel.commons;

import org.apache.commons.ssl.rmi.Test;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jumpmind.symmetric.csv.CsvWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vthaduri on 23/01/2017.
 */
public class TestDataHelper {
    //public static HashMap<String, String> storeValues = new HashMap();

    public static List<HashMap<String, String>> getExcelTestData(String filepath, String sheetName) {
        List<HashMap<String, String>> mydata = new ArrayList<>();
        try {
            FileInputStream fileInputStream = new FileInputStream(filepath);
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
            XSSFSheet xssfSheet = xssfWorkbook.getSheet(sheetName);
            Row HeaderRow = xssfSheet.getRow(0);
            for (int i = 1; i < xssfSheet.getPhysicalNumberOfRows(); i++) {
                Row currentRow = xssfSheet.getRow(i);
                HashMap<String, String> currentHash = new HashMap<String, String>();
                for (int j = 0; j < currentRow.getPhysicalNumberOfCells(); j++) {
                    Cell currentCell = currentRow.getCell(j);
                    switch (currentCell.getCellType()) {
                        case Cell.CELL_TYPE_STRING:
                            currentHash.put(HeaderRow.getCell(j).getStringCellValue(), currentCell.getStringCellValue());
                            break;
                    }
                }
                mydata.add(currentHash);
            }
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mydata;
    }
    public static  List<HashMap<String, String>> getDBTestData(ResultSet resultSet)
    {
        List<HashMap<String, String>> mydata = new ArrayList<>();
        try
        {
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

            while (resultSet.next())
            {
                HashMap<String, String> currentHash = new HashMap<String, String>();
                for (int i = 1; i <= resultSetMetaData.getColumnCount();i++)
                {
                    currentHash.put(resultSetMetaData.getColumnName(i),resultSet.getString(i));
                }
                mydata.add(currentHash);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return mydata;
    }

    public static void writeTestResults(String scenario, String partyId, String status, String exception)
    {
        URL location = TestDataHelper.class.getProtectionDomain().getCodeSource().getLocation();
        String path = location.getFile().replace("classes/","") + "TestResults.csv";
        boolean alreadyExists = new File(path).exists();
        try {
            // use FileWriter constructor that specifies open for appending
            CsvWriter csvOutput = new CsvWriter(new FileWriter(path, true), ',');

            // if the file didn't already exist then we need to write out the header line
            if (!alreadyExists)
            {
                csvOutput.write("Scenario");
                csvOutput.write("PartyId");
                csvOutput.write("Status");
                csvOutput.write("Error Details");
                csvOutput.endRecord();
            }
            // else assume that the file already has the correct header line
            csvOutput.write(scenario);
            csvOutput.write(partyId);
            csvOutput.write(status);
            csvOutput.write(exception);
            csvOutput.endRecord();
            csvOutput.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
