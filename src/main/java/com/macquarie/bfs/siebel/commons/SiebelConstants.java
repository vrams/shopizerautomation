package com.macquarie.bfs.siebel.commons;

/**
 * Created by rkurian on 2/08/2017.
 */
public class SiebelConstants {

    public static final String TIMEOUT_SECONDS = "TimeOutInSeconds";
    public static final String TYPE = "Type";
    public static final String SUB_TYPE = "SubType";
    public static final String BUSINESS_TYPE = "BusinessType";
    public static final String STATUS = "Status";
    public static final String SECTOR = "Sector";
    public static final String SOURCE = "Source";
    public static final String INDUSTRY_CODES = "IndustryCodes";
    public static final String SALES_BU = "SalesBusinessUnit";
    public static final String REPORTING_GROUP_ID = "ReportingGroupId";
    public static final String INDUSTRY_CODES_VALUES = "IndustryCodeValues";
    public static final String ARBN = "ARBN";
    public static final String ABS_CODE = "ABSCode";
    public static final String ABN = "ABN";
    public static final String ACN = "ACN";
    public static final String INDUSTRY_SW = "IndustrySoftware";
    public static final String ACCOUNT_SW = "AccountingSoftware";
    public static final String BUSINESS_CODE = "BusinessCode";
    public static final String BUSINESS_SIZE = "BusinessSize";
    public static final String AB_CLASSIFICATION = "ABClassification";
    public static final String CUSTOMER_CATEGORY = "CustomerCategory";
    public static final String PIVOT_BU = "PivotBusinessUnit";
    public static final String LEGISLATION_TYPE = "LegislationType";
    public static final String LEGISLATION_LOC = "LegislationLoc";
    public static final String TRUST_TYPE = "TrustType";
    public static final String GROUP_TYPE = "GroupType";
    public static final String DEFT_CODE = "DEFTExtractCode";
    public static final String SERVICE_PLAN = "ServicePlan";
    public static final String CUSTOMER_OWNER =  "CustomerOwner";
    public static final String SHORT_NAME = "SHORT_NAME" ;
    public static final String LEGAL_NAME = "LEGAL_NAME" ;
    public static final String FIRST_NAME = "FIRST_NAME" ;
    public static final String LAST_NAME = "LAST_NAME" ;
    public static final String BUSINESS_UNIT = "BusinessUnit";
    public static final String MERGE_CONTACT = "MergeContact";
    public static final String IS_FACILITY =  "IsFacility";
    public static final String IS_ADDITIONAL_ACCOUNT = "AdditionalAccount";
}
