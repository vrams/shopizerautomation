package com.macquarie.bfs.siebel.commons;

import com.macquarie.bfs.siebel.pages.SiebelContactsPage;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.sourceforge.jtds.jdbc.*;
import sun.text.IntHashtable;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by rkurian on 29/08/2017.
 */
public class JDBCConnection {

    static Logger logger = LoggerFactory.getLogger(JDBCConnection.class);

    public  String userName = null;
    public  String password = null;
    public  String connectionURL = null;
    private Connection con = null;
    private Statement stmt = null;
    protected PropertiesConfiguration pc;

    private static Connection conn;

    public JDBCConnection(){

    }

    private String[] getDBCredentials(PropertiesConfiguration config){
        String[] credentails = new String[3];
        credentails[0] = config.getString("PivotTSB2DB");
        credentails[1] = config.getString("TSB2User");
        credentails[2] = config.getString("TSB2Password");
        return credentails;
    }

    public static  void main (String args[]) throws Exception{
        connect();
        System.out.println ("Got connected OK");
    }

    public static void connect()
            throws ClassNotFoundException, SQLException
    {
        DriverManager.registerDriver
                (new net.sourceforge.jtds.jdbc.Driver());

        String url = "jdbc:jtds:sybase://bfssyddev12:20003/pivot_ext_salmon";
        conn = DriverManager.getConnection(url,"bs_tst_install_dbo","bspasswd");

    }

    public static HashMap<String, String>  getContactDetailsFromPivot(String pivotId){
        String sql = null;
        HashMap<String, String> contactsMap = null;
        try {
            connect();
            contactsMap = new HashMap<String,String>();
             sql = "select type, shortnam, legal_name, sector_code, abs_code, bus_code, ab_classification, audit_when, audit_who, audit_action from portrait where cimnumb = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(pivotId));
            ResultSet res = stmt.executeQuery();
            while(res.next()) {
                contactsMap.put("SHORT_NAME" , res.getString(2) );
                contactsMap.put("LEGAL_NAME" , res.getString(3) );
            }

            sql = "select * from personal where cimnumb = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(pivotId));
            res = stmt.executeQuery();
            while(res.next()) {
                contactsMap.put("DOB" , res.getString(3) );
                contactsMap.put("FIRST_NAME" , res.getString(6) );
                contactsMap.put("LAST_NAME" , res.getString(7) );
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return contactsMap;
    }

    public static HashMap<String,String> getCompanyDetailsFromPivot(String pivotId) {
        String sql = null;
        HashMap<String, String> companiesMap = null;
        try {
            connect();
            companiesMap = new HashMap<String,String>();
            sql = "select type, shortnam, legal_name, sector_code, abs_code, bus_code, ab_classification, audit_when, audit_who, audit_action from portrait where cimnumb =  ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(pivotId));
            ResultSet res = stmt.executeQuery();
            while(res.next()) {
                companiesMap.put("SHORT_NAME" , res.getString(2) );
                companiesMap.put("LEGAL_NAME" , res.getString(3) );
            }
            sql = "select cimnumb, busname, bustype, acn, arbn, audit_when, audit_who, audit_action from business where cimnumb = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(pivotId));
            res = stmt.executeQuery();
            while(res.next()) {
                companiesMap.put("ACN" , res.getString(4) );
                companiesMap.put("ARBN" , res.getString(5) );
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companiesMap;
    }

    public static HashMap<String,String> getReportGrpDetailsFromPivot(String pivotId) {
        String sql = null;
        HashMap<String, String> reportGrpMap = null;
        try {
            connect();
            reportGrpMap = new HashMap<String,String>();
            sql = "select type, shortnam, legal_name, abs_code, bus_code, ab_classification, audit_when, audit_who, audit_action from portrait where cimnumb =  ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(pivotId));
            ResultSet res = stmt.executeQuery();
            while(res.next()) {
                reportGrpMap.put("SHORT_NAME" , res.getString(2) );

            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reportGrpMap;
    }

    public static List<String> getAddressNumbersFromPivot(String pivotId) {
        String sql = null;
        ArrayList<String> addressNumberList = null;
        try {
            connect();
            addressNumberList  = new ArrayList<>();
            sql = "select addr.addrinfo_numb, addr.addrnumb, addr.salutation, addr.addressee1, addr.addressee2 from addrinfo addr , portrait_address paddr  where addr.addrinfo_numb = paddr.addrinfo_numb\n" +
                    "and  paddr.cimnumb = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(pivotId));
            ResultSet res = stmt.executeQuery();
            while(res.next()) {
                addressNumberList.add(res.getString(2));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return addressNumberList;
    }

    public static String getAddressDetailsFromPivot(String addressNumber) {
        String sql = null;
        String address1 = null;
        try{
            connect();
            sql = "select * from addresses where addrnumb =  ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Integer.parseInt(addressNumber));
            ResultSet res = stmt.executeQuery();
            while(res.next()) {
                address1 = res.getString(3);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return  address1;
    }
}
