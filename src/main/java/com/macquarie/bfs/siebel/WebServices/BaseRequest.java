package com.macquarie.bfs.siebel.WebServices;


import com.macquarie.bfs.siebel.commons.JDBCConnection;
import com.macquarie.bfs.siebel.commons.SiebelException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vthaduri on 19/01/2017.
 */

public class BaseRequest {

	private static Logger logger = LoggerFactory.getLogger(BaseRequest.class);
	protected PropertiesConfiguration pc;
	protected String environment = "";
	protected String response = "";
	BaseRequest() {
		try {
			if(System.getProperty("environment") != null)
				environment = System.getProperty("environment");
			pc = new PropertiesConfiguration("objectRepository.properties");
			System.setProperty("soapui.https.protocols", "TLSv1,TLSv1.2,SSLv3");
			setJDBCConnection();
		    }catch (SiebelException se){
		       se.printStackTrace();
	    	} catch (Exception e) {
			logger.info("Unable to find Object Repository");
			e.printStackTrace();
		    }
	}

	private void setJDBCConnection() throws SiebelException {
		try {
			switch (environment.toUpperCase()) {
				case "DEV1":
/*					JDBCConnection.connectionURL = pc.getString("DEV1DB");
					JDBCConnection.userName = pc.getString("DEV1DBAuthSchema").split(":")[0];
					JDBCConnection.password = pc.getString("DEV1DBAuthSchema").split(":")[1];
					JDBCConnection.jdbcSchema = pc.getString("DEV1DBAuthSchema").split(":")[2];*/
					break;
				case "DEV2":
					break;
				case "TST1":
					break;
				case "TST2":
					break;
				case "SIT1":
					break;
				case "SIT2":
					break;
				case "IA2":
/*					JDBCConnection.connectionURL = pc.getString("PivotTSB2DB");
					JDBCConnection.userName = pc.getString("TSB2User");
					JDBCConnection.password = pc.getString("TSB2Password");*/
					//JDBCConnection.jdbcSchema = pc.getString("DEV1DBAuthSchema").split(":")[2];
					break;
				case "PREPROD":
					break;
				default:
					//JDBCConnection.connectionURL = null;
					logger.error("Invalid/No such environment");
			}
		}
		catch (Exception e)
		{
			throw new SiebelException("Invalid/Wrong JDBC Configuration in object repository for the Environment: " + environment);
		}
	}




	public String getWsseUsername()
	    {
	    	if(System.getProperty("environment") != null)
	    		environment = System.getProperty("environment");
	    	String username = "";
	    	switch(environment.toUpperCase())
	    	{
	    		case "DEV1":
	    			username = pc.getString("DEV1WsseUsername");
	    			break;
	    		case "DEV2":
	    			username = pc.getString("DEV2WsseUsername");
	    			break;
	    		case "TST1":
	    			username = pc.getString("TST1WsseUsername");
	    			break;
	    		case "TST2":
	    			username = pc.getString("TST2WsseUsername");
	    			break;
	    		case "SIT1":
	    			username = pc.getString("SIT1WsseUsername");
	    			break;
	    		case "SIT2":
	    			username = pc.getString("SIT2WsseUsername");
	    			break;
	    		default:
	    			username = null;
	    			logger.error("Invalid/No such environment");
	    	}
	    	return username;
	    }
	    
	    public String getWssePassword()
	    {
	    	if(System.getProperty("environment") != null)
	    		environment = System.getProperty("environment");
	    	String password = "";
	    	switch(environment.toUpperCase())
	    	{
	    		case "DEV1":
	    			password = pc.getString("DEV1WssePassword");
	    			break;
	    		case "DEV2":
	    			password = pc.getString("DEV2WssePassword");
	    			break;
	    		case "TST1":
	    			password = pc.getString("TST1WssePassword");
	    			break;
	    		case "TST2":
	    			password = pc.getString("TST2WssePassword");
	    			break;
	    		case "SIT1":
	    			password = pc.getString("SIT1WssePassword");
	    			break;
	    		case "SIT2":
	    			password = pc.getString("SIT2WssePassword");
	    			break;
	    		default:
	    			password = null;
	    			logger.error("Invalid/No such environment");
	    	}
	    	return password;
	    }
	    
	    public String getEndpoint()
	    {
	    	String endPoint = "";
/*	    	if(System.getProperty("environment") == null)
	    		endPoint = pc.getString(environment);
	    	else*/
	    		endPoint = pc.getString(System.getProperty("SIT1"));
	    	return endPoint;
	    }
}
