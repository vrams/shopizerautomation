package com.macquarie.bfs.siebel.WebServices;

import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlTestSuite;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCaseRunner;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStep;
import com.eviware.soapui.model.testsuite.TestStepResult;
import com.eviware.soapui.support.types.StringToObjectMap;
import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

/**
 * Created by vthaduri on 23/02/2017.
 */
public class WebServiceRequests extends BaseRequest{

    public String getAdminClientRequest(String adminId, String adminType) {
        try {
            //Prepare the SoapUI Project
            WsdlProject project = new WsdlProject(getClass().getClassLoader().getResource(pc.getString("SoapUIProject")).getFile());
            WsdlTestSuite wsdlTestSuite = project.getTestSuiteByName("GetAdminClientId");
            WsdlTestCase wsdlTestCase = wsdlTestSuite.getTestCaseByName("GetAdminClientId");
            WsdlTestStep wsdlTestStep = wsdlTestCase.getTestStepByName("GetAdminClientId");
            //Set the end point URL based on the environment
            wsdlTestStep.setPropertyValue("Endpoint", "http://bfssydtst772:9080/MDMWSProvider/MDMService");
            //Set the Header Username and Password
            wsdlTestCase.setPropertyValue("wsseUsername", "tsmdmopsusr");
            wsdlTestCase.setPropertyValue("wssePassword", "LD2SF2cmukWS7GS");
            //Set the payload parameters (Other custom properties)
            wsdlTestCase.setPropertyValue("AdminClientNum", adminId);
            wsdlTestCase.setPropertyValue("AdminSystemType", adminType);
            wsdlTestCase.setPropertyValue("InquiryLevel", "3");
            WsdlTestCaseRunner wsdlTestCaseRunner = new WsdlTestCaseRunner(wsdlTestCase, new StringToObjectMap(wsdlTestCase.getProperties()));
            System.out.println(wsdlTestCaseRunner.toString());
            TestStepResult testStepResult = wsdlTestCaseRunner.runTestStep(wsdlTestStep);
            if (testStepResult instanceof WsdlTestRequestStepResult) {
                response = (((WsdlTestRequestStepResult) testStepResult).getResponse().getContentAsString());
            }
            project.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
