package com.macquarie.bfs.siebel.WebServices;

import com.macquarie.bfs.siebel.commons.SiebelException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

/**
 * Created by vthaduri on 18/01/2017.
 */
public class XMLParser {
    private static Logger logger = Logger.getLogger(XMLParser.class);
    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder documentBuilder;
    private Document document;
    private XPath xpath;
    private PropertiesConfiguration pc;
    private NodeList nodeList;
    private Node node;

    public XMLParser() {
        try {
            dbFactory = DocumentBuilderFactory.newInstance();
            documentBuilder = dbFactory.newDocumentBuilder();
            xpath = XPathFactory.newInstance().newXPath();

            pc = new PropertiesConfiguration("objectRepository.properties");
        } catch (Exception e) {
            logger.error("Unable to find Properties file");
            e.printStackTrace();
        }
    }

    //Parses the SOAP response and returns the desired value from the supplied xpath expression
    public NodeList parseResponse(String response, String xpathExpression) throws SiebelException {
        try {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(response)));
            document.getDocumentElement().normalize();
            nodeList = (NodeList) xpath.compile(xpathExpression).evaluate(document, XPathConstants.NODESET);
            if (nodeList.getLength() == 0 || nodeList == null) {
                logger.info("Error in Xpath Expression. Xpath doesn't exist");
                // throw new SiebelException("FATAL Error in SOAP UI Response");
            }
        } catch (NullPointerException e) {
            throw new SiebelException("SOAPUI Response is Empty");
        } catch (XPathExpressionException e) {
            throw new SiebelException("Invalid Xpath or Xpath doesn't exist");
        } catch (IOException e) {
            throw new SiebelException("Invalid XML Response or unable to parse the XML Document");
        } catch (SAXException e) {
            throw new SiebelException("XML Document parsing exception");
        }
        return nodeList;
    }

    public String getElement(String response, String xpath) throws SiebelException {
        return parseResponse(response, xpath).item(0).getFirstChild().getNodeValue();
    }

    public int getNodeCount(String response, String xpath) throws SiebelException {
        return parseResponse(response, xpath).getLength();
    }

    public String getNodeElement(String response, String xpathExpression) throws SiebelException {
        String element = "";
        try {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(response)));
            document.getDocumentElement().normalize();
            node = (Node) xpath.evaluate(xpathExpression, document, XPathConstants.NODE);
            if (node == null)
                element = "NULL";
            else
                element = node.getFirstChild().getNodeValue();

        } catch (NullPointerException e) {
            throw new SiebelException("SOAPUI Response is Empty");
        } catch (XPathExpressionException e) {
            throw new SiebelException("Invalid Xpath or Xpath doesn't exist");
        } catch (IOException e) {
            throw new SiebelException("Invalid XML Response or unable to parse the XML Document");
        } catch (SAXException e) {
            throw new SiebelException("XML Document parsing exception");
        }
        return element;
    }

    public HashMap<String,String> getNode(String response, String xpathExpression, String tagReference, String tagValue) throws SiebelException {
        HashMap<String,String> values = new HashMap<String, String>();
        try {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(response)));
            document.getDocumentElement().normalize();
            NodeList nodeList = (NodeList) xpath.compile(xpathExpression).evaluate(document, XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    values.put(element.getElementsByTagName(tagReference).item(0).getTextContent(),
                            element.getElementsByTagName(tagValue).item(0).getTextContent());
                }
            }
        } catch (Exception e) {
            throw new SiebelException("Invalid SOAP Response/Xpath Expression");
        }
        return values;
    }
}
