package com.macquarie.bfs.siebel.pages;

import com.macquarie.bfs.siebel.WebServices.WebServiceRequests;
import com.macquarie.bfs.siebel.commons.DownloadDrivers;
import com.macquarie.bfs.siebel.commons.XMLParser;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by vthaduri on 13/02/2017.
 * Base Class for creating driver instance which can be extended by other classes
 */
public class BasePage {
    private static Logger logger = LoggerFactory.getLogger(BasePage.class);

    protected WebDriver driver;
    protected PropertiesConfiguration siebelProperties;
    protected String environment = "";
    protected XMLParser xmlParser;
    protected WebServiceRequests webServiceRequests;
    protected int timeOutInSeconds = 30;
    protected int implicitWaitTimeOut = 0;

    By saveImage = By.xpath("//div[@id='mask-img']");
    By loadingImage = By.xpath("//*[@id=\"mask\"]");

    /* Base page constructor */
    public BasePage(WebDriver driver) {
        this.driver = driver;

        //this.driver = new DownloadDrivers().getDriver("ie");
        PageFactory.initElements(driver, this);
        try {
            siebelProperties = new PropertiesConfiguration("ObjectRepository.properties"); //get the object repository property file
            timeOutInSeconds = Integer.valueOf(siebelProperties.getString("TimeOutInSeconds"));
            implicitWaitTimeOut = Integer.valueOf(siebelProperties.getString("ImplicitWaitTimeOut"));
            this.driver.manage().timeouts().implicitlyWait(implicitWaitTimeOut, TimeUnit.SECONDS);

        } catch (Exception e) {
            logger.info("Unable to find Object Repository");
            logger.info("*** Exception ---"+e.getMessage());
            e.printStackTrace();
        }
    }



    /* Returns the web driver instance */
    public WebDriver getDriver() {
        return this.driver;
    }

    /* Function to wait for visibility of the element to be displayed*/
    public void waitForElementToBeDisplayed(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);// Driver wait time
            wait.until(ExpectedConditions.visibilityOf(element));
            if(element == null){
                logger.info("**** Element is NULL ----");
            }

        } catch (Throwable throwable) {
            throwable.getMessage();
        }
    }

    /* Function to wait for visibility of the element to be displayed*/
    public void waitForElementToBeDisplayed(WebElement element, String message)
    {
        try
        {
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);// Driver wait time
            wait.until(ExpectedConditions.visibilityOf(element));

            logger.info("*** Waited for --"+message);

        }
        catch (Throwable e)
        {
            throw new WebDriverException(message + " has Exception. Caused by " + e.getCause());
        }
    }
    /* Function to wait for visibility of the element to be displayed by passing the desired time*/
    public void waitForElementToBeDisplayed(WebElement element, int time) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, time); // Driver wait time
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Throwable throwable) {
            throwable.getMessage();
        }
    }
    /* Function to wait for visibility of the elements to be displayed*/
    public void waitForVisibilityElements(List<WebElement> elements) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.visibilityOfAllElements(elements));
        } catch (Throwable throwable) {
            throwable.getMessage();
        }
    }
    /* Wait for the element to disappear. Usually used when the pop-up loading window to disappear */
    public void waitForLoadingImageDisappear() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(loadingImage));
            logger.info("*** Waited for Loading Image to disappear");
        } catch (Throwable throwable) {
            throwable.getMessage();
        }
    }
    /* Wait for the element to disappear. Usually used when the pop-up loading window to disappear */
    public void waitForSaveImageDisappear() {
        try {
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(saveImage));
            logger.info("*** Waited for Save Image to disappear");
        } catch (Throwable throwable) {
            throwable.getMessage();
        }
    }

    public void waitForElementToDisappear(By locator){
        try{
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
        }catch (Exception e){
            e.getStackTrace();
        }
    }

    public void waitForElementToAppear(By locator){
        try{
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        }catch (Exception e){
            e.getStackTrace();
        }
    }
    public void waitForElementToAppear(By locator, String message){
        try{
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            logger.info("*** Waited for -"+message);
        }catch (Exception e){
            e.getStackTrace();
        }
    }
    public void waitForElementToBeClickable(WebElement element){
        try{
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }catch (Exception e){
            e.getStackTrace();
        }
    }


    public void waitForElementToBeClickable(WebElement element, String message){
        try{
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            logger.info("Waited for "+message);
        }catch (Exception e){
            e.getStackTrace();
        }
    }


    /* Function to wait for presence of the element */
    public void waitForPresence(By by) {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }
    /* Wait for the Alert to display */
    public void waitForAlert() {
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.alertIsPresent());
    }
    /* Find an element using the WebElement Object */
    public WebElement findElement(WebElement webElement) {
        waitForElementToBeDisplayed(webElement);
        return webElement;
    }
    /* Find an element using the By class object */
    public WebElement findElement(By by) {
        WebElement webElement = driver.findElement(by);
        waitForElementToBeDisplayed(webElement);
        return webElement;
    }
    /* Function for selecting the dropdown text */
    public void selectDropdownText(WebElement dropdown, String selectText) {
        Select select = new Select(dropdown);
        select.selectByVisibleText(selectText);
    }
    /* Function to impose wait for desired milli seconds of time */
    public void waitFor(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ie) {
            ie.getMessage();
        }
    }
    /* Function to wait for the page to load. It will till 180 seconds otherwise it will fail the test*/
    public void waitForPageToLoad() {
        ExpectedCondition<Boolean> javascriptDone = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                try {
                    return ((JavascriptExecutor) getDriver()).executeScript("return document.readyState").equals("complete");
                } catch (Exception e) {
                    return Boolean.FALSE;
                }
            }
        };

        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);//Typical wait time 180 seconds
        wait.until(javascriptDone);
    }

    public void reloadPage()
    {
        driver.navigate().refresh();
        waitForPageToLoad();
    }

    public boolean retryFindClick(By by) {
        boolean success = false;
        int clickAttempts = 0;
        while(clickAttempts < 30) {
            try {
                driver.findElement(by).click();
                success = true;
                break;
            } catch(StaleElementReferenceException e) {
                logger.info("**** Caught Stale Element Reference ---");
            }
            clickAttempts++;
        }
        return success;

    }

    public boolean retryFindClick(WebElement element) {
        boolean success = false;
        int clickAttempts = 0;
        while(clickAttempts < 30) {
            try {
                element.click();
                success = true;
                break;
            } catch(StaleElementReferenceException e) {
                logger.info("**** Caught Stale Element Reference ---");
            }
            clickAttempts++;
        }
        return success;

    }


    public void waitTillStalnessOfElement(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(ExpectedConditions.stalenessOf(element));
    }

    public void scrollPageDown() {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        //executor.executeScript("window.scrollBy(0,250)", "");
        executor.executeScript("scroll(0, 350);");
    }
    public void scrollDown() {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        //executor.executeScript("window.scrollBy(0,250)", "");
        executor.executeScript("scroll(0, 450);");
    }

    public void scrollPageUp() {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("scroll(0, -350);");

    }

    public void scrollPageLeft() {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("scroll(-300, 0);");

    }
    public void waitForAllElementsToBeVisible(List<WebElement> elements, String message){
        try{
            WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
            wait.until(ExpectedConditions.visibilityOfAllElements(elements));
            logger.info("Waited for "+message);
        }catch (Exception e){
            e.getStackTrace();
        }

    }

    public void performAction(WebElement element , String value, boolean tab){
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.sendKeys(value).perform();
        //actions.build().perform();
        if(tab){
            actions.sendKeys(Keys.TAB).perform();
        }
    }

    public void closePerformanceDashBoard() {
        Actions keyAction = new Actions(driver);
        keyAction.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys("U").perform();
        logger.info("Closed performance dashboard");
    }

    public void scrollToBottom() {
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("window.scrollTo(0,document.body.scrollHeight)");
    }

    public void inputText(WebElement webElement, String text)
    {
        waitForElementToBeDisplayed(webElement);
        webElement.sendKeys(text);
        logger.info("Input text to" + text);
    }

}
