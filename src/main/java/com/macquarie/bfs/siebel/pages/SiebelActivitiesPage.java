package com.macquarie.bfs.siebel.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vthaduri on 24/01/2016.
 */
public class SiebelActivitiesPage extends BasePage
{
    static Logger logger = LoggerFactory.getLogger(SiebelActivitiesPage.class);
    private WebDriver driver;

    public SiebelActivitiesPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//button[@title='Activities:New']")
    WebElement activitiesNewBtn;

    public void clickActivitiesNewButton(){
        waitForElementToBeDisplayed(activitiesNewBtn);
        activitiesNewBtn.click();
        logger.info("Clicked on the activities new button");
    }
}
