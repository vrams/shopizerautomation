package com.macquarie.bfs.siebel.pages;

import com.macquarie.bfs.siebel.commons.SiebelUtil;
import flex.messaging.util.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vthaduri on 24/01/2016.
 */
public class SiebelContactsPage extends BasePage {
    static Logger logger = LoggerFactory.getLogger(SiebelContactsPage.class);

    public SiebelContactsPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindAll(@FindBy(xpath = "//table[@class=\"AppletBackTable\"]/tbody/tr/td/button[@class=\"appletButton\"]"))
    List<WebElement> bottomButtonList;

    @FindAll(@FindBy(xpath = "//div[@class=\"mceGridField\"]/input"))
    List<WebElement> bottomTableFields;

    @FindBy(xpath = "//input[@aria-labelledby='FirstName_Label']")
    WebElement firstName;

    @FindBy(xpath = "//input[@aria-labelledby='LastName_Label']")
    WebElement lastName;

    @FindBy(name = "Last Name")
    WebElement contactsLastName;

    @FindAll(@FindBy(xpath = "//div[@id=\"s_vctrl_div_tabScreen\"]/ul/li/a"))
    List<WebElement> bottomContactTabs;

    @FindBy(xpath = "//table[@summary=\"System Links\"]/tbody/tr/td[@id=\"1_s_1_l_External_Id\"]")
    WebElement mdmId;

    @FindBy(xpath = "//div[@class=\"mceGridField\"]/input[@aria-labelledby=\"MBL_Siebel_Id_Label\"]")
    WebElement siebelId;

    @FindBy(xpath = "//div[@class=\"mceGridField\"]/input[@aria-labelledby=\"LastName_Label\"]")
    WebElement contactLastName;

    @FindBy(xpath = "//div[@class=\"mceGridField\"]/input[@aria-labelledby=\"FirstName_Label\"]")
    WebElement contactFirstName;

    @FindBy(xpath = "//div[@class=\"mceGridField\"]/input[@aria-labelledby=\"Type_Label\"]")
    WebElement contactType;

    @FindBy(xpath = "//div[@class=\"mceGridField\"]/input[@aria-labelledby=\"Birth_Date_Label\"]")
    WebElement contactDOB;

    @FindBy(xpath = "//div[@class=\"mceGridField\"]/input[@aria-label=\"Title\"]")
    WebElement contactTitle;

    @FindBy(xpath = "//input[@aria-labelledby='Sector_Label']")
    WebElement sector;

    @FindBy(xpath = "//input[@aria-labelledby='Source_Label']")
    WebElement source;

    @FindBy(xpath = "//input[@aria-labelledby='MBLBU_Label']")
    WebElement businessUnit;

    @FindBy(id = "s_4_1_92_0_icon")
    WebElement reportGrpIcon;

    @FindBy(xpath = "//button[@title='Reporting Group:New']")
    WebElement addNewReportGrpBtn;

    @FindBy(xpath = "//input[@aria-labelledby='Group_Customer_Name_Label']")
    WebElement reportGrpName;

    @FindBy(xpath = "//input[@aria-labelledby='Segment_Label']")
    WebElement reportGrpSector;

    @FindBy(xpath = "//td[@class='scField']/input[@aria-labelledby='Source_Label']")
    WebElement reportGrpSource;

    @FindBy(xpath = "//input[@aria-labelledby='Sales_Business_Unit_Label']")
    WebElement reportGrpSalesBU;

    @FindBy(xpath = "//td[@class='scField']/input[@aria-labelledby='RM_Label']//following::img[@class='applet-form-mvg']")
    WebElement relpManagerIcon;

    @FindBy(xpath = "//button[@title='Owner/Adviser:OK']")
    WebElement relManagerOwnerOKBtn;

    @FindBy(xpath = "//button[@title='Reporting Group:Save']")
    WebElement saveReportGrpBtn;

    @FindBy(xpath = "//button[@title='Reporting Group:OK']")
    WebElement reportGrpOKBtn;

    @FindBy(xpath = "//input[@aria-labelledby='UserType_Label']")
    WebElement subType;

    @FindBy(xpath = "//input[@aria-labelledby='MBLABClassification_Label']")
    WebElement abClassification;

    @FindBy(id = "s_4_1_5_0_icon")
    WebElement indCodeIcon;

    @FindBy(xpath = "//span[text()='Reporting Group:']")
    WebElement reportGrpText;

    @FindBy(xpath = "//button[@title='Contacts:Query']")
    WebElement contactsQryBtn;

    @FindBy(xpath = "//td[contains(@id,'MBL_Siebel_Id')]")
    WebElement tdSiebelId;

    @FindBy(id = "1_s_2_l_Last_Name")
    WebElement lastNameColumn;

    @FindBy(id = "1_Last_Name")
    WebElement lastNameInput;

    @FindBy(name = "MBL_Siebel_Id")
    WebElement siebelIdInput;

    @FindBy(xpath = "//button[@title='Contacts:Go']")
    WebElement contactsGoBtn;

    @FindBy(xpath = "//button[@title='Contact:Push To Pivot']")
    WebElement pushToPivotBtn;

    @FindBy(xpath = "//button[@title='Fax:New']")
    WebElement newFaxBtn;

    @FindBy(xpath = "//td[@class='AppletTitle' and text()='Fax']")
    WebElement faxText;

    @FindBy(xpath = "//td[@id='1_s_2_l_Address']")
    WebElement faxNumberTd;

    @FindBy(xpath = "//td[@id='1_s_2_l_Address']/input[@id='1_Address']")
    WebElement faxNumberInput;

    @FindBy(xpath = "//td[@id='2_s_2_l_Name']") //
            WebElement primaryFax;

    @FindBy(xpath = "//td[@class='AppletTitle' and text()='Email']")
    WebElement emailText;

    @FindBy(xpath = "//button[@title='Email:New']")
    WebElement newEmailBtn;

    @FindBy(xpath = "//td[@id='1_s_5_l_Address']")
    WebElement emailAddrTd;

    @FindBy(xpath = "//td[@id='1_s_5_l_Address']/input[@id='1_Address']")
    WebElement emailAddrInput;

    @FindBy(xpath = "//td[@id='2_s_5_l_Address']")
    WebElement primaryEmail;

    @FindBy(id = "2_Address")
    WebElement primaryEmailInput;

    @FindBy(linkText = "Telecoms")
    WebElement telecoms;

    @FindBy(xpath = "//button[@title='Phone:New']")
    WebElement phoneNewBtn;

    @FindBy(linkText = "Addresses")
    WebElement addresses;

    @FindBy(id = "s_1_1_0_0_Ctrl")
    WebElement addAddress;

    @FindBy(id = "s_1_1_1_0_Ctrl")
    WebElement editAddress;

    @FindBy(xpath = "//label[text()='Enter search']")
    WebElement childWinLabel;

    @FindBy(id = "ButtonSearch")
    WebElement searchAddress;

    @FindBy(xpath = "//a[contains(@title,'70 Kent Street')]")
    WebElement streetAddr;

    @FindBy(id = "Accept")
    WebElement acceptBtn;

    @FindBy(xpath = "//div[contains(text(),'Searching on')]")
    WebElement searchText;

    @FindBy(linkText = "System Links")
    WebElement systemLinks;

    @FindBy(id = "s_4_1_89_0_icon")
    WebElement salesRepIcon;

    @FindBy(xpath = "//button[@title='Owner/Adviser:Show Available']")
    WebElement showAvailableBtn;

    @FindBy(xpath = "//span[text()='starting with']")
    WebElement rgSpanText;

    @FindBy(xpath = "//input[@aria-labelledby='PopupQuerySrchspec_Label']")
    WebElement searchSalesRepInput;

    @FindBy(xpath = "//span/input[@aria-labelledby='PopupQuerySrchspec_Label']//following::span/button[@title='Add Positions:Go']")
    WebElement salesRepGoBtn;

    @FindBy(xpath = "//button[@title='Owner/Adviser:Add >']")
    WebElement addSalesRepBtn;

    @FindAll(@FindBy(xpath = "//table[@summary='Owner/Adviser']/tbody/tr[contains(@class, 'ui-widget-content jqgrow ui-row-ltr')]"))
    List<WebElement> salesRepList;

    @FindBy(xpath = "//button[@title='Owner/Adviser:OK']")
    WebElement salesRepOKBtn;

    @FindBy(xpath = "//button[@title='Owner/Adviser:< Remove']")
    WebElement removeBtn;

    @FindBy(xpath = "//input[@id='1_External_Id']")
    WebElement pivotId;

    @FindBy(xpath = "//td[@id='1_s_1_l_External_Id']")
    WebElement pivotIdColumn;

    @FindBy(name = "s_4_1_61_0")
    WebElement dateOfBirth;

    @FindBy(id = "1_s_2_l_MBL_Siebel_Id")
    WebElement sourceContact;

    @FindBy(id = "2_s_2_l_MBL_Siebel_Id")
    WebElement destContact;

    @FindBy(xpath = "//button[@title='Contacts Menu']")
    WebElement contactsMenu;

    @FindBy(linkText = "Merge Records...")
    WebElement mergeRecords;

    @FindBy(xpath = "//span[text()='Merge Records']")
    WebElement mergeRecordsText;

    @FindBy(xpath = "//button[@title='Merge Records:OK']")
    WebElement mergeRecordsOkButton;

    @FindAll(@FindBy(xpath = "//table[@summary='Contacts']/tbody/tr[contains(@class, 'ui-widget-content jqgrow ui-row-ltr')]"))
    List<WebElement> contactList;

    @FindBy(xpath = "//a[@data-tabindex = 'tabScreen2'  and contains(text(),'Financial Accounts')]")
    WebElement financialAccounts;

    @FindBy(id = "1_s_3_l_Account_Number")
    WebElement accountNumberTd;

    @FindBy(xpath = "//a[@name = 'Account Number'  and  @role='textbox']")
    WebElement accountNumberRef;

    @FindBy(id = "1_s_2_l_Account_Number")
    WebElement accountNumber;

    @FindAll(@FindBy(xpath = "//table[@summary='Phone']/tbody/tr[contains(@class, 'ui-widget-content jqgrow ui-row-ltr')]"))
    List<WebElement> phonelList;

    @FindBy(tagName = "td")
    By contactsTd;

    @FindBy(id = "2_s_2_l_Account_Number")
    WebElement additionalAccNumber;

    @FindBy(id = "1_s_3_l_Account_Status")
    WebElement facilityStatus;

    @FindBy(id = "1_s_2_l_Account_Status")
    WebElement accountStatus;

    @FindBy(xpath = "//a[@data-tabindex = 'tabScreen11'  and contains(text(),'Relationships')]")
    WebElement relationships;

    @FindBy(xpath = "//a[@name='Household Number']")
    WebElement reportGroupName;

    @FindBy(xpath = "//input[@aria-label='Owner/Adviser']//following::img[@class='applet-form-mvg']")
    WebElement rgSalesRepIcon;

    @FindBy(xpath = "//button[@aria-label='Facility:Query']")
    WebElement facilityQueryButton;

    @FindBy(id = "1_s_3_l_Account_Number")
    WebElement facilityNumberCell;

    @FindBy(name = "Account_Number")
    WebElement facilityNumber;

    @FindBy(xpath = "//button[@aria-label='Facility:Go']")
    WebElement facilityGoButton;

    @FindBy(xpath = "//input[@aria-labelledby='UserType_Label']")
    WebElement subTypeValue;

    @FindBy(xpath = "//input[@aria-labelledby='Sector_Label']")
    WebElement sectorValue;

    public void clickFinancialAccount(){
        waitForElementToBeDisplayed(financialAccounts);
        financialAccounts.click();
        logger.info("Clicked on the Financial Assets tab for the Siebel");
    }

    public String getFacilityId()
    {
        waitForElementToBeDisplayed(accountNumberTd);
        accountNumberTd.click();
        String facilityId = accountNumberTd.getAttribute("title");
        logger.info("Facility Number for Siebel Id: " + siebelId + " is: " + facilityId);
        return facilityId;
    }

    public void clickAddress(){
        waitForElementToBeDisplayed(addresses);
        addresses.click();
        scrollPageDown();

    }

    public void clickButtonInBottomTable(String buttonName) {
        for (WebElement button : bottomButtonList) {
            waitForElementToBeDisplayed(button);
            if (button.getText().equalsIgnoreCase(buttonName)) {
                button.click();
                waitForLoadingImageDisappear();
                logger.info("Clicked on " + buttonName + " button on bottom table of Contacts page");
                break;
            }
        }
        waitForPageToLoad();
    }

    public void inputBottomTableField(String field, String value) {
        waitForVisibilityElements(bottomTableFields);
        for (WebElement fields : bottomTableFields) {
            if (fields.getAttribute("aria-label").toLowerCase().equalsIgnoreCase(field)) {
                fields.clear();
                fields.sendKeys(value);
                logger.info("Input data in the " + field + ": " + value);
                break;
            }

        }
    }

    public void inputFirstName(String firstname) {
        waitForElementToBeDisplayed(firstName);
        firstName.sendKeys(firstname);
        logger.info("Input value in FirstName: " + firstname);
    }

    public void inputLastName(String lastname) {
        waitForElementToBeDisplayed(lastName);
        lastName.sendKeys(lastname);
        logger.info("Input value in LastName: " + lastname);
    }

    public void clickLastName() {
        waitForElementToBeDisplayed(contactsLastName, "Contacts Last Name");
        contactsLastName.click();
        waitForLoadingImageDisappear();
        logger.info("Clicked on Contacts Last name");
    }

    public void clickTabInBottomTable(String tabName) {
        waitForVisibilityElements(bottomContactTabs);
        for (WebElement fields : bottomContactTabs) {
            if (fields.getText().equalsIgnoreCase(tabName)) {
                fields.click();
                waitForLoadingImageDisappear();
                logger.info("Clicked on tab: " + tabName);
                break;
            }
        }
    }

    public String getMDMId() {
        String id = "";
        waitForElementToBeDisplayed(mdmId);
        id = mdmId.getAttribute("title");
        logger.info("Captured MDM Id: " + id);
        return id;
    }

    public String getSiebelId() {
        String id = "";
        waitForElementToBeDisplayed(siebelId);
        id = siebelId.getAttribute("value");
        logger.info("Captured Siebel Id: " + id);
        return id;
    }

    public String getContactType() {
        String id = "";
        waitForElementToBeDisplayed(contactType);
        id = contactType.getAttribute("value");
        logger.info("Captured Contact type: " + id);
        return id;
    }

    public String getContactTitle() {
        String id = "";
        waitForElementToBeDisplayed(contactTitle);
        id = contactTitle.getAttribute("value");
        logger.info("Captured Contact title: " + id);
        return id;
    }

    public String getContactDOB() {
        String id = "";
        waitForElementToBeDisplayed(contactDOB);
        id = contactDOB.getAttribute("value");
        logger.info("Captured Contact DOB: " + id);
        return id;
    }

    public String getContactFirstName() {
        String id = "";
        waitForElementToBeDisplayed(contactFirstName);
        id = contactFirstName.getAttribute("value");
        logger.info("Captured Contact Firstname: " + id);
        return id;
    }

    public String getContactLastName() {
        String id = "";
        waitForElementToBeDisplayed(contactLastName);
        id = contactLastName.getAttribute("value");
        logger.info("Captured Contact Lastname: " + id);
        return id;
    }

    public void inputSector(String sector) {
        waitForElementToBeDisplayed(this.sector);
        this.sector.sendKeys(sector);
    }

    public void inputSource(String source) {
        waitForElementToBeDisplayed(this.source);
        this.source.sendKeys(source);
    }

    public void inputBusinessUnit(String businessUnit) {
        waitForElementToBeDisplayed(this.businessUnit);
        this.businessUnit.sendKeys(businessUnit);
    }

    public void clickReportingGrpIcon() {
        waitForElementToBeDisplayed(reportGrpIcon);
        //reportGrpIcon.click();
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("scroll(1093, 712)");
        executor.executeScript("arguments[0].click();", findElement(reportGrpIcon));
    }

    public void addNewReportGrp() {
        waitForElementToBeDisplayed(addNewReportGrpBtn);
        addNewReportGrpBtn.click();
    }

    public void inputReportGrpName(String reportGrpName) {
        waitForElementToBeDisplayed(this.reportGrpName);
        this.reportGrpName.sendKeys(reportGrpName);
    }

    public void inputReportGrpSector(String reportGrpSector) {
        waitForElementToBeDisplayed(this.reportGrpSector);
        this.reportGrpSector.sendKeys(reportGrpSector);
    }

    public void inputReportGrpSource(String reportGrpSource) {
        waitForElementToBeDisplayed(this.reportGrpSource);
        this.reportGrpSource.sendKeys(reportGrpSource);
    }

    public void inputReportGrpSalesBU(String reportGrpSalesBU) {
        waitForElementToBeDisplayed(this.reportGrpSalesBU);
        this.reportGrpSalesBU.sendKeys(reportGrpSalesBU);
    }

    public void clickReportGrpRelManagerIcon() {
        waitForElementToBeDisplayed(this.relpManagerIcon);
        this.relpManagerIcon.click();
    }

    public void clickRelManagerOwnerAdviserBtn() {
        waitForElementToBeDisplayed(relManagerOwnerOKBtn);
        relManagerOwnerOKBtn.click();
    }

    public void saveReportingGroup() {
        waitForElementToBeDisplayed(saveReportGrpBtn);
        saveReportGrpBtn.click();
    }

    public void clickReportGroupOkButton() {
        waitForElementToBeDisplayed(reportGrpOKBtn);
        reportGrpOKBtn.click();
    }

    public void inputSubType(String subType) {
        waitForElementToBeDisplayed(this.subType);
        this.subType.sendKeys(subType);
    }

    public void clickIndustryCodeIcon() {
        waitForElementToBeDisplayed(indCodeIcon);
        indCodeIcon.click();
    }

    public void waitTillMainPageLoads() {

        waitForElementToBeDisplayed(reportGrpText);
    }

    public void clickContactsQueryButton() {
        waitForElementToBeDisplayed(contactsQryBtn);
        contactsQryBtn.click();
    }

    public void clickSiebelIdColumn() {
        waitForElementToBeDisplayed(tdSiebelId);
        tdSiebelId.click();
    }

    public void inputSiebelId(String siebelId) {
        waitForElementToBeDisplayed(siebelIdInput);
        siebelIdInput.sendKeys(siebelId);
    }

    public void clickContactsGoBtn() {
        waitForElementToBeDisplayed(contactsGoBtn);
        contactsGoBtn.click();
    }

    public void clickPushToPivotBtn() {
        waitForElementToBeDisplayed(pushToPivotBtn);
        pushToPivotBtn.click();
    }

    public void clickAddNewFax() {
        waitForElementToBeDisplayed(newFaxBtn);
        newFaxBtn.click();
    }

    public void waitForFaxTab() {
        waitForElementToBeDisplayed(faxText);
    }

    public void clickFaxNumberColumn() {
        waitForElementToBeDisplayed(faxNumberTd);
        faxNumberTd.click();
    }

    public void inputFaxNumber(String faxNumber) {
        waitForElementToBeDisplayed(this.faxNumberInput);
        this.faxNumberInput.sendKeys(faxNumber);
    }

    public void addSecondaryFaxNumber() {
        waitForElementToBeDisplayed(primaryFax);
        waitForElementToBeDisplayed(faxNumberTd);
        faxNumberTd.click();
    }

    public void waitForEmailTab() {

        waitForElementToBeDisplayed(emailText);
    }

    public void clickAddNewEmail() {
        waitForElementToBeDisplayed(newEmailBtn);
        newEmailBtn.click();

    }

    public void clickEmailAddrColumn() {
        waitForElementToBeDisplayed(emailAddrTd);
        emailAddrTd.click();
    }

    public void inputEmailAddress(String emailAddress) {
        waitForElementToBeDisplayed(emailAddrInput);
        emailAddrInput.sendKeys(emailAddress);
    }

    public void addSecondaryEmail() {
        waitForElementToBeDisplayed(primaryEmail);
        //waitForElementToBeClickable(primaryFax);
        waitForElementToBeDisplayed(emailAddrTd);
        emailAddrTd.click();
    }

    public void updatePrimaryEmail(String email) {
        Actions actions = new Actions(driver);
        actions.moveToElement(primaryEmail);
        actions.click();
        actions.moveToElement(primaryEmailInput);
        actions.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), email);
        actions.build().perform();
    }

    public void clickContactInformation() {
        waitForElementToBeDisplayed(telecoms, "telecoms");
        telecoms.click();
        waitForElementToBeDisplayed(phoneNewBtn, "Add New Phone");
    }

    public void getPhoneList() {
        waitForAllElementsToBeVisible(phonelList, "Waited for telecomms");
    }

    public void checkPrimaryContact() {
        for (WebElement phone : phonelList) {
            List<WebElement> phoneTbCells = phone.findElements(By.tagName("td"));
            int index = 0;
            for (WebElement e : phoneTbCells) {
                index = index + 1;
                if (index == 2) {
                    //check title = 'Y'
                    String titleValue = e.getAttribute("title");
                    if (titleValue.equalsIgnoreCase("Checked") || titleValue.equalsIgnoreCase("Y")) {
                         updatePrimaryPhone(phoneTbCells);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    private void updatePrimaryPhone(List<WebElement> phoneTbCells) {
       logger.info("*** Updating Primary Phone");
       WebElement phoneNumber = phoneTbCells.get(4);
       phoneNumber.click();
       WebElement phoneNumberInput = phoneNumber.findElement(By.tagName("input"));
        Actions actions = new Actions(driver);
        actions.moveToElement(phoneNumberInput);
        String phoneNo = SiebelUtil.generateRandomNumbers();
        logger.info("*** New phone number ---"+phoneNo);
        actions.sendKeys(Keys.HOME, Keys.chord(Keys.SHIFT, Keys.END), phoneNo);
        actions.build().perform();
    }

    public void clickAddOrEditAddress() {
        clickAddOrEditAddress(null , true, null);
    }
    public void clickAddOrEditAddress(String address , boolean isAdd, String addressType) {
        String parentWindow = driver.getWindowHandle();
        logger.info("**** parent window handle is ----"+parentWindow );
        scrollPageDown();
        if(isAdd){
            waitForElementToBeDisplayed(addAddress);
            addAddress.click();
        }else {
            waitForElementToBeDisplayed(editAddress);
            editAddress.click();
        }
        // added a wait to get child pid.
        int timeCount = 1;
        do
        {
            driver.getWindowHandles();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeCount++;
            if ( timeCount > 50 )
            {
                break;
            }
        }
        while ( driver.getWindowHandles().size() == 1 );
        for (String windHandle: driver.getWindowHandles())
        {
            logger.info("****   windows ----"+windHandle);
            if(!parentWindow.equalsIgnoreCase(windHandle)){
                logger.info("**** child window handle ----"+windHandle);
                driver.switchTo().window(windHandle);
            }
        }
        WebElement qasFrame = driver.findElement(By.name("top"));
        driver.switchTo().frame(qasFrame);
        if(address != null){
            driver.findElement(By.id("searchText")).clear();
            driver.findElement(By.id("searchText")).sendKeys(address);
        }else {
            driver.findElement(By.id("searchText")).sendKeys("Unit 33 70 Kent Street, DEAKIN ACT");
        }
        waitForElementToBeDisplayed(searchAddress);
        searchAddress.click();
        if(isAdd) {
            waitForElementToBeDisplayed(searchText);
            WebElement qasResult = driver.findElement(By.name("results"));
            //waitForElementToBeDisplayed(qasResult);
            driver.switchTo().frame(qasResult);
            logger.info("**** switched to QAS Result frame ");
            waitForElementToBeDisplayed(streetAddr, "Address");
            streetAddr.click();
            driver.switchTo().defaultContent();
            driver.switchTo().frame(qasFrame);

            WebElement  addressTp = driver.findElement(By.name("AddressType"));
            Select addressTpSelect = new Select(addressTp);
            if(addressType == null){
                addressType = "Business";
            }
            addressTpSelect.selectByVisibleText(addressType);
        }else{
            driver.switchTo().defaultContent();
            driver.switchTo().frame(qasFrame);
        }
        waitForElementToBeDisplayed(acceptBtn);
        acceptBtn.click();
        logger.info("**** Click Accept Button ----");
        driver.switchTo().window(parentWindow);
        waitForPageToLoad();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickSystemLinks(){
        waitForElementToBeDisplayed(systemLinks);
        systemLinks.click();
    }

    public void clickSalesRepIcon(){
        waitForElementToBeDisplayed(salesRepIcon, "Sales Rep Icon");
        salesRepIcon.click();
    }

    public void clickShowAvailableButton(){
        waitForElementToBeClickable(showAvailableBtn);
        showAvailableBtn.click();
    }

    public void inputSalesRepName(String lastName){
        waitForElementToBeDisplayed(rgSpanText);
        waitForElementToBeDisplayed(searchSalesRepInput);
        searchSalesRepInput.sendKeys(lastName);
    }

    public void searchSalesRep(){
        waitForElementToBeClickable(salesRepGoBtn);
        salesRepGoBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void clickAddSaleRepButton(){
        waitForElementToBeClickable(addSalesRepBtn , "Add Sales Representative");
        addSalesRepBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void selectPrimarySalesRep() {
        for (WebElement salesRep : salesRepList) {
            List<WebElement> salesRepTdCells = salesRep.findElements(By.tagName("td"));
            int index = 0;
            for (WebElement e : salesRepTdCells) {
                index = index + 1;
                if (index == 2) {
                    //check title = 'Y'
                    String titleValue = e.getAttribute("title");
                    if (titleValue.equalsIgnoreCase("UnChecked") || titleValue.equalsIgnoreCase("N")) {
                        e.click();
                        logger.info("*** clicked the td - title column");
                        WebElement primaryChkBox =  e.findElement(By.tagName("input"));
                        primaryChkBox.click();
                        logger.info("**** Click 1 ----");
                        primaryChkBox.click();
                        logger.info("**** Click 2 ----");
                        logger.info("*** clicked the check box , now break from the loop -title column");
                        break;
                    }
                }
            }
        }
    }

    public void clickSalesRepOkButton(){
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
        waitForElementToBeDisplayed(salesRepOKBtn);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("scroll(907, 177)");
        executor.executeScript("arguments[0].click();",  salesRepOKBtn);
    }

    public void clickOkButton(){
        waitForElementToBeClickable(salesRepOKBtn);
        salesRepGoBtn.click();
    }

    public void removeNonPrimaryField() {
        for (WebElement salesRep : salesRepList) {
            List<WebElement> salesRepTdCells = salesRep.findElements(By.tagName("td"));
            int index = 0;
            for (WebElement e : salesRepTdCells) {
                index = index + 1;
                if (index == 2) {
                    //check title = 'Y'
                    String titleValue = e.getAttribute("title");
                    if (titleValue.equalsIgnoreCase("UnChecked") || titleValue.equalsIgnoreCase("N")) {
                        removeNonPrimarySalesRep(salesRepTdCells);
                    }
                }
            }
        }
    }

    private void removeNonPrimarySalesRep(List<WebElement> salesRepTdCells) {
        logger.info("*** Removing Sales Rep");
        WebElement salesRepName = salesRepTdCells.get(3);
        salesRepName.click();
        waitForElementToBeClickable(removeBtn);
        removeBtn.click();
    }


    public String getPivotId(){
        waitForElementToBeDisplayed(pivotIdColumn);
        pivotIdColumn.click();
        waitForElementToBeDisplayed(pivotId);
        return pivotId.getAttribute("value");
    }

    public String getDateOfBirth() {
        waitForElementToBeDisplayed(dateOfBirth);
       return dateOfBirth.getAttribute("value");
    }

    public String formatDOB(String dob) {
        String[] dobArr = dob.split("/");
        if(dobArr[0].length() == 1){
            dob = "0"+dob;
        }
         return dob;
    }
    public void clickAddAddress(String address , String addressType) {
        String parentWindow = driver.getWindowHandle();
        logger.info("**** parent window handle is ----"+parentWindow );
        scrollPageDown();
        waitForElementToBeDisplayed(addAddress);
        waitForElementToBeClickable(addAddress);
        addAddress.click();
        // added a wait to get child pid.
        int timeCount = 1;
        do
        {
            driver.getWindowHandles();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeCount++;
            if ( timeCount > 50 )
            {
                break;
            }
        }
        while ( driver.getWindowHandles().size() == 1 );
        for (String windHandle: driver.getWindowHandles())
        {
            logger.info("****   windows ----"+windHandle);
            if(!parentWindow.equalsIgnoreCase(windHandle)){
                logger.info("**** child window handle ----"+windHandle);
                driver.switchTo().window(windHandle);
                logger.info("**** switched to child window");//Now , getting child pid
            }
        }
        WebElement qasFrame = driver.findElement(By.name("top"));
        driver.switchTo().frame(qasFrame);
        driver.findElement(By.id("searchText")).clear();
        driver.findElement(By.id("searchText")).sendKeys(address);

        waitForElementToBeDisplayed(searchAddress);
        searchAddress.click();
        WebElement  addressTp = driver.findElement(By.id("AddressType"));
        Select addressTpSelect = new Select(addressTp);
        if(addressType == null){
            addressType = "Business";
        }
        addressTpSelect.selectByVisibleText(addressType);
        waitForElementToBeDisplayed(acceptBtn);
        acceptBtn.click();
        logger.info("**** Click Accept Button ----");
        driver.switchTo().window(parentWindow);
        waitForPageToLoad();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void inputLastNameForContactSearch(String lastName){
        waitForElementToBeDisplayed(lastNameColumn);
        lastNameColumn.click();
        waitForElementToBeDisplayed(lastNameInput);
        lastNameInput.sendKeys(lastName);

    }

    public void selectRecordsToMerge() {
        waitForElementToBeDisplayed(sourceContact);
        waitForElementToBeDisplayed(destContact);
        Actions actions = new Actions(driver);
        actions.click(sourceContact).keyDown(Keys.CONTROL).click(destContact).perform();
    }

    public void clickContactsMenu(){
        waitForElementToBeDisplayed(contactsMenu);
        contactsMenu.click();
        waitForElementToBeDisplayed(mergeRecords);
        mergeRecords.click();
    }

    public void mergeContacts() {
        waitForElementToBeDisplayed(mergeRecordsText);
        waitForElementToBeDisplayed(mergeRecordsOkButton);
        mergeRecordsOkButton.click();
        waitForLoadingImageDisappear();
        waitForSaveImageDisappear();
    }

    public void getMergedContactDetails(String lastName) {
        WebElement mergedContact = driver.findElement(By.linkText(lastName));
        waitForElementToBeDisplayed(mergedContact);
        mergedContact.click();
        waitForLoadingImageDisappear();
    }

    public void selectSourceContact(String middleName) {
        WebElement conId = null;
        WebElement conLastName = null;
        WebElement conMiddleName = null;
        for (WebElement sourceContact : contactList) { //tr
            //Get all td - column from tr
            List<WebElement> contactColumn = sourceContact.findElements(By.tagName("td"));//td list
            conId = contactColumn.get(2);
            conLastName = contactColumn.get(3);
            conMiddleName = contactColumn.get(5);
            try{

                if((conMiddleName.getAttribute("title") != null) && (conMiddleName.getAttribute("title").equalsIgnoreCase(middleName))){
                    conId.click();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public String getAccountNumber(){
        String accNum;
        waitForElementToBeDisplayed(accountNumber);
        //accountNumber.click();
        accNum = accountNumber.getAttribute("title");
        logger.info("*** Account Number ---"+accNum);
        return  accNum;
    }

    public String getAdditionalAccountNumber(){
        waitForElementToBeDisplayed(additionalAccNumber);
        logger.info("*** Additional AccNumber ---"+additionalAccNumber.getAttribute("title"));
        return  additionalAccNumber.getAttribute("title");
    }

    public String getFacilityStatus() {
        waitForElementToBeDisplayed(facilityStatus);
        facilityStatus.click();
        String facilitySts = facilityStatus.getAttribute("title");
        logger.info("**** Contacts Facility Status ---"+facilitySts);
        return facilitySts;
    }

    public String getAccountStatus() {
        waitForElementToBeDisplayed(accountStatus);
        accountStatus.click();
        String accStatus = accountStatus.getAttribute("title");
        logger.info("**** Account Status ---"+accStatus);
        return accStatus;
    }

    public void clickRelationshipTab(){
        waitForElementToBeDisplayed(relationships);
        relationships.click();

    }

    public void clickReportGroup(){
        waitForElementToBeDisplayed(reportGroupName);
        reportGroupName.click();
    }

    public void clickRGSalesRep(){
        waitForElementToBeDisplayed(rgSalesRepIcon);
        rgSalesRepIcon.click();
    }

    public void clickFacilityQueryButton()
    {
        waitForElementToBeDisplayed(facilityQueryButton);
        facilityQueryButton.click();
    }

    public void clickFacilityNumberCell()
    {
        waitForElementToBeDisplayed(facilityNumberCell);
        facilityNumberCell.click();
    }

    public void inputFacilityNumber(String accountNumber)
    {
        waitForElementToBeDisplayed(facilityNumber);
        facilityNumber.sendKeys(accountNumber);
    }

    public void clickFacilityGoButton()
    {
        waitForElementToBeDisplayed(facilityGoButton);
        facilityGoButton.click();
    }

    public String getSubtypeValue(){
        waitForElementToBeDisplayed(subTypeValue);
        return subTypeValue.getAttribute("value");
    }
    //sectorValue

    public String getSectorValue(){
        waitForElementToBeDisplayed(sectorValue);
        return sectorValue.getAttribute("value");
    }
}
