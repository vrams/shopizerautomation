package com.macquarie.bfs.siebel.pages;

import com.macquarie.bfs.siebel.commons.SiebelUtil;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by rkurian on 29/08/2017.
 */
public class SiebelReportingGroup extends BasePage {

    static Logger logger = LoggerFactory.getLogger(SiebelReportingGroup.class);

    public SiebelReportingGroup(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(id="s_3_1_48_0_Ctrl")
    WebElement reportGrpNewBtn;

    @FindBy(xpath = "//input[@aria-labelledby='Name_Label']")
    WebElement name;

    @FindBy(xpath = "//input[@aria-labelledby='Segment_Label']")
    WebElement sector;

    @FindBy(xpath = "//input[@aria-labelledby='Source_Label']")
    WebElement source;

    @FindBy(xpath = "//input[@aria-labelledby='AB_Classification_Label']")
    WebElement abClassification;

    @FindBy(xpath = "//input[@aria-labelledby='Status_Label']")
    WebElement type;

    @FindBy(xpath = "//input[@aria-labelledby='Type_Label']")
    WebElement status;

    @FindBy(xpath = "//input[@aria-labelledby='Reporting_Group_Type_Label']")
    WebElement groupType;

    @FindBy(xpath = "//input[@aria-labelledby='DEFT_Extract_Code_Label']")
    WebElement deftCode;

    @FindBy(xpath = "//input[@aria-labelledby='Sales_Business_Unit_Label']")
    WebElement salesBU;

    @FindBy(xpath = "//input[@aria-labelledby='Pivot_Business_Unit_Label']")
    WebElement pivotBU;

    @FindBy(xpath = "//input[@aria-labelledby='Service_Plan_Label']")
    WebElement servicePlan;

    @FindBy(xpath = "//input[@aria-labelledby='DEFT_Consolidation_Label']")
    WebElement deftCheckBox;

    @FindBy(xpath = "//input[@aria-labelledby='MBL_Customer_Owner_Label']")
    WebElement customerOwner;

    @FindBy(xpath = "//button[@title='Reporting Group:Query']")
    WebElement reportGrpQuery;

    @FindBy(id = "1_s_2_l_Name")
    WebElement nameTd;

    @FindBy(id = "1_Name")
    WebElement inputName;

    @FindBy(xpath = "//button[@title='Reporting Group:Go']")
    WebElement reportGoBtn;

    @FindBy(xpath = "//button[@title='Reporting Group:Push to Pivot']")
    WebElement pushToPivot;

    @FindBy(linkText = "System Links")
    WebElement systemLinks;

    @FindBy(id = "s_3_1_14_0_icon")
    WebElement salesRepIcon;

    @FindBy(xpath = "//button[@title='Owner/Adviser:Show Available']")
    WebElement showAvailableBtn;

    @FindBy(xpath = "//span[text()='starting with']")
    WebElement rgSpanText;

    @FindBy(xpath = "//input[@aria-labelledby='PopupQuerySrchspec_Label']")
    WebElement searchSalesRepInput;

    @FindBy(xpath = "//span/input[@aria-labelledby='PopupQuerySrchspec_Label']//following::span/button[@title='Add Positions:Go']")
    WebElement salesRepGoBtn;

    @FindBy(xpath = "//button[@title='Owner/Adviser:Add >']")
    WebElement addSalesRepBtn;

    @FindAll(@FindBy(xpath = "//table[@summary='Owner/Adviser']/tbody/tr[contains(@class, 'ui-widget-content jqgrow ui-row-ltr')]"))
    List<WebElement> salesRepList;

    @FindBy(xpath = "//button[@title='Owner/Adviser:OK']")
    WebElement salesRepOKBtn;

    @FindBy(xpath = "//button[@title='Owner/Adviser:< Remove']")
    WebElement removeBtn;

    @FindBy(xpath = "//button[@title='Contacts:New']")
    WebElement newContactsBtn;

    @FindBy(xpath = "//td[@id='1_s_2_l_First_Name']")
    WebElement firstNameCol;

    @FindBy(xpath = "//input[@id='1_First_Name']")
    WebElement firstName;

    @FindBy(xpath = "//td[@id='1_s_2_l_Last_Name']")
    WebElement lastNameCol;

    @FindBy(xpath = "//input[@id='1_Last_Name']")
    WebElement lastName;

    @FindBy(xpath = "//a[@name='Last Name']")
    By lastNameAnchorTag;

    @FindBy(xpath = "//a[@name='Last Name']")
    WebElement lastNameTag;

    @FindBy(xpath = "//td[@id='1_s_2_l_M_M']")
    WebElement titleCol;

    @FindBy(xpath = "//input[@id='1_M_M']")
    WebElement title;

    @FindBy(xpath = "//button[@title='Companies:New']")
    WebElement newCompanyBtn;

    @FindBy(xpath = "//td[@id='1_s_1_l_Name']")
    WebElement companyNameCol;

    @FindBy(xpath = "//input[@id='1_Name']")
    WebElement companyName;

    @FindBy(xpath = "//td[@id='1_s_2_l_MBL_Business_Phone']")
    WebElement businessPhoneCol;

    @FindBy(xpath = "//input[@id='1_MBL_Business_Phone']")
    WebElement businessPhone;

    @FindBy(xpath = "//td[@id='1_s_1_l_MBL_Business_Phone']")
    WebElement companyBusinessPhoneColumn;

    @FindBy(xpath = "//td[@id='1_s_1_l_MBL_Business_Phone']/input[@id='1_MBL_Business_Phone']")
    WebElement companyBusinessPhoneInput;


    @FindBy(xpath = "//input[@id='1_External_Id']")
    WebElement pivotId;

    @FindBy(xpath = "//td[@id='1_s_2_l_External_Id']")
    WebElement pivotIdColumn;

    @FindBy(id = "1_s_2_l_Key_Contact_Flag")
    WebElement principalContactTd;

    @FindBy(id = "1_Key_Contact_Flag")
    WebElement keyContactFlag;

    @FindBy(id = "1_s_2_l_MBL_Primary_Group_Flag")
    WebElement primaryGroupFlag;

    @FindBy(id = "1_s_2_l_MBL_Siebel_Id")
    WebElement contactId;

    @FindBy(id = "1_s_1_l_MBL_Contact_ID")
    WebElement companyId;

    @FindBy(xpath = "//input[@aria-labelledby ='MBLGroupCustomerNumber_Label']")
    WebElement reportGroupId;

    @FindBy(xpath = "//td[@id='1_s_2_l_Household_Name']")
    WebElement groupIdColumn;

    @FindBy(xpath = "//input[@aria-labelledby='FirstName_Label']")
    WebElement contactFirstName;

    @FindBy(xpath = "//input[@aria-labelledby='LastName_Label']")
    WebElement contactLastName;

    @FindBy(xpath = "//input[@aria-labelledby='MBLPhone_Label']")
    WebElement contactPhoneNumber;

    @FindBy(xpath = "//input[@aria-labelledby='MBLGroupCustomerName_Label']")
    WebElement reportingGroupName;

   // @FindBy(xpath = "//input[@aria-labelledby='Name_Label']")
    @FindBy(xpath = "//input[@name='s_4_1_9_0']")
    WebElement companyNameLabel;

    @FindBy(xpath = "//input[@aria-labelledby='MBLHomePhone_Label']")
    WebElement companyPhone;

    @FindBy(xpath = "//input[@aria-labelledby='Reporting_Group_Label']")
    WebElement companyReportingGroup;

    @FindBy(id = "1_s_2_l_External_Id")
    WebElement reportGroupPivotId;

    @FindBy(id="s_at_m_3")
    WebElement reportingGroupMenu;

    @FindBy(xpath = "//li[starts-with(@data-caption,\"Save Record\")]/a[@class=\"ui-corner-all\"]")
    WebElement saveRecord;

    public void clickReportGroupButton(){
        waitForElementToBeDisplayed(reportGrpNewBtn);
        reportGrpNewBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }
    public void inputReportingGroupName(String groupName){
        waitForElementToBeDisplayed(name);
        name.sendKeys(groupName);
        //waitForNameInput(groupName);
        logger.info("Group Name --"+groupName);
        //performAction(this.name, groupName, true);
    }

    public void inputSector(String sector){
        waitForElementToBeDisplayed(this.sector);
        //this.sector.clear();
        this.sector.sendKeys(sector);
        //performAction(this.sector, sector, true);
    }

    public void inputSource(String source){
        waitForElementToBeDisplayed(this.source);
        //this.source.clear();
        this.source.sendKeys(source);
    }

    public void inputABClassification(String abClassification){
        waitForElementToBeDisplayed(this.abClassification);
        this.abClassification.clear();
        this.abClassification.sendKeys(abClassification);
    }

    public void inputType(String type){
        waitForElementToBeDisplayed(this.type);
        this.type.clear();
        this.type.sendKeys(type);
    }

    public void inputStatus(String status){
        waitForElementToBeDisplayed(this.status);
        this.status.clear();
        this.status.sendKeys(status);
    }

    public void inputGroupType(String groupType){
        waitForElementToBeDisplayed(this.groupType);
        this.groupType.clear();
        this.groupType.sendKeys(groupType);
    }

    public void inputDeftCode(String deftCode){
        waitForElementToBeDisplayed(this.deftCode);
        this.deftCode.clear();
        this.deftCode.sendKeys(deftCode);
    }

    public void inputSalesBU(String salesBU){
        waitForElementToBeDisplayed(this.salesBU);
        //this.salesBU.clear();
        this.salesBU.sendKeys(salesBU);
    }

    public void inputPivotBU(String pivotBU){
        waitForElementToBeDisplayed(this.pivotBU);
        this.pivotBU.clear();
        this.pivotBU.sendKeys(pivotBU);
    }

    public void inputServicePlan(String servicePlan){
        Actions actions = new Actions(driver);
        actions.moveToElement(this.servicePlan);
        actions.sendKeys(servicePlan);
        actions.build().perform();
    }

    public void clickDeftChecbox(){
        waitForElementToBeDisplayed(deftCheckBox);
        waitForElementToBeClickable(deftCheckBox);
        deftCheckBox.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void inputCustomerOwner(String customerOwner){
        waitForElementToBeDisplayed(this.customerOwner);
        this.customerOwner.clear();
        this.customerOwner.sendKeys(customerOwner);
    }

    public void saveReportingGroup(){

        waitForElementToBeDisplayed(reportingGroupMenu);
        reportingGroupMenu.click();
        waitForElementToBeDisplayed(saveRecord);
        saveRecord.click();
        waitForPageToLoad();
        logger.info("Saved Reporting Group");
    }

    public void clickReportGrpQuery(){
        waitForElementToBeDisplayed(reportGrpQuery);
        reportGrpQuery.click();
    }

    public void searchReportingGroup(String reportingGrp){
        waitForElementToBeDisplayed(nameTd);
        waitForElementToBeClickable(nameTd);
        nameTd.click();
        waitForElementToBeDisplayed(inputName);
        inputName.sendKeys(reportingGrp);

    }

    public void clickReportGrpGoButton(){
        waitForElementToBeDisplayed(reportGoBtn);
        waitForElementToBeClickable(reportGoBtn);
        reportGoBtn.click();
    }

    public void clickPushToPivotButton(){
        waitForLoadingImageDisappear();
        waitForSaveImageDisappear();
        waitForElementToBeDisplayed(pushToPivot);
        waitForElementToBeClickable(pushToPivot);
        pushToPivot.click();
        logger.info("*** Pushed reporting group details to Pivot ***");
    }

    public void waitForNameInput(String groupName) {

      String name = findElement(this.name).getAttribute("value");
      try{
          Thread.sleep(4000);
      }catch (Exception e){
          e.printStackTrace();
      }
      if(name == null || name.equalsIgnoreCase("")){
          this.name.sendKeys(groupName);
      }
    }

    public void checkForAlerts() {
        if(isAlertPresent()) {
            logger.info("*** Alerts present");
            driver.switchTo().alert().accept();
        }else{
            logger.info("*** Alerts not present");
        }
    }

    private boolean isAlertPresent()
    {
        try
        {
            driver.switchTo().alert();
            return true;
        }
        catch (NoAlertPresentException Ex)
        {
            return false;
        }
    }

    public void clickSystemLinks(){
        waitForPageToLoad();
        waitForElementToBeDisplayed(systemLinks);
        systemLinks.click();
    }

    public void clickSalesRepIcon(){
        waitForElementToBeDisplayed(salesRepIcon);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("scroll(1227, 493)");
        executor.executeScript("arguments[0].click();", salesRepIcon);
    }

    public void clickShowAvailableButton(){
        waitForElementToBeClickable(showAvailableBtn);
        showAvailableBtn.click();
    }

    public void inputSalesRepName(String lastName){
        waitForElementToBeDisplayed(rgSpanText);
        waitForElementToBeDisplayed(searchSalesRepInput);
        searchSalesRepInput.sendKeys(lastName);
    }

    public void searchSalesRep(){
        waitForElementToBeClickable(salesRepGoBtn);
        salesRepGoBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void clickAddSaleRepButton(){
        waitForElementToBeClickable(addSalesRepBtn , "Add Sales Representative");
        addSalesRepBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void selectPrimarySalesRep() {
        for (WebElement salesRep : salesRepList) {
            List<WebElement> salesRepTdCells = salesRep.findElements(By.tagName("td"));
            int index = 0;
            for (WebElement e : salesRepTdCells) {
                index = index + 1;
                if (index == 2) {
                    //check title = 'Y'
                    String titleValue = e.getAttribute("title");
                    //logger.info("*** title value ---"+titleValue);
                    if (titleValue.equalsIgnoreCase("UnChecked") || titleValue.equalsIgnoreCase("N")) {
                        e.click();
                        logger.info("*** clicked the td - column");
                        WebElement primaryChkBox =  e.findElement(By.tagName("input"));
                        primaryChkBox.click();
                        primaryChkBox.click();
                        logger.info("*** clicked the check box , now break from the loop - column");
                        break;
                    }
                }
            }
        }
    }

    public void clickSalesRepOkButton(){
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
        waitForElementToBeDisplayed(salesRepOKBtn);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("scroll(907, 177)");
        executor.executeScript("arguments[0].click();", salesRepOKBtn);
    }

    public void waitForPageRefresh() {
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void removeNonPrimaryField() {
        for (WebElement salesRep : salesRepList) {
            List<WebElement> salesRepTdCells = salesRep.findElements(By.tagName("td"));
            int index = 0;
            for (WebElement e : salesRepTdCells) {
                index = index + 1;
                if (index == 2) {
                    //check title = 'Y'
                    String titleValue = e.getAttribute("title");
                    //logger.info("*** title value ---"+titleValue);
                    if (titleValue.equalsIgnoreCase("UnChecked") || titleValue.equalsIgnoreCase("N")) {
                        removeNonPrimarySalesRep(salesRepTdCells);
                    }
                }
            }
        }
    }

    private void removeNonPrimarySalesRep(List<WebElement> salesRepTdCells) {
        logger.info("*** Removing Sales Rep");
        WebElement salesRepName = salesRepTdCells.get(3);
        salesRepName.click();
        waitForElementToBeClickable(removeBtn);
        removeBtn.click();
    }

    public void getReportingGroupDetails(String groupName){
        logger.info("**** Group Name ----"+groupName);
        By group = By.xpath("//td[@id='1_s_2_l_Name']/a");
        waitForElementToAppear(group);
        findElement(group).click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void clickNewContactsButton(){
        waitForElementToBeDisplayed(newContactsBtn);
        newContactsBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void inputFirstName(String firstName){
        waitForElementToBeDisplayed(firstNameCol);
        waitForElementToBeClickable(firstNameCol);
        firstNameCol.click();
        waitForLoadingImageDisappear();
        waitForSaveImageDisappear();
        waitForElementToBeDisplayed(this.firstName);
        this.firstName.sendKeys(firstName);
    }

    public void clickPrimaryGroupFlag(){
        waitForElementToBeDisplayed(primaryGroupFlag);
        primaryGroupFlag.sendKeys(Keys.TAB);
    }
    public void inputLastName(String lastName){
        Actions action = new Actions(driver);
        action.doubleClick(lastNameCol).perform();
        waitForElementToBeDisplayed(this.lastName);
        this.lastName.sendKeys(lastName);
    }

    public void inputTitle(String title) {
        waitForElementToBeDisplayed(titleCol);
        titleCol.click();
        waitForElementToBeDisplayed(this.title);
        this.title.sendKeys(title);
    }

    public void clickNewCompanyButton(){
/*
        waitForElementToBeDisplayed(newCompanyBtn);
        newContactsBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
*/
        Actions actions = new Actions(driver);
        actions.moveToElement(newCompanyBtn);
        actions.click();
        actions.build().perform();
    }

    public void inputCompanyName(String companyName){
        waitForElementToBeDisplayed(companyNameCol);
        companyNameCol.click();
        waitForElementToBeDisplayed(this.companyName);
        this.companyName.sendKeys(companyName);
    }

    public void saveContacts(){
        Actions keyAction = new Actions(driver);
        keyAction.keyDown(Keys.CONTROL).sendKeys("S").perform();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        keyAction.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys("X").perform();
    }

    public void inputBusinessPhone(String phoneNumber){
        waitForElementToBeDisplayed(businessPhoneCol);
        businessPhoneCol.click();
        waitForElementToBeDisplayed(businessPhone);
        businessPhone.sendKeys( phoneNumber + Keys.ENTER);
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public String getPivotId(){
        waitForElementToBeDisplayed(pivotIdColumn);
        pivotIdColumn.click();
        waitForElementToBeDisplayed(pivotId);
        return pivotId.getAttribute("value");
        //return findElement(pivotId).getAttribute("value");
    }

    public void selectPrimaryContact() {
        waitForElementToBeDisplayed(principalContactTd , "principal contact td column");
        principalContactTd.click();
        waitForLoadingImageDisappear();
        waitForSaveImageDisappear();
        waitForElementToBeDisplayed(keyContactFlag);
        keyContactFlag.click();
        keyContactFlag.click();
    }

    public void inputCompanyBusinessPhone(String businessPhone) {
        waitForElementToBeDisplayed(companyBusinessPhoneColumn);
        companyBusinessPhoneColumn.click();
        waitForElementToBeDisplayed(companyBusinessPhoneInput);
        companyBusinessPhoneInput.sendKeys(businessPhone + Keys.ENTER);
    }

    public String getContactId() {
        waitForElementToBeDisplayed(contactId);
        return contactId.getText();
    }

    public String getCompanyId() {
        waitForElementToBeDisplayed(companyId);
        return companyId.getText();
    }

    public String getReportGroupId() {
        waitForElementToBeDisplayed(reportGroupId);
        return  reportGroupId.getAttribute("value");
    }

    public void clickReportGroupIdColumm(){
        waitForElementToBeDisplayed(groupIdColumn);
        groupIdColumn.click();
    }

    public void getContactsDetails(String lastName) {
        WebElement contactName = findElement(By.linkText(lastName));
        contactName.click();
    }

    public String getFirstName() {
        waitForElementToBeDisplayed(contactFirstName);
        return contactFirstName.getAttribute("value");
    }

    public String getLastName() {
        waitForElementToBeDisplayed(contactLastName);
        return contactLastName.getAttribute("value");
    }

    public String getContactPhoneNumber() {
        waitForElementToBeDisplayed(contactPhoneNumber);
        return contactPhoneNumber.getAttribute("value");
    }

    public String getReportingGroupName() {
        waitForElementToBeDisplayed(reportingGroupName);
        return reportingGroupName.getAttribute("value");
    }

    public void getCompanyDetails(String name) {
        WebElement companyName = findElement(By.linkText(name));
        companyName.click();
    }

    public String getCompanyName(){
        waitForElementToBeDisplayed(companyNameLabel);
        return  companyNameLabel.getAttribute("value");
    }

    public String getCompanyPhone(){
        waitForElementToBeDisplayed(companyPhone);
        return  companyPhone.getAttribute("value");
    }

    public String getCompanyReportingGroup(){
        waitForElementToBeDisplayed(companyReportingGroup);
        return  companyReportingGroup.getAttribute("value");
    }

    public String getReportGroupPivotId(){
        waitForElementToBeDisplayed(reportGroupPivotId);
        reportGroupPivotId.click();
        String rgPivotId = reportGroupPivotId.getText();
        logger.info("**** Reporting Group Pivot ID ---"+rgPivotId);
        return rgPivotId;
    }



}
