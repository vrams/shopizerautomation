package com.macquarie.bfs.siebel.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vthaduri on 24/01/2016.
 */
public class SiebelLoginPage extends BasePage
{
    static Logger logger = LoggerFactory.getLogger(SiebelLoginPage.class);

    public SiebelLoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(name = "SWEUserName")
    WebElement loginTextBox;

    @FindBy(name = "SWEPassword")
    WebElement passwordTextBox;

    @FindBy(id = "s_swepi_22")
    WebElement logonButton;

    @FindBy(xpath = "//*[@id=\"_sweappmenu\"]/div/div[1]")
    WebElement oracleLogo;


    public void openApplication()
    {
        environment = "IA2";
        getDriver().navigate().to(siebelProperties.getString(environment));
        logger.info("Application opened in " + environment + " Environment");
        getDriver().manage().window().maximize();
        waitForPageToLoad();
    }

    public void inputUsername(String username)
    {
        inputText(loginTextBox, username);
    }

    public void inputPassword(String password)
    {
        inputText(passwordTextBox, password);
    }
    public void loginApplication(String username, String password)
    {
        waitForPageToLoad();
        loginTextBox.clear();
        passwordTextBox.clear();
        loginTextBox.sendKeys(username);
        passwordTextBox.sendKeys(password);
        loginTextBox.clear();
        loginTextBox.sendKeys(username);
        logonButton.click();
        waitForElementToBeDisplayed(oracleLogo);
        logger.info("Logged into application using username: " + username );
    }

}
