package com.macquarie.bfs.siebel.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vthaduri on 24/01/2016.
 */
public class SiebelHomePage extends BasePage
{
    static Logger logger = LoggerFactory.getLogger(SiebelHomePage.class);
    private WebDriver driver;

    public SiebelHomePage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//div[@class=\"applicationMenu\"]/span/li[@data-caption=\"&File\"]")
    WebElement fileMenu;

    @FindBy(xpath = "//div[@id=\"s_sctrl_tabScreen\"]/ul/li/a[@data-tabindex=\"tabScreen4\"]")
    WebElement contactsTab;

    @FindAll(@FindBy(xpath = "//div[@id=\"s_sctrl_tabScreen\"]/ul/li/a"))
    List<WebElement> tabList;

    @FindBy(id = "loginLogo")
    By loginLogo;

    @FindBy(xpath = "//label[text()='User ID']")
    WebElement userIdLabel;

    @FindBy(xpath = "//div[@class=\"applicationMenu\"]/span/li[@data-caption=\"&File\"]/ul/li[starts-with(@data-caption,'Log Out')]")
    WebElement logOut;

    private void clickFileMenu()
    {
        waitFor(2000);
        waitForElementToBeDisplayed(fileMenu);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.scrollTo(0," + fileMenu.getLocation().y + ")");
        fileMenu.click();
        logger.info("Clicked on File menu");
    }

    private void clickLogoutMenu()
    {
        waitForElementToBeDisplayed(logOut);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("window.scrollTo(0," + logOut.getLocation().y + ")");
        logOut.click();
        logger.info("Clicked on logOut menu");
    }

    public void clickLogout()
    {
        clickFileMenu();
        clickLogoutMenu();
        waitFor(3000);

    }
    public void siebelLogout(WebDriver driver){
        try{
            Actions keyAction = new Actions(driver);
            keyAction.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys("X").keyUp(Keys.CONTROL).keyUp(Keys.SHIFT).perform();
            waitForElementToBeDisplayed(userIdLabel, "user id label");
            Thread.sleep(2000);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void clickTab(String tab, String test)
    {
        if (tab.equalsIgnoreCase("Contacts"))
        {
            waitForElementToBeDisplayed(contactsTab);
            contactsTab.click();
        }
        //waitForPageToLoad();
        waitForLoadingImageDisappear();

        logger.info("Clicked on " + tab + " tab");
    }

    public void clickTab(String tab)
    {
        waitForVisibilityElements(tabList);
        for (WebElement tabs: tabList)
        {

            if (tabs.getText().equalsIgnoreCase(tab))
            {
                tabs.click();
                logger.info("Clicked on tab: " + tab);
                break;
            }
        }
        waitForLoadingImageDisappear();
    }

    public WebElement getSiebelTab(String tab){
        WebElement siebelTab = null;
        for (WebElement tabs: tabList)
        {
            if (tabs.getText().equalsIgnoreCase(tab)){
                siebelTab = tabs;
            }
        }
        return  siebelTab;
    }

}
