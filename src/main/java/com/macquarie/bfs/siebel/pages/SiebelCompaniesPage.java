package com.macquarie.bfs.siebel.pages;

import com.macquarie.bfs.siebel.commons.SiebelUtil;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by rkurian on 15/06/2017.
 */
public class SiebelCompaniesPage  extends  BasePage{

    static Logger logger = LoggerFactory.getLogger(SiebelCompaniesPage.class);
    //private WebDriver driver;

    public SiebelCompaniesPage(WebDriver webDriver) {
        super(webDriver);
    }


    @FindBy(xpath = "//*[@id=\"a_3\"]/div[1]/table/tbody/tr/td[1]")
    WebElement companies;

    @FindBy(id = "s_3_1_8_0_Ctrl")
    WebElement query;

    @FindBy(id = "jqgh_s_3_l_Primary_Organization")
    WebElement primaryOrg;

    @FindBy(id = "s_3_1_5_0_Ctrl")
    WebElement goButton;

    @FindBy(id = "s_3_1_0_0_Ctrl")
    WebElement newActivity;

    @FindBy(name = "s_2_1_1_0")
    WebElement vipRating;

    @FindBy(linkText = "Wadman Ronald Pty Ltd")
    WebElement label;

    @FindBy(xpath = "//*[contains(text(),'Wadman Ronald')]")
    WebElement webElement;

    @FindBy(id = "s_2_1_170_0_Ctrl")
    WebElement createNewCompany ;

    @FindBy(id = "s_at_m_2")
    WebElement menuButton;

    @FindBy(xpath = "//*[@id=\"tb_8\"]")
    WebElement changePos;

    @FindBy(xpath = "//*[@id=\"a_1\"]/div[1]/table/tbody/tr/td[1]")
    WebElement text;

    @FindBy(id = "5_s_1_l_Active_Position")
    WebElement mrbSales;

    @FindBy(id = "s_1_1_0_0_Ctrl")
    WebElement changePosButton;

    @FindBy(linkText = "Companies")
    WebElement companyTab;

    @FindBy(linkText = "Home")
    WebElement home;

    @FindBy(linkText = "All Companies Across Organizations")
    WebElement allCompaniesOrg;

    @FindBy(id = "Company_Details_Label")
    WebElement companyDetails;

    @FindBy(name = "s_4_1_2_0")
    WebElement companyName;

    @FindBy(name = "s_4_1_88_0")
    WebElement type;

    @FindBy(name = "s_4_1_47_0")
    WebElement subType;

    @FindBy(name = "s_4_1_101_0")
    WebElement dear;

    @FindBy(name = "s_4_1_112_0")
    WebElement businessType;

    @FindBy(name = "s_4_1_15_0")
    WebElement status;

    @FindBy(name = "s_4_1_48_0")
    WebElement sector;

    @FindBy(name = "s_4_1_63_0")
    WebElement source;

    @FindBy(name = "s_4_1_90_0")
    WebElement salesBU;

    @FindBy(id = "s_4_1_122_0_Ctrl")
    WebElement cancelButton;

    @FindAll(@FindBy(xpath = "//div[@id=\"s_sctrl_tabScreen\"]/ul/li/a"))
    List<WebElement> tabList;

    @FindBy(id = "s_4_1_11_0_icon")
    WebElement rptGroupIcon;

    @FindBy(xpath = "//button[@title='Reporting Group:New']")
    WebElement createNewRG;

    @FindBy(linkText = "Reporting Group")
    WebElement reportingGroup;

    @FindBy(xpath = "//input[@aria-labelledby='Group_Customer_Name_Label']")
    WebElement rgName;

    @FindBy(xpath = "//span[@id='Label_Name_Label']")
    WebElement labelName;

    @FindBy(xpath = "//form[@action='/finsOUI_enu/start.swe']/table/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td[2]/table/tbody/tr/img")
    WebElement createRM;

    @FindBy(xpath = "//button[@title='Reporting Group:Show Available']")
    WebElement rgShowAvlBtn;

    @FindBy(xpath = "//button[@title='Add Reporting Group:Query']")
    WebElement rgQueryBtn;

    @FindBy(xpath = "//span/input[@aria-labelledby='PopupQuerySrchspec_Label']")
    WebElement searchInput;

    @FindBy(xpath = "//span/input[@aria-labelledby='PopupQuerySrchspec_Label']//following::span/button[@title='Add Reporting Group:Go']")
    WebElement rgGoBtn;

    @FindBy(xpath = "//button[@title='Reporting Group:Add >']")
    WebElement addRG;

    @FindBy(xpath = "//button[@title='Reporting Group:OK']")
    WebElement rgOkBtn;

    By repGroupOK = By.xpath("//button[@title='Reporting Group:OK']");

    @FindBy(xpath = "//td[@title='BARRY PLANT ROWVILLE']")
    WebElement rgNameSelected;

    @FindBy(xpath = "//div[contains(@id, 'SSA_Primary_Field')]")
    WebElement primaryRG;

    @FindBy(id = "s_4_1_69_0_icon")
    WebElement industryCode;

    @FindBy(xpath = "//span[text()='Industry Codes']")
    WebElement industryCdText;

    @FindBy(xpath = "//button[@title='Industry Codes:New']")
    WebElement indCodesNewBtn;

    @FindBy(xpath = "//input[@aria-label='Industry Code Type']")
    WebElement indCodeType;

    @FindBy(xpath = "//input[@aria-label='Industry Code Type']//following::img[@class='applet-form-pick']")
    WebElement indCode;

    @FindBy(xpath = "//input[@aria-labelledby='PopupQuerySrchspec_Label']")
    WebElement indCodeInput;

    @FindBy(xpath = "//button[@title='Pick Industry Code:Find']")
    WebElement pickIndCodeBtn;

    @FindBy(xpath = "//button[@title='Industry Codes:Save']")
    WebElement indCodeSaveBtn;

    @FindBy(xpath = "//button[@title='Industry Codes:OK']")
    WebElement indCodeOkBtn;

    @FindBy(xpath = "//button[@title='Industry Codes:OK']")
    By industryCodesOK;

    @FindBy(xpath = "//span[text()='Pick Industry Code'")
    WebElement pickIndCode;

    @FindBy(xpath = "//button[@title='Account:Save']")
    WebElement saveCompanyBtn;

    @FindBy(xpath = "//a[contains(@href,'Test_Automation')]")
    WebElement companyNameRef;

    @FindBy(xpath = "//input[@aria-label='Siebel ID']")
    WebElement siebelId;

    @FindBy(xpath = "//button[@title='Companies:Query']")
    WebElement companyQryBtn;

    @FindBy(name = "MBL_Contact_ID")
    WebElement mblContactId;

    @FindBy(xpath = "//td[contains(@id,'_MBL_Contact_ID')]")
    WebElement mblIdColumn;

    @FindBy(xpath = "//button[@title='Companies:Go']")
    WebElement companyGoBtn;

    WebElement companyRef;

    @FindBy(id = "s_4_1_36_0_L")
    WebElement repGroupLabel;

    @FindBy(xpath = "//a[text()='Addresses']")
    WebElement address;

    @FindBy(xpath = "//td[text()='All Addresses']")
    WebElement allAddressesText;

    @FindBy(xpath = "//button[@title='All Addresses:Add']")
    WebElement addAddressBtn;

    @FindBy(linkText = "Telecoms")
    WebElement telecoms;

    @FindBy(xpath = "//button[@title='Phone:New']")
    WebElement phoneNewBtn;

    @FindBy(xpath = "//td[@id='1_s_1_l_Name']")
    WebElement phoneType;

    @FindBy(xpath = "//input[@id='1_Name']")
    WebElement phoneTpName;

    @FindBy(xpath = "//td[@id='1_s_1_l_Address']")
    WebElement phoneNum;

    @FindBy(xpath = "//input[@id='1_Address']")
    WebElement phoneNumberInput;

    @FindBy(xpath ="//input[@aria-label='ARBN']")
    WebElement arbn;

    @FindBy(xpath = "//input[@aria-label='ABS Code']")
    WebElement absCode;

    @FindBy(xpath = "//input[@aria-label='ABN']")
    WebElement abn;

    @FindBy(xpath = "//input[@aria-label='ACN']")
    WebElement acn;

    @FindBy(xpath = "//input[@aria-label='Industry Software']")
    WebElement industrySoftware;

    @FindBy(xpath = "//input[@aria-label='Accounting Software']")
    WebElement accSoftware;

    @FindBy(xpath = "//input[@aria-label='Termination Agreement']")
    WebElement termAgreement;

    @FindBy(xpath = "//input[@aria-label='Pivot Business Unit']")
    WebElement pivotBU;

    @FindBy(xpath = "//input[@aria-label='Business Code']")
    WebElement businessCode;

    @FindBy(xpath = "//input[@aria-label='Business Size']")
    WebElement businessSize;

    @FindBy(xpath = "//input[@aria-label='AB Classification']")
    WebElement abClassification;

    @FindBy(xpath = "//input[@aria-label='Customer Category']")
    WebElement customerCategory;

    @FindBy(linkText = "More Info")
    WebElement moreInfo;

    @FindBy(xpath = "//input[@aria-label='Legislation Type']")
    WebElement legislationType;

    @FindBy(xpath = "//input[@aria-label='Legislation Loc']")
    WebElement legislationLoc;

    @FindBy(xpath = "//input[@aria-label='Trust Type']")
    WebElement trustType;

    @FindBy(xpath = "//input[@aria-label='Parent Company']")
    WebElement parentCompany;

    @FindBy(xpath = "//input[@aria-label='Parent Exchange']")
    WebElement parentExchange;

    @FindBy(xpath = "//input[@aria-label='Exchange']")
    WebElement exchange;

    @FindBy(xpath = "//input[@aria-label='Registration/ Membership Body']")
    WebElement membershipBody;

    @FindBy(xpath = "//input[@aria-label='Regulator Name']")
    WebElement regulatorName;

    @FindBy(xpath = "//input[@aria-label='Regulator Issued Id']")
    WebElement regulatorId;

    @FindBy(xpath = "//button[@title='Account:Push to Pivot']")
    WebElement pushToPivot;

    @FindBy(xpath = "//span[text()='Legislation Type']")
    By legislationTpText;

    @FindBy(id = "2_s_1_l_Name")
    WebElement primaryPhone;

    @FindBy(xpath = "//button[@title='Fax:New']")
    WebElement newFaxBtn;

    @FindBy(xpath = "//td[@class='AppletTitle' and text()='Fax']")
    WebElement faxText;

    @FindBy(xpath = "//td[@id='1_s_3_l_Address']")
    WebElement faxNumberTd;

    @FindBy(xpath = "//td[@id='1_s_3_l_Address']/input[@id='1_Address']")
    WebElement faxNumberInput;

    @FindBy(xpath = "//td[@id='2_s_3_l_Address']") //
    WebElement primaryFax;

    @FindBy(xpath = "//td[@class='AppletTitle' and text()='Email']")
    WebElement emailText;

    @FindBy(xpath = "//button[@title='Email:New']")
    WebElement newEmailBtn;

    @FindBy(xpath = "//td[@id='1_s_5_l_Address']")
    WebElement emailAddrTd;

    @FindBy(xpath = "//td[@id='1_s_5_l_Address']/input[@id='1_Address']")
    WebElement emailAddrInput;

    @FindBy(xpath = "//td[@id='2_s_5_l_Address']")
    WebElement primaryEmail;

    @FindBy(xpath = "//span[@id='MBL_ITC_Industry_Code_Entry_Label']")
    WebElement industryCodeLabel;

    @FindBy(id = "2_Address")
    WebElement primaryEmailInput;

    @FindBy(linkText = "Addresses")
    WebElement addresses;
    @FindBy(id = "ButtonSearch")
    WebElement searchAddress;

    @FindBy(xpath = "//a[contains(@title,'70 Kent Street')]")
    WebElement streetAddr;

    @FindBy(id = "Accept")
    WebElement acceptBtn;

    @FindBy(xpath = "//div[contains(text(),'Searching on')]")
    WebElement searchText;

    @FindBy(id = "s_1_1_0_0_Ctrl")
    WebElement addAddress;

    @FindBy(id = "1_s_3_l_Name")
    WebElement searchByCompanyName;

    @FindBy(xpath = "//td[@id='1_s_3_l_Name']/input[@id='1_Name']")
    WebElement inputCompanyName;

    @FindBy(xpath = "//input[@aria-labelledby='Sales_Rep_Entry_Label']//following::img[@class='applet-form-mvg']")
    WebElement salesRepIcon;

    @FindBy(xpath = "//button[@title='Owner/Adviser:Show Available']")
    WebElement showAvailableBtn;

    @FindBy(xpath = "//span[text()='starting with']")
    WebElement rgSpanText;

    @FindBy(xpath = "//input[@aria-labelledby='PopupQuerySrchspec_Label']")
    WebElement searchSalesRepInput;

    @FindBy(xpath = "//span/input[@aria-labelledby='PopupQuerySrchspec_Label']//following::span/button[@title='Add Positions:Go']")
    WebElement salesRepGoBtn;

    @FindBy(xpath = "//button[@title='Owner/Adviser:Add >']")
    WebElement addSalesRepBtn;

    @FindAll(@FindBy(xpath = "//table[@summary='Owner/Adviser']/tbody/tr[contains(@class, 'ui-widget-content jqgrow ui-row-ltr')]"))
    List<WebElement> salesRepList;

    @FindBy(xpath = "//button[@title='Owner/Adviser:OK']")
    WebElement salesRepOKBtn;

    @FindBy(xpath = "//button[@title='Owner/Adviser:< Remove']")
    WebElement removeBtn;

    @FindBy(linkText = "System Links")
    WebElement systemLinks;

    @FindAll(@FindBy(xpath = "//table[@summary='System Links']/tbody/tr[contains(@class, 'ui-widget-content jqgrow ui-row-ltr')]"))
    List<WebElement> systemList;

    @FindAll(@FindBy(xpath = "//table[@summary='All Addresses']/tbody/tr[contains(@class, 'ui-widget-content jqgrow ui-row-ltr')]"))
    List<WebElement> allAddresses;

    @FindBy(id = "1_s_3_l_MBL_Contact_ID")
    WebElement sourceCompany;

    @FindBy(id = "2_s_3_l_MBL_Contact_ID")
    WebElement destinationCompany;

    @FindBy(xpath = "//button[@title='Companies Menu']")
    WebElement companiesMenu;

    @FindBy(linkText = "Merge Records...")
    WebElement mergeRecords;

    @FindBy(xpath = "//button[@title='Merge Records:OK']")
    WebElement mergeRecordsOkButton;

    @FindBy(xpath = "//span[text()='Merge Records']")
    WebElement mergeRecordsText;

    @FindBy(xpath = "//input[@aria-labelledby='MBL_Sub_Type_Label']")
    WebElement subTypeInput;

    @FindBy(xpath = "//input[@aria-labelledby='MBL_Sector_Label']")
    WebElement sectorInput;

    @FindBy(xpath = "//input[@aria-labelledby='Source_Label']")
    WebElement sourceInput;

    @FindBy(xpath = "//input[@aria-labelledby='MBLBU_Label']")
    WebElement salesBUInput;

    @FindBy(xpath = "//input[@aria-labelledby='MBLABSCode_Label']")
    WebElement absCodeInput;

    @FindBy(xpath = "//input[@aria-labelledby='AB_Classification_Label']") //
    WebElement abClassificationInput;

    @FindBy(xpath = "//a[@data-tabindex = 'tabScreen2'  and contains(text(),'Financial Accounts')]")
    WebElement financialAccounts;

    @FindBy(id = "1_s_3_l_Account_Number")
    WebElement facilityId;

    @FindBy(id = "1_s_2_l_Account_Number")
    WebElement accountNumber;

    @FindBy(id = "1_s_3_l_Account_Status")
    WebElement facilityStatus;

    @FindBy(id = "1_s_2_l_Account_Status")
    WebElement accountStatus;

    @FindBy(xpath = "//a[@data-tabindex = 'tabScreen10'  and contains(text(),'Relationships')]")
    WebElement relationships;

    @FindBy(xpath = "//a[@name='Household Number']")
    WebElement reportGroupName;

    @FindBy(id = "2_s_2_l_Account_Number")
    WebElement additionalAccNumber;

    public String getSubTypeValue(){
        waitForElementToBeDisplayed(subTypeInput);
        return subTypeInput.getAttribute("value");
    }

    public String getSectorValue(){
        waitForElementToBeDisplayed(sectorInput);
        return sectorInput.getAttribute("value");
    }

    public String getSalesBUValue(){
        waitForElementToBeDisplayed(salesBUInput);
        return salesBUInput.getAttribute("value");
    }

    public String getABSCodeValue(){
        waitForElementToBeDisplayed(absCodeInput);
        return absCodeInput.getAttribute("value");
    }

    public String getABClassificationUnit(){
        waitForElementToBeDisplayed(abClassificationInput);
        return abClassificationInput.getAttribute("value");
    }

    public void clickAddNewFax() {
        waitForElementToBeDisplayed(newFaxBtn);
        findElement(newFaxBtn).click();
    }

    public void waitForFaxTab(){
        waitForElementToBeDisplayed(faxText);
    }

    public void clickFaxNumberColumn(){
        waitForElementToBeDisplayed(faxNumberTd);
        findElement(faxNumberTd).click();
    }

    public void inputFaxNumber(String faxNumber){
        waitForElementToBeDisplayed(this.faxNumberInput);
        findElement(this.faxNumberInput).sendKeys(faxNumber);
    }

    public void addSecondaryFaxNumber() {
        waitForElementToBeDisplayed(primaryFax);
        waitForElementToBeDisplayed(faxNumberTd);
        findElement(faxNumberTd).click();
    }

    public void waitForEmailTab() {

        waitForElementToBeDisplayed(emailText);
    }

    public void clickAddNewEmail() {
        waitForElementToBeDisplayed(newEmailBtn);
        findElement(newEmailBtn).click();

    }

    public void clickEmailAddrColumn() {
        waitForElementToBeDisplayed(emailAddrTd);
        findElement(emailAddrTd).click();
    }

    public void inputEmailAddress(String emailAddress){
        waitForElementToBeDisplayed(emailAddrInput);
        findElement(emailAddrInput).sendKeys(emailAddress);
    }

    public void addSecondaryEmail() {
        waitForElementToBeDisplayed(primaryEmail);
        waitForElementToBeDisplayed(emailAddrTd);
        findElement(emailAddrTd).click();
    }

    public void waitTillPrimaryEmailAppear(){
        waitForElementToBeDisplayed(primaryEmail);
    }


    public void clickTab(String tab)
    {
        waitForVisibilityElements(tabList);
        for (WebElement tabs: tabList)
        {
            if (tabs.getText().equalsIgnoreCase(tab))
            {
                tabs.click();
                logger.info("Clicked on tab: " + tab);
                break;
            }
        }
        waitForLoadingImageDisappear();
    }

    public void clickNewCompanyBtn(){
        waitForElementToBeDisplayed(createNewCompany);
        createNewCompany.click();
    }

    public void inputCompanyName(String companyName){
        waitForElementToBeDisplayed(this.companyName);
        this.companyName.sendKeys(companyName);
    }
    public void inputCompanyType(String type){
        this.type.clear();
        waitForElementToBeDisplayed(this.type);
        this.type.sendKeys(type);
    }

    public void inputCompanySubType(String subType){
        waitForElementToBeDisplayed(this.subType);
        this.subType.sendKeys(subType);
    }

    public void inputDear(String dear){
        waitForElementToBeDisplayed(this.dear);
        this.dear.sendKeys(dear);
    }
    public void inputCompanyBusinessType(String businessType){
        waitForElementToBeDisplayed(this.businessType);
        this.businessType.sendKeys(businessType);
    }
    //Set status in the companies page - by default it is inactive
    public void inputCompanyStatus(String status){
        waitForElementToBeDisplayed(this.businessType);
        this.businessType.sendKeys(status);
    }
    public void inputCompanySector(String sector){
        waitForElementToBeDisplayed(this.sector);
        this.sector.sendKeys(sector);
    }
    public void inputCompanySource(String source){
        waitForElementToBeDisplayed(this.source);
        this.source.sendKeys(source);
    }
    public void inputCompanySalesBU(String salesBU){
        waitForElementToBeDisplayed(this.salesBU);
        this.salesBU.sendKeys(salesBU);
    }

    public void getReportingGroupPopupWindow(){
        waitForElementToBeDisplayed(rptGroupIcon);
        rptGroupIcon.click();
    }
    public void getAvailableReportingGroup(){
        waitForElementToBeDisplayed(rgShowAvlBtn);
        rgShowAvlBtn.click();
    }
    public void searchAndAddReportingGroup(String searchInput) throws Exception{
        waitForElementToBeDisplayed(rgQueryBtn , "Reporting Group Query Btn");
        findElement(this.searchInput).sendKeys(searchInput);
        findElement(rgGoBtn).click();
        waitForElementToBeClickable(addRG, "Add Reporting Group Button to be clickable");
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("scroll(865, 494)");
        executor.executeScript("arguments[0].click();", addRG);
        waitForElementToBeDisplayed(primaryRG, "Primary Reporting Group");
        findElement(rgOkBtn).click();
        waitForElementToDisappear(repGroupOK);
    }

    public void getIndustryCodePopupWindow(){
        waitForElementToBeDisplayed(industryCode, "Industry Code Popup Window");
        industryCode.click();
    }

    public void createIndustryCode(String industryCode, String value){
        waitForElementToBeDisplayed(indCodesNewBtn, "IndustryCode New Button");
        indCodesNewBtn.click();
        findElement(indCodeType).sendKeys(industryCode);
        waitForElementToBeDisplayed(indCode, "Industry Code");
        indCode.click();
        findElement(indCodeInput).sendKeys(value);
        findElement(pickIndCodeBtn).click();
        findElement(indCodeSaveBtn).click();
        waitForElementToBeDisplayed(industryCdText, "Industry Code Text");
    }

    public void saveIndustryCodes(){
        findElement(indCodeOkBtn).click();
        waitForElementToDisappear(industryCodesOK);
    }

    public void saveCompany(){
        waitForElementToBeDisplayed(saveCompanyBtn);
        saveCompanyBtn.click();
        waitForSaveImageDisappear();
    }

    public String getSiebelId(){
        return findElement(siebelId).getAttribute("value");
    }

    public String getCompanyName(){

        return findElement(companyName).getAttribute("value");
    }

    public String generateCompanyName(){
        String name = SiebelUtil.generateRandomText("Company", 2) + "_"+ SiebelUtil.generateRandomNumber(9999);
        return name;
    }

    public void waitForHomePageToLoad() {
        waitForElementToBeDisplayed(home, "home");
    }

    public void changePosition() {
        findElement(changePos).click();
        waitForElementToBeDisplayed(text, "Change Position");
        findElement(mrbSales).click();
        findElement(changePosButton).click();

    }

    public void getDetails() {
        waitForElementToBeDisplayed(companyGoBtn);
        companyGoBtn.click();
    }
    public void clickCompanyGoBtn() {
        waitForElementToBeDisplayed(companyGoBtn);
        companyGoBtn.click();
    }

    public void clickCompanyQueryBtn(){
        waitForElementToBeDisplayed(companyQryBtn, "Company Query Button");
        companyQryBtn.click();
    }

    public void inputSiebelId(String siebelId){
        waitForElementToBeDisplayed(mblIdColumn);
        mblIdColumn.click();
        waitForElementToBeDisplayed(mblContactId, "Siebel ID input");
        mblContactId.sendKeys(siebelId);
    }

    public void getCompanyDetails(String companyName){
        By company = By.xpath("//a[text()='"+companyName+"']");
        waitForElementToAppear(company);
        findElement(By.xpath("//a[text()='"+companyName+"']")).click();
    }

    public void clickGetAddresses(){
        waitForElementToBeDisplayed(address, "Addresses");
        address.click();
    }

    public void clickAddAddress() {
        String parentWindow = driver.getWindowHandle();
        logger.info("**** parent window handle is ----"+parentWindow );
        scrollPageDown();
        waitForElementToBeDisplayed(addAddress);
        addAddress.click();
        int timeCount = 1;
        do
        {
            driver.getWindowHandles();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeCount++;
            if ( timeCount > 50 )
            {
                break;
            }
        }
        while ( driver.getWindowHandles().size() == 1 );
        for (String windHandle: driver.getWindowHandles())
        {
            logger.info("****   windows ----"+windHandle);
            if(!parentWindow.equalsIgnoreCase(windHandle)){
                logger.info("**** child window handle ----"+windHandle);
                driver.switchTo().window(windHandle);
            }
        }
        WebElement qasFrame = driver.findElement(By.name("top"));
        driver.switchTo().frame(qasFrame);
        driver.findElement(By.id("searchText")).sendKeys("Unit 33 70 Kent Street, DEAKIN ACT");
        waitForElementToBeDisplayed(searchAddress);
        searchAddress.click();
        waitForElementToBeDisplayed(searchText);
        WebElement qasResult = driver.findElement(By.name("results"));
        driver.switchTo().frame(qasResult);
        waitForElementToBeDisplayed(streetAddr, "Address");
        streetAddr.click();
        driver.switchTo().defaultContent();
        driver.switchTo().frame(qasFrame);
        WebElement addressType = driver.findElement(By.name("AddressType"));
        Select addressTpSelect = new Select(addressType);
        addressTpSelect.selectByVisibleText("Business");
        waitForElementToBeDisplayed(acceptBtn);
        acceptBtn.click();
        driver.switchTo().window(parentWindow);
        waitForPageToLoad();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void addAddress(String address){
        waitForElementToBeDisplayed(allAddressesText, "All Addresses Text");
        String parentWindow = driver.getWindowHandle();
        findElement(addAddressBtn).click();
        for (String windHandle: driver.getWindowHandles())
        {
            driver.switchTo().window(windHandle);
        }
        waitForPageToLoad();
        //Performing the operation
        JavascriptExecutor js = (JavascriptExecutor)driver;
        String viewSource =  js.executeScript("return document.documentElement.innerHTML;").toString();
        System.out.println(viewSource);

        driver.close();
        driver.switchTo().window(parentWindow);
    }
    public void clickContactInformation() {
        waitForElementToBeDisplayed(telecoms, "telecoms");
        telecoms.click();
        waitForElementToBeDisplayed(phoneNewBtn, "Add New Phone");
    }

    public void clickAddNewPhoneButton() {
        findElement(phoneNewBtn).click();
    }

    public void inputArbn(String arbn){
        waitForElementToBeDisplayed(this.arbn);
        this.arbn.sendKeys(arbn);
    }

    public void addPhoneType(String type) {
        waitForElementToBeDisplayed(phoneType, "Phone Type td");
        waitForElementToBeClickable(phoneType);
        phoneType.click();
        waitForElementToBeDisplayed(phoneTpName);
        phoneTpName.sendKeys(type);

    }

    public void addMobilePhone(){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("scroll(284, 696)");
        executor.executeScript("arguments[0].click();", findElement(phoneType));
        waitForElementToBeDisplayed(phoneTpName);
        phoneTpName.sendKeys("Mobile");

    }

    public void addPhoneNumber(String number) {
        waitForElementToBeDisplayed(phoneNum);
        phoneNum.click();
        waitForElementToBeClickable(phoneNumberInput);
        phoneNumberInput.sendKeys(number);
    }

    public void inputAbsCode(String absCode) {
       waitForElementToBeDisplayed(this.absCode);
       this.absCode.sendKeys(absCode);
    }

    public void inputAbn(String abn) {
        waitForElementToBeDisplayed(this.abn);
        this.abn.sendKeys(abn);
    }

    public void inputAcn(String acn) {
        waitForElementToBeDisplayed(this.acn);
        this.acn.sendKeys(acn);
    }

    public void inputIndustrySoftware(String industrySoftware) {
        waitForElementToBeDisplayed(this.industrySoftware);
        this.industrySoftware.sendKeys(industrySoftware);
    }

    public void inputaccSoftware(String accSoftware) {
        waitForElementToBeDisplayed(this.accSoftware);
        this.accSoftware.sendKeys(accSoftware);
    }

    public void checkTermAgreement() {
        waitForElementToBeDisplayed(this.termAgreement);
        this.termAgreement.click();
    }
    public void inputBusinessCode(String businessCode){
        waitForElementToBeDisplayed(this.businessCode);
        this.businessCode.sendKeys(businessCode);
    }
    public void inputBusinessSize(String businessSize){
        waitForElementToBeDisplayed(this.businessSize);
        this.businessSize.sendKeys(businessSize);
    }

    public void inputPivotBU(String pivotBU){
        waitForElementToBeDisplayed(this.pivotBU);
        this.pivotBU.sendKeys(pivotBU);
    }

    public void inputPivotBusinessUnit(String pivotUnit){
        waitForElementToBeDisplayed(this.pivotBU);
        this.pivotBU.clear();
        this.pivotBU.sendKeys(pivotUnit + Keys.ENTER);
    }

    public void inputABClassification(String abClassification){
        waitForElementToBeDisplayed(this.abClassification);
        this.abClassification.sendKeys(abClassification);
    }

    public void inputCustomerCategory(String customerCategory){
        waitForElementToBeDisplayed(this.customerCategory);
        this.customerCategory.sendKeys(customerCategory);
    }

    public void clickMoreInfo() {
        waitForElementToBeDisplayed(moreInfo);
        moreInfo.click();
    }

    public void inputlegislationType(String legislationType){
        waitForElementToAppear(this.legislationTpText);
        waitForElementToBeDisplayed(this.legislationType);
        this.legislationType.clear();
        this.legislationType.sendKeys(legislationType);
    }

    public void inputlegislationLoc(String legislationLoc){
        waitForElementToBeDisplayed(this.legislationLoc);
        this.legislationLoc.sendKeys(legislationLoc);
    }

    public void inputTrustType(String trustType){
        waitForElementToBeDisplayed(this.trustType);
        this.trustType.sendKeys(trustType);
    }

    public void inputParentCompany(String parentCompany){
        waitForElementToBeDisplayed(this.parentCompany);
        this.parentCompany.sendKeys(parentCompany);
    }

    public void inputParentExchange(String parentExchange){
        waitForElementToBeDisplayed(this.parentExchange);
        this.parentExchange.sendKeys(parentExchange);
    }

    public void inputExchange(String exchange){
        waitForElementToBeDisplayed(this.exchange);
        this.exchange.sendKeys(exchange);
    }

    public void inputMembershipBody(String membershipBody){
        waitForElementToBeDisplayed(this.membershipBody);
        this.membershipBody.sendKeys(membershipBody);
    }

    public void inputRegulatorName(String regulatorName){
        waitForElementToBeDisplayed(this.regulatorName);
        this.regulatorName.sendKeys(regulatorName);
    }

    public void inputRegulatorId(String regulatorId){
        waitForElementToBeDisplayed(this.regulatorId);
       this.regulatorId.sendKeys(regulatorId);
    }

    public void saveRecord() {
        Actions keyAction = new Actions(driver);
        keyAction.keyDown(Keys.CONTROL).sendKeys("S").perform();
    }

    public void pushToPivot() {
        waitForElementToBeDisplayed(pushToPivot);
        pushToPivot.click();
    }

    public void addEmailAddress() {
    }



    public void inputStatus(String status){
        waitForElementToBeDisplayed(this.status);
        this.status.clear();
        this.status.sendKeys(status);

    }

    public void addAnotherPhoneType(String mobile) {
        waitForElementToBeDisplayed(primaryPhone);
        waitForElementToBeDisplayed(phoneType, "Phone Type td");
        waitForElementToBeClickable(phoneType);
        phoneType.click();
        waitForElementToBeDisplayed(phoneTpName);
        phoneTpName.sendKeys(mobile);

    }

    public void updatePrimaryEmail(String email) {
        Actions actions = new Actions(driver);
        actions.moveToElement(primaryEmail);
        actions.click();
        actions.moveToElement(primaryEmailInput);
        actions.sendKeys(Keys.HOME,Keys.chord(Keys.SHIFT,Keys.END),email);
        actions.build().perform();
    }

    public void clickSalesRepIcon(){
        waitForElementToBeDisplayed(salesRepIcon);
        salesRepIcon.click();
    }

    public void clickShowAvailableButton(){
        waitForElementToBeClickable(showAvailableBtn);
        showAvailableBtn.click();
    }

    public void inputSalesRepName(String lastName){
        waitForElementToBeDisplayed(rgSpanText);
        waitForElementToBeDisplayed(searchSalesRepInput);
        searchSalesRepInput.sendKeys(lastName);
    }

    public void searchSalesRep(){
        //waitForElementToBeDisplayed(salesRepGoBtn);
        waitForElementToBeClickable(salesRepGoBtn);
        salesRepGoBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void clickAddSaleRepButton(){
        waitForElementToBeClickable(addSalesRepBtn , "Add Sales Representative");
        addSalesRepBtn.click();
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
    }

    public void selectPrimarySalesRep() {
        for (WebElement salesRep : salesRepList) {
            List<WebElement> salesRepTdCells = salesRep.findElements(By.tagName("td"));
            int index = 0;
            for (WebElement e : salesRepTdCells) {
                index = index + 1;
                if (index == 2) {
                    //check title = 'Y'
                    String titleValue = e.getAttribute("title");
                    if (titleValue.equalsIgnoreCase("UnChecked") || titleValue.equalsIgnoreCase("N")) {
                        e.click();
                        logger.info("*** clicked the td - title column");
                        WebElement primaryChkBox =  e.findElement(By.tagName("input"));
                        primaryChkBox.click();
                        primaryChkBox.click();
                        logger.info("*** clicked the check box , now break from the loop -title column");
                        break;
                    }
                }
            }
        }
    }

    public void clickSalesRepOkButton(){
        waitForSaveImageDisappear();
        waitForLoadingImageDisappear();
        waitForElementToBeDisplayed(salesRepOKBtn);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("scroll(907, 177)");
        executor.executeScript("arguments[0].click();",  salesRepOKBtn);
    }

    public void clickOkButton(){
        waitForElementToBeClickable(salesRepOKBtn);
        salesRepGoBtn.click();
    }

    public void removeNonPrimaryField() {
        for (WebElement salesRep : salesRepList) {
            List<WebElement> salesRepTdCells = salesRep.findElements(By.tagName("td"));
            int index = 0;
            for (WebElement e : salesRepTdCells) {
                index = index + 1;
                if (index == 2) {
                    //check title = 'Y'
                    String titleValue = e.getAttribute("title");
                    if (titleValue.equalsIgnoreCase("UnChecked") || titleValue.equalsIgnoreCase("N")) {
                        removeNonPrimarySalesRep(salesRepTdCells);
                    }
                }
            }
        }
    }

    private void removeNonPrimarySalesRep(List<WebElement> salesRepTdCells) {
        logger.info("*** Removing Sales Rep");
        WebElement salesRepName = salesRepTdCells.get(3);
        salesRepName.click();
        waitForElementToBeClickable(removeBtn);
        removeBtn.click();
    }
    public void clickSystemLinks(){
        Actions actions = new Actions(driver);
        actions.moveToElement(systemLinks).click().perform();
    }

    public String getPivotId() {
        //Get the list of integrated systems.
        String pivotId = null;
        int index = 0;
        int ix = 0;
        logger.info("**** Number of  systems ---"+systemList.size());
        for(WebElement system : systemList){ // List of system links rows -tr
            ix = ix + 1 ;
            logger.info("**** systemList index is ---"+ix+" \t ****");
            List<WebElement> systemTdColumns = system.findElements(By.tagName("td")); //Column List - td List
            for(WebElement column : systemTdColumns){ // Iterate through each column - td
                logger.info("****  system column  -- column title ---"+column.getAttribute("title"));
                index = index + 1;
//                if(index == 3){
                    String titleValue = column.getAttribute("title");
                    logger.info("**** Title Value  of 3rd column----"+titleValue);
                    if((titleValue != null) && (titleValue.equalsIgnoreCase("Pivot"))){
                        pivotId = clickPivotIdColumn(systemTdColumns);
                        break;
                    }
//                }
            }
        }
        return pivotId;
    }

    private String clickPivotIdColumn(List<WebElement> systemTdColumns) {
        String titleValue = null;
        try {
            WebElement systemLinkTd = systemTdColumns.get(4);
            titleValue = systemLinkTd.getText();
            systemLinkTd.click();
            logger.info("**** Tilte Value is ----"+titleValue);
            Thread.sleep(4000);
        }catch (Exception e){
            e.printStackTrace();
        }
        return  titleValue;
    }

    public void clickAddAddress(String address , String addressType) {
        String parentWindow = driver.getWindowHandle();
        logger.info("**** parent window handle is ----"+parentWindow );
        scrollPageDown();
        waitForElementToBeDisplayed(addAddress);
        waitForElementToBeClickable(addAddress);
        addAddress.click();
        int timeCount = 1;
        do
        {
            driver.getWindowHandles();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeCount++;
            if ( timeCount > 50 )
            {
                break;
            }
        }
        while ( driver.getWindowHandles().size() == 1 );
        for (String windHandle: driver.getWindowHandles())
        {
            logger.info("****   windows ----"+windHandle);
            if(!parentWindow.equalsIgnoreCase(windHandle)){
                logger.info("**** child window handle ----"+windHandle);
                driver.switchTo().window(windHandle);
            }
        }
        WebElement qasFrame = driver.findElement(By.name("top"));
        driver.switchTo().frame(qasFrame);
        driver.findElement(By.id("searchText")).clear();
        driver.findElement(By.id("searchText")).sendKeys(address);

        waitForElementToBeDisplayed(searchAddress);
        searchAddress.click();
        WebElement  addressTp = driver.findElement(By.id("AddressType"));
        Select addressTpSelect = new Select(addressTp);
        if(addressType == null){
            addressType = "Business";
        }
        addressTpSelect.selectByVisibleText(addressType);
        waitForElementToBeDisplayed(acceptBtn);
        acceptBtn.click();
        logger.info("**** Click Accept Button ----");
        driver.switchTo().window(parentWindow);
        waitForPageToLoad();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void searchCompanyByName(String companyName) {
        waitForElementToBeDisplayed(searchByCompanyName);
        searchByCompanyName.click();
        waitForElementToBeDisplayed(inputCompanyName);
        inputCompanyName.sendKeys(companyName);
    }

    public void selectRecordsToMerge() {
        waitForElementToBeDisplayed(sourceCompany);
        waitForElementToBeDisplayed(destinationCompany);
        Actions actions = new Actions(driver);
        actions.click(sourceCompany).keyDown(Keys.CONTROL).click(destinationCompany).perform();
    }

    public void clickCompaniesMenu(){
        waitForElementToBeDisplayed(companiesMenu);
        companiesMenu.click();
        waitForElementToBeDisplayed(mergeRecords);
        mergeRecords.click();
    }

    public void mergeCompanies() {
        waitForElementToBeDisplayed(mergeRecordsText);
        waitForElementToBeDisplayed(mergeRecordsOkButton);
        mergeRecordsOkButton.click();
        waitForLoadingImageDisappear();
        waitForSaveImageDisappear();
    }

    public void clickPrimaryOrgId(){
        waitForElementToBeDisplayed(sourceCompany);
        sourceCompany.click();
    }

    public String getPrimaryAddress() {
        String addressType = null;
        logger.info("**** Number of  Addresses ---"+allAddresses.size());
        for(WebElement address : allAddresses) {  // List of address rows - tr.
            List<WebElement> addressColumns = address.findElements(By.tagName("td"));
            if (addressColumns.get(1).getAttribute("title") != null) {
                if (addressColumns.get(1).getAttribute("title").equalsIgnoreCase("Y")) {
                    addressType = addressColumns.get(4).getText();
                    logger.info("*** Address Type is ---" + addressType);
                }
            } else {
                logger.info("****  Title attribute is null ----");
            }
        }
        return addressType;
    }

    public void clickFinancialAccount() {
        waitForElementToBeDisplayed(financialAccounts);
        financialAccounts.click();
        logger.info("Clicked on the Financial Assets tab for the Siebel");
    }

    public String getFacilityId() {
        waitForElementToBeDisplayed(facilityId);
        facilityId.click();
        String facility = facilityId.getAttribute("title");
        logger.info("Facility Id : " + facility);
        return facility;
    }

    public String getAccountNumber(){
        String accNum;
        waitForElementToBeDisplayed(accountNumber);
        accNum = accountNumber.getAttribute("title");
        logger.info("**** Account Number --"+accNum);
        return  accNum;
    }

    public String getFacilityStatus() {
        waitForElementToBeDisplayed(facilityStatus);
        facilityStatus.click();
        String facilitySts = facilityStatus.getAttribute("title");
        logger.info("**** Facility Status ---"+facilitySts);
        return facilitySts;
    }

    public String getAccountStatus() {
        waitForElementToBeDisplayed(accountStatus);
        accountStatus.click();
        String accStatus = accountStatus.getAttribute("title");
        logger.info("**** Account Status ---"+accStatus);
        return accStatus;
    }

    public void clickRelationshipTab() {
        waitForElementToBeDisplayed(relationships);
        relationships.click();
    }

    public void clickReportGroup(){
        waitForElementToBeDisplayed(reportGroupName);
        reportGroupName.click();
    }

    public String getAdditionalAccountNumber(){
        waitForElementToBeDisplayed(additionalAccNumber);
        logger.info("**** Additional Account Number ---"+additionalAccNumber.getAttribute("title"));
        return  additionalAccNumber.getAttribute("title");
    }
}
