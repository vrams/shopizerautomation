package com.macquarie.bfs.siebel.pivot;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.winium.WiniumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import winium.elements.desktop.ComboBox;
import winium.elements.desktop.extensions.WebElementExtensions;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.security.Key;

/**
 * Created by vthaduri on 17/10/2017.
 */
public class PivotFacilityWindow extends BaseWindow {

    private static Logger logger = LoggerFactory.getLogger(PivotFacilityWindow.class);

    public PivotFacilityWindow(WiniumDriver windowDriver) {
        super(windowDriver);
    }

    @FindBy(xpath = "//*[@AutomationId='36']")
    WebElement reportingName;

    @FindBy(xpath = "//*[@AutomationId='31']")
    WebElement expiryDate;

    @FindBy(className = "PBTabControl32_60")
    WebElement tab;

    @FindBy(name = "File")
    WebElement fileMenu;

    @FindBy(name = "Save")
    WebElement saveMenu;

    @FindBy(xpath = "//*[@AutomationId='TitleBar']")
    WebElement titleBar;

    @FindBy(xpath = "//*[@AutomationId='Close']")
    private WebElement closeWindow;

    @FindBy(xpath = "//*[@AutomationId='Item 1']")
    private WebElement facilityFileMenu;

    @FindBy(name = "New")
    private WebElement newMenu;

    @FindBy(xpath = "//*[@AutomationId='Item 600']")
    private WebElement account;

    @FindBy(xpath = "//*[@AutomationId='33']")
    private WebElement loanPurposeCode;

    @FindBy(xpath = "//*[@AutomationId='Item 614']")
    private WebElement closeFacilityWindow;

    @FindBy(xpath = "//*[@AutomationId='1001']")
    private WebElement facilityNumber;

    @FindBy(xpath = "//*[@AutomationId='1000']")
    private WebElement findFacility;

    @FindBy(xpath = "//*[@AutomationId='Item 506']")
    private WebElement openFacilityDetails;

    @FindBy(xpath = "//*[@AutomationId='Item 2']")
    private WebElement gotoMenu;

    @FindBy(xpath = "//*[@AutomationId='Item 555']")
    private WebElement searchFacility;

    @FindBy(xpath = "//*[@AutomationId='1000']")
    private WebElement inputFacilityId;

    @FindBy(xpath = "//*[@AutomationId='1001']")
    private WebElement clickFacilityOkBtn;

    @FindBy(name = "OK")
    private WebElement okBtn;

    @FindBy(name = "Close")
    private WebElement closeButton;

    public void setReportingName(String name)
    {
        tab.sendKeys(Keys.TAB);
        waitFor(1000);
        pressKeyCombination(KeyEvent.VK_ALT, KeyEvent.VK_T);
        pressKey(KeyEvent.VK_TAB);
        reportingName.sendKeys(name);
        logger.info("Input Reporting Name:" + name);
    }

    public void setExpiryDate(String date)
    {
        pressKeyCombination(KeyEvent.VK_ALT, KeyEvent.VK_X);
        expiryDate.sendKeys(date);
        logger.info("Input Expiry Date: " + date);
    }

    public void clickLoanTab()
    {
        tab.click();
        pressKeyCombination(KeyEvent.VK_CONTROL, KeyEvent.VK_TAB);
        logger.info("Navigated to Interest Tab");
        pressKeyCombination(KeyEvent.VK_CONTROL, KeyEvent.VK_TAB);
        logger.info("Navigated to Loan Tab");
    }

    public void selectFundingOptions()
    {
        pressKeyCombination(KeyEvent.VK_ALT, KeyEvent.VK_SHIFT, KeyEvent.VK_S);
        pressKey(KeyEvent.VK_UP);
        pressKey(KeyEvent.VK_DOWN);
        pressKey(KeyEvent.VK_DOWN);
        waitFor(1000);
        logger.info("Selected one option in Funding Options");
    }

    public void selectInterestRateOptions()
    {
        pressKeyCombination(KeyEvent.VK_ALT, KeyEvent.VK_SHIFT, KeyEvent.VK_V);
        pressKey(KeyEvent.VK_UP);
        pressKey(KeyEvent.VK_DOWN);
        pressKey(KeyEvent.VK_DOWN);
        waitFor(1000);
        logger.info("Selected one option in Interest Rate Options");
    }

    public void clickLimitsTab()
    {
        tab.click();
        pressKeyCombination(KeyEvent.VK_CONTROL, KeyEvent.VK_TAB);
        logger.info("Navigated to Limits Tab");
    }

    public void selectLoanPurposeGroup()
    {
        pressKeyCombination(KeyEvent.VK_ALT, KeyEvent.VK_U);
        ComboBox comboBox;
        comboBox = WebElementExtensions.toComboBox(windowDriver.findElement(By.xpath("//*[@AutomationId='13']")));
        comboBox.click();
        pressKey(KeyEvent.VK_DOWN);
        comboBox.click();
        logger.info("Selected one option in the Loan Purpose Group");
    }

    public void selectLoanPurposeCode()
    {
        pressKeyCombination(KeyEvent.VK_ALT, KeyEvent.VK_C);
        ComboBox comboBox;
        comboBox = WebElementExtensions.toComboBox(windowDriver.findElement(By.xpath("//*[@AutomationId='14']")));
        comboBox.click();
        pressKey(KeyEvent.VK_DOWN);
        comboBox.click();
        logger.info("Selected one option in the Loan Purpose Code");
    }

    public void saveFacility()
    {
        pressKeyCombination(KeyEvent.VK_CONTROL, KeyEvent.VK_S);
        logger.info("Saved facility");
    }

    public String getFacilityNumber()
    {
        String facilityId = titleBar.getAttribute("Name");
        logger.info("Facility Number: " + facilityId.split("-")[0].trim());
        return facilityId.split("-")[0].trim();
    }

    public void setCloseWindow()
    {
        waitFor(1000);
        closeWindow.click();
    }


    public void clickFacilityFileMenu() {
     pressKeyCombination(KeyEvent.VK_ALT, KeyEvent.VK_F, KeyEvent.VK_ENTER);
    }

    public void clickNewAccountOption(){
        newMenu.click();
        waitFor(1000);
    }

    public void clickFacilityTitleBar() {
        titleBar.click();
    }

    public void closeFacilityWindow(){
        closeButton.click();
    }

    public void searchFacility(String facilityId){
        waitFor(1000);
        pressKey(KeyEvent.VK_TAB);
        waitFor(1000);
        facilityNumber.sendKeys(facilityId);
    }

    public void findFacility(){
       waitFor(500);
       findFacility.click();
       waitFor(500);
    }

    public void getFacilityDetails(){
        waitFor(500);
        facilityNumber.click();
        pressKey(KeyEvent.VK_TAB, 3);
        waitFor(1000);
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_F);
    }

    public void clickFacilityMenu() {
        pressKey(KeyEvent.VK_DOWN, 2);
        pressKey(KeyEvent.VK_RIGHT);
        pressKeyCombination(KeyEvent.VK_DOWN, KeyEvent.VK_ENTER);
    }

    public void inputFacilityId(String facilityId){
        waitFor(1000);
        inputFacilityId.click();
        waitFor(1000);
        inputFacilityId.sendKeys(facilityId);
    }

    public void clickFacilityOkButton(){
        clickFacilityOkBtn.click();
        waitFor(2000);
    }

    public void selectModifyFacilityOption() {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_F);
        pressKey(KeyEvent.VK_ENTER);
        pressKey(KeyEvent.VK_DOWN,2);
        pressKey(KeyEvent.VK_ENTER);
        waitFor(2000);
        pressKey(KeyEvent.VK_TAB);
    }

    public void editFacility()
    {
       pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_U);
       pressKey(KeyEvent.VK_UP,2);
    }

    public void selectFacilityStatus()
    {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_U);
        pressKey(KeyEvent.VK_UP,2);
    }

    public void clickAccountLimitBtn(){
        waitFor(1000);
        okBtn.click();
        waitFor(1000);
    }

    public void saveFacilityValues() {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_F);
        pressKey(KeyEvent.VK_ENTER);
        pressKey(KeyEvent.VK_DOWN,3);
        pressKey(KeyEvent.VK_ENTER);
    }

    public void changeFacilityValues() {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_F);
        pressKey(KeyEvent.VK_ENTER);
        pressKey(KeyEvent.VK_DOWN,3);
        pressKey(KeyEvent.VK_ENTER);
    }

    public void closeFacilityEditWindow() {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_F);
        pressKey(KeyEvent.VK_ENTER);
        pressKey(KeyEvent.VK_DOWN,8);
        pressKey(KeyEvent.VK_ENTER);
    }

    public void clickModifyFacility()
    {
        pressKeyCombination(KeyEvent.VK_CONTROL,KeyEvent.VK_M);
    }
}
