
package com.macquarie.bfs.siebel.pivot;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.winium.WiniumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import winium.elements.desktop.ComboBox;
import winium.elements.desktop.extensions.WebElementExtensions;

import java.awt.event.KeyEvent;

/**
 * Created by vthaduri on 17/10/2017.
 */
public class PivotLoginWindow extends BaseWindow {

    private static Logger logger = LoggerFactory.getLogger(PivotLoginWindow.class);

    public PivotLoginWindow(WiniumDriver windowDriver) {
        super(windowDriver);
    }

    @FindBy(name = "Log on to Pivot")
    private WebElement loginWindowTitle;

    @FindBy(xpath = "//*[@AutomationId='1002']")
    private WebElement username;

    @FindBy(xpath = "//*[@AutomationId='1003']")
    private WebElement password;

    @FindBy(xpath = "//*[@AutomationId='1005']")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@AutomationId='1008']")
    private WebElement dataSource;

    //@FindBy(xpath = "//*[@AutomationId='6']")
    @FindBy(name = "Yes")
    private WebElement securityYesButton;

    @FindBy(xpath = "//*[@AutomationId='Maximize']")
    private WebElement maximise;

    @FindBy(xpath = "//*[@AutomationId='Close']")
    private WebElement closeWindow;

    public void clickLoginWindow()
    {
        loginWindowTitle.click();
        logger.info("Clicked on the the Login window title to set focus");
    }

    public void maximiseWindow(){
        waitFor(1000);
        maximise.click();
    }

    public void inputUsername(String user)
    {
        String text = username.getText();
        if(!text.isEmpty())
        {
            for(int i = 0; i <= text.length();i++) {
                username.clear();
            }
        }
        username.sendKeys(user);
        logger.info("Input Username: " + user);
    }

    public void inputPassword(String pass)
    {
        waitFor(1000);
        windowDriver.findElement(By.xpath("//*[@AutomationId='1003']")).sendKeys(pass);
        waitFor(1000);
    }

    public void clickLoginButton()
    {
        loginButton.click();
        waitFor(4000);
        logger.info("Clicked on login button");
    }

    public void selectDatasource(String source)
    {
        ComboBox comboBox;
        comboBox = WebElementExtensions.toComboBox(windowDriver.findElement(By.xpath("//*[@AutomationId='1008']")));
        comboBox.click();
        comboBox.findElementByName(source).click();
    }

    public void clickSecurityYesButton()
    {
        //waitFor(2000);
        //securityYesButton.click();
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_Y);
        logger.info("Clicked on Yes button on security dialog box");
    }

    public void closePivotApplication()
    {
        closeWindow.click();
        logger.info("Closed Pivot Application");
    }
}
