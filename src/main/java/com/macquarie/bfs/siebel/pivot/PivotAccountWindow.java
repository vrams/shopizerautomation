package com.macquarie.bfs.siebel.pivot;

/**
 * Created by rkurian on 30/10/2017.
 */

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.winium.WiniumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import winium.elements.desktop.ComboBox;
import winium.elements.desktop.extensions.WebElementExtensions;

import java.awt.event.KeyEvent;

/**
 * Created by vthaduri on 17/10/2017.
 */
public class PivotAccountWindow extends BaseWindow {

    private static Logger logger = LoggerFactory.getLogger(PivotAccountWindow.class);

    public PivotAccountWindow(WiniumDriver windowDriver) {
        super(windowDriver);
    }

    @FindBy(xpath = "//*[@AutomationId='Item 1']")
    private WebElement facilityFileMenu;

    @FindBy(name = "New")
    private WebElement newMenu;

    @FindBy(xpath = "//*[@AutomationId='Item 600']")
    private WebElement account;

    @FindBy(xpath = "//*[@AutomationId='1000']")
    private WebElement loanPurposeCode;

/*    @FindBy(xpath = "/*//*[@AutomationId='1000']")
    private WebElement loanCode140;*/

    @FindBy(xpath = "//*[@AutomationId='34']")
    private WebElement accountLimit;

    @FindBy(xpath = "//*[@AutomationId='Item 708']")
    private WebElement saveAccount;

    @FindBy(xpath = "//*[@AutomationId='2']")
    private WebElement nominatedAccountOk;

    @FindBy(name = "OK")
    private WebElement accountLimitValue;

    @FindBy(xpath = "//*[@AutomationId='TitleBar']")
    WebElement titleBar;

    @FindBy(xpath = "//*[@AutomationId='Item 717']")
    WebElement closeAccountWindow;

    @FindBy(xpath = "//*[@AutomationId='1000']")
    private WebElement inputAccountId;

    @FindBy(xpath = "//*[@AutomationId='1001']")
    private WebElement clickAccountOkBtn;

    @FindBy(name = "Yes")
    private WebElement yesButton;

    @FindBy(name = "OK")
    private WebElement okBtn;

    @FindBy(xpath = "//*[@AutomationId='Close']")
    private WebElement closeWindow;

    @FindBy(name = "Close")
    private WebElement closeButton;

    public void clickAccounts(){
        account.click();
    }

    public void selectLoanPurposeCode(){
        pressKey(KeyEvent.VK_TAB, 16);
        pressKey(KeyEvent.VK_DOWN);
        waitFor(1000);
        pressKey(KeyEvent.VK_TAB);
        waitFor(1000);
    }

    public void inputAccountLimit(String accountLt){

        accountLimit.sendKeys(accountLt);
    }

    public void saveAccount(){
        pressKeyCombination(KeyEvent.VK_CONTROL, KeyEvent.VK_S);
    }

    public void nominateAccountOk(){
        okBtn.click();
        waitFor(1000);
    }

    public void clickAccountFileMenu() {

        pressKeyCombination(KeyEvent.VK_ALT, KeyEvent.VK_F, KeyEvent.VK_ENTER);
    }

    public void accountLimitValue() {
      okBtn.click();
      waitFor(1000);
    }

    public String getAccountNumber(){
        String name = titleBar.getAttribute("Name");
        String[] accountName = name.split(" ");
        logger.info("**** accountName[0]"+accountName[0]+"---- accountName[1]---"+accountName[1]);
        String accountId = accountName[0] + accountName[1];
        logger.info("**** Account Id ---"+accountId);
        return accountId;
    }

    public void closeAccountWindow() {

        titleBar.click();
        closeButton.click();
    }

    public void clickAccountMenu(){
        waitFor(1000);
        pressKey(KeyEvent.VK_DOWN, 1);
        pressKey(KeyEvent.VK_RIGHT);
        pressKey(KeyEvent.VK_DOWN, 2);
        pressKey(KeyEvent.VK_ENTER);
    }

    public void inputAccountId(String accountId){
        waitFor(1000);
        inputAccountId.click();
        inputAccountId.sendKeys(accountId);
    }

    public void clickAccountOk(){
        clickAccountOkBtn.click();
        waitFor(1000);
    }

    public void selectModifyAccountOption() {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_F);
        waitFor(1000);
        //pressKey(KeyEvent.VK_ENTER);
        pressKey(KeyEvent.VK_DOWN,2);
        waitFor(1000);
        pressKey(KeyEvent.VK_ENTER);
        waitFor(2000);
        pressKey(KeyEvent.VK_TAB);
        waitFor(2000);
    }

    public void editAccount() {
        waitFor(2000);
        //pressKey(KeyEvent.VK_TAB,7);
        pressKey(KeyEvent.VK_TAB,1);
        waitFor(1000);
        pressKey(KeyEvent.VK_DOWN);
        waitFor(1000);
        pressKey(KeyEvent.VK_ENTER);
    }

    public void clickAccountLimitBtn(){
        waitFor(1000);
        okBtn.click();
        waitFor(1000);
    }

    public void saveAccountValues() {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_F);
        waitFor(1000);
        //pressKey(KeyEvent.VK_ENTER);
        pressKey(KeyEvent.VK_DOWN,3);
        pressKey(KeyEvent.VK_ENTER);
    }

    public void closeAccountEditWindow() {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_F);
        waitFor(1000);
        //pressKey(KeyEvent.VK_ENTER);
        //pressKey(KeyEvent.VK_LEFT,2);
        waitFor(1000);
        pressKey(KeyEvent.VK_DOWN,7);
        pressKey(KeyEvent.VK_ENTER);
    }

    public void setCloseWindow() {
        waitFor(1000);
        closeWindow.click();
    }

    public void clickAccountModify()
    {
        pressKeyCombination(KeyEvent.VK_CONTROL,KeyEvent.VK_M);
    }

    public void changeAccountStatus()
    {
        pressKey(KeyEvent.VK_TAB,2);
        pressKey(KeyEvent.VK_DOWN);
        yesButton.click();
        pressKeyCombination(KeyEvent.VK_CONTROL, KeyEvent.VK_S);
        okBtn.click();
    }

    public void gotoAccountNumberSearch()
    {
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_T);
        clickAccountMenu();
    }
}

