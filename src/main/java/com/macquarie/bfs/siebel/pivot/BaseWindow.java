package com.macquarie.bfs.siebel.pivot;

import com.macquarie.bfs.siebel.pages.BasePage;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.winium.WiniumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import winium.elements.desktop.ComboBox;
import winium.elements.desktop.extensions.WebElementExtensions;

import java.awt.*;
import java.util.List;

/**
 * Created by vthaduri on 17/10/2017.
 */
public class BaseWindow {
    private static Logger logger = LoggerFactory.getLogger(BasePage.class);

    protected WiniumDriver windowDriver;
    protected PropertiesConfiguration siebelProperties;
    protected int timeOutInSeconds = 0;
    protected int implicitWaitTimeOut = 0;

    /* Base window constructor */
    public BaseWindow(WiniumDriver windowDriver) {
        this.windowDriver = windowDriver;

        //this.windowDriver = new DownloadDrivers().getWindowDriver("ie");
        PageFactory.initElements(windowDriver, this);
        try {
            siebelProperties = new PropertiesConfiguration("ObjectRepository.properties"); //get the object repository property file
            timeOutInSeconds = Integer.valueOf(siebelProperties.getString("TimeOutInSeconds"));
            implicitWaitTimeOut = Integer.valueOf(siebelProperties.getString("ImplicitWaitTimeOut"));

        } catch (Exception e) {
            logger.info("Unable to find Object Repository");
            logger.info("*** Exception ---"+e.getMessage());
            e.printStackTrace();
        }
    }



    /* Returns the web windowDriver instance */
    public WiniumDriver getWindowDriver() {
        return this.windowDriver;
    }

    /* Function to wait for visibility of the element to be displayed*/
    public void waitForElementToBeDisplayed(WebElement element) {
        try {
            WebDriverWait wait = new WebDriverWait(windowDriver, timeOutInSeconds);// Driver wait time
            wait.until(ExpectedConditions.visibilityOf(element));
            if(element == null){
                logger.info("**** Element is NULL ----");
            }

        } catch (Throwable throwable) {
            throwable.getMessage();
        }
    }

   /* Function for selecting the dropdown text */
    public void selectDropdownText(String xpath, String selectText) {
        ComboBox comboBox;
        comboBox = WebElementExtensions.toComboBox(windowDriver.findElement(By.xpath(xpath)));
        comboBox.click();
        comboBox.findElementByName(selectText).click();
    }
    /* Function to impose wait for desired milli seconds of time */
    public void waitFor(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ie) {
            ie.getMessage();
        }
    }

    public void pressKey(int key, int noOfTimes)
    {
        try {
            Robot robot = new Robot();
            for (int i = 1; i <= noOfTimes; i++) {
                robot.keyPress(key);
                robot.keyRelease(key);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public void pressKey(int key)
    {
        try {
            Robot robot = new Robot();
            robot.keyPress(key);
            robot.keyRelease(key);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void pressKeyCombination(int key1, int key2)
    {
        try {
            Robot robot = new Robot();
            robot.keyPress(key1);
            robot.keyPress(key2);
            robot.keyRelease(key2);
            robot.keyRelease(key1);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public void pressKeyCombination(int key1, int key2, int key3)
    {
        try {
            Robot robot = new Robot();
            robot.keyPress(key1);
            robot.keyPress(key2);
            robot.keyPress(key3);
            robot.keyRelease(key3);
            robot.keyRelease(key2);
            robot.keyRelease(key1);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
