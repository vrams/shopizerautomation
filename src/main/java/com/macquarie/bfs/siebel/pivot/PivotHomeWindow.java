package com.macquarie.bfs.siebel.pivot;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.winium.WiniumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import winium.elements.desktop.ComboBox;
import winium.elements.desktop.extensions.WebElementExtensions;

import java.awt.event.KeyEvent;

/**
 * Created by vthaduri on 17/10/2017.
 */
public class PivotHomeWindow extends BaseWindow {

    private static Logger logger = LoggerFactory.getLogger(PivotHomeWindow.class);

    public PivotHomeWindow(WiniumDriver windowDriver) {
        super(windowDriver);
    }

    @FindBy(name = "File")
    private WebElement fileMenu;

    @FindBy(name = "New")
    private WebElement newMenu;

    //@FindBy(xpath = "//*[@AutomationId='Item 4']")
    @FindBy(name = "Tools")
    private WebElement toolsMenu;

    @FindBy(xpath = "//*[@AutomationId='Item 503']")
    private WebElement facilityMenu;

    @FindBy(xpath = "//*[@AutomationId='Item 558']")
    private WebElement findMenu;

    //@FindBy(xpath = "//*[@AutomationId='2']")
    @FindBy(name = "Customer Number")
    private WebElement customerNumberRadioButton;

    @FindBy(xpath = "//*[@AutomationId='1002']")
    private WebElement searchForTextBox;

    @FindBy(name = "Find")
    private WebElement findButton;

    @FindBy(xpath = "//*[@AutomationId='1001']")
    private WebElement searchResult;

    @FindBy(xpath = "//*[@AutomationId='1003']")
    private WebElement customerPane;

    @FindBy(xpath = "//*[@AutomationId='TitleBar']")
    WebElement titleBar;

    public void clickToolsMenu()
    {
         // toolsMenu.click();
        pressKeyCombination(KeyEvent.VK_ALT,KeyEvent.VK_T);
          logger.info("Clicked on Tools Menu");
    }


    public void clickFindMenu()
    {
        //findMenu.click();
        pressKey(KeyEvent.VK_DOWN,2);
        pressKey(KeyEvent.VK_ENTER);
        logger.info("Clicked on Find under Tools Menu");
    }

    public void clickCustomerNumberRadioButton()
    {
        waitFor(1000);
        customerNumberRadioButton.click();
        logger.info("Clicked on the customer number radio button");
    }

    public void setSearchForTextBox(String customerNumber)
    {
        searchForTextBox.sendKeys(customerNumber);
        logger.info("Clicked on the customer number radio button");
    }

    public void clickFindButton()
    {
        findButton.click();
        logger.info("Clicked on Find button in search window");
    }

    public void clickSearchResult()
    {
        searchResult.click();
        logger.info("Selected Search Result");
        waitFor(1000);
    }

    public void clickFileMenu()
    {
        fileMenu.click();
        logger.info("Clicked on File menu");
    }

    public void clickNewMenu()
    {
        newMenu.click();
        logger.info("Clicked on New menu");
    }

    public void clickFacilityMenu()
    {
        facilityMenu.click();
        logger.info("Clicked on Facility menu");
        try {
            Thread.sleep(4000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clickCustomerPane(){
        waitFor(1000);
        customerPane.click();
        waitFor(1000);
        //pressKey(KeyEvent.VK_RIGHT);
    }


}
