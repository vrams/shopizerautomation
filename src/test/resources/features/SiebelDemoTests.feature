Feature: Siebel Automation Demo Tests
  In Order to test the Siebel Functionality
  As an Admin User
  I want to verify the Siebel pages

  @login
  Scenario Outline: Login to Siebel Application
    Given I Open Siebel Application
    And I Login with username "rkurian" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contact
      | Type   | Title   | FirstName   | LastName   | Gender   | DOB   | Mobile   |
      | <Type> | <Title> | <FirstName> | <LastName> | <Gender> | <DOB> | <Mobile> |
    Then I get the Siebel Id for the new contact
    Then I verify the details using GetAdminclient SOAP request
    Then I Logout of the Siebel Application

    Examples:
      | Type   | Title | FirstName | LastName | Gender | DOB        | Mobile       |
      | Client | Mr    | David     | Williams | M      | 12/02/1980 | +61401234567 |
