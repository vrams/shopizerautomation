Feature: Siebel Automation of Contacts Tests
  In Order to test the Siebel Functionality
  As an Admin User
  I want to verify the Siebel pages

  @contacts
  Scenario Outline: Create Contact in Siebel and push to pivot
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contact
      | Type   | SubType   | Title   | Gender   | Sector   | Source   | BusinessUnit   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | IndustryCodes   | IndustryCodeValues   |
      | <Type> | <SubType> | <Title> | <Gender> | <Sector> | <Source> | <BusinessUnit> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <IndustryCodes> | <IndustryCodeValues> |
    Then I get the Siebel Id for the new contact
    And I create Address and Contact Information
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I search for new Contact and Push the details to Pivot
    Then I verify the Contact details in Pivot
    Then I Logout of the Siebel Application

    Examples:
      | Type    | SubType        | Title | Gender | Sector   | Source | BusinessUnit | ABClassification | CustomerCategory | PivotBusinessUnit  | IndustryCodes | IndustryCodeValues |
      | Contact | Account Holder | Mr    | M      | Councils | Other  | Sydney       | Councils         | Bank             | Body Corp Serv-NSW | APRA,ANZSIC   | 4235,9511          |

  @editContactEmail
  Scenario Outline: Edit Contact Email
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I query the contact details with SiebelId
      | SiebelId   |
      | <SiebelId> |
    And I query the contact details and update the email
    And I push the Contact details to Pivot
    Then I verify the Contact details in Pivot
    And I Logout of the Siebel Application

    Examples:
      | SiebelId |
      | 78788761 |

  @editPrimaryPhone
  Scenario Outline: Edit Contact Email
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I query the contact details with SiebelId
      | SiebelId   |
      | <SiebelId> |
    And I query the contact details and update the primary phone
    And I push the Contact details to Pivot
    Then I verify the Contact details in Pivot
    And I Logout of the Siebel Application

    Examples:
      | SiebelId |
      | 78788761 |

  @updateContactAddress
  Scenario Outline: Update Primary Address
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contact
      | Type   | SubType   | Title   | Gender   | Sector   | Source   | BusinessUnit   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | IndustryCodes   | IndustryCodeValues   |
      | <Type> | <SubType> | <Title> | <Gender> | <Sector> | <Source> | <BusinessUnit> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <IndustryCodes> | <IndustryCodeValues> |
    Then I get the Siebel Id for the new contact
    And I create Address and Contact Information
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I search for new Contact and Push the details to Pivot
    Then I verify the Contact details in Pivot
    Then I edit the address and push the updated details to Pivot
    Then I verify the new address details in Pivot
    Then I Logout of the Siebel Application

    Examples:
      | Type    | SubType        | Title | Gender | Sector   | Source | BusinessUnit | ABClassification | CustomerCategory | PivotBusinessUnit  | IndustryCodes | IndustryCodeValues |
      | Contact | Account Holder | Mr    | M      | Councils | Other  | Sydney       | Councils         | Bank             | Body Corp Serv-NSW | APRA,ANZSIC   | 4235,9511          |


  @mergeContacts
  Scenario Outline: Merge Contacts
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contacts for merge
      | MergeContact   | Type   | Status   | SubType   | Title   | Gender   | Sector   | Source   | BusinessUnit   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | IndustryCodes   | IndustryCodeValues   |
      | <MergeContact> | <Type> | <Status> | <SubType> | <Title> | <Gender> | <Sector> | <Source> | <BusinessUnit> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <IndustryCodes> | <IndustryCodeValues> |
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I search for new Contact and Push the details to Pivot
    Then I select the contacts and merge the records
    Then I validate the merged record details for Contacts
    Then I Logout of the Siebel Application

    Examples:
      | MergeContact | Type   | Status | SubType                     | Title | Gender | Sector              | Source                          | BusinessUnit    | ABClassification    | CustomerCategory | PivotBusinessUnit  | IndustryCodes | IndustryCodeValues |
      | Y            | Client | Active | Participant, Account Holder | Mr    | M      | Accounting,Councils | Accredited Referrer,Advertising | Sydney,Adelaide | Accounting,Councils | Bank,Equity      | Body Corp Serv-NSW | APRA,ANZSIC   | 4235,9511          |