Feature: Pivot Automation
  In Order to test the Pivot Application

  @pivotIntegration
  Scenario Outline: Create Contact in Siebel and push to pivot
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contact
      | Type   | SubType   | Title   | Gender   | Sector   | Source   | BusinessUnit   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | IndustryCodes   | IndustryCodeValues   |
      | <Type> | <SubType> | <Title> | <Gender> | <Sector> | <Source> | <BusinessUnit> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <IndustryCodes> | <IndustryCodeValues> |
    Then I get the Siebel Id for the new contact
    And I create Address and Contact Information
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I search for new Contact and Push the details to Pivot
    Then I verify the Contact details in Pivot
    Then I Logout of the Siebel Application
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    Then I search for pivot customer number created in Siebel
    And I create a facility for the pivot customer number
    Then I close the Pivot Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I search for contact facility and verify the facility number in Siebel
    Then I Logout of the Siebel Application

    Examples:
      | Type    | SubType        | Title | Gender | Sector   | Source | BusinessUnit | ABClassification | CustomerCategory | PivotBusinessUnit  | IndustryCodes | IndustryCodeValues |
      | Contact | Account Holder | Mr    | M      | Councils | Other  | Sydney       | Councils         | Bank             | Body Corp Serv-NSW | APRA,ANZSIC   | 4235,9511          |


