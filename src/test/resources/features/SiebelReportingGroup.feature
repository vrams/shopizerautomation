Feature: Siebel Automation Reporting Group tab
  In Order to test the Siebel Reporting Group Functionality
  As an Admin User
  I want to verify the Siebel Reporting Group pages

  @reportingGroup
  Scenario Outline: Test Reporting Group tab
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Reporting Group" tab on Siebel Home Page
    And I create a new Reporting Group
      | Type   | SubType   | GroupType   | Status   | Sector   | Source   | SalesBusinessUnit   | ServicePlan   | ABClassification   | PivotBusinessUnit   | DEFTExtractCode   | CustomerOwner   |
      | <Type> | <SubType> | <GroupType> | <Status> | <Sector> | <Source> | <SalesBusinessUnit> | <ServicePlan> | <ABClassification> | <PivotBusinessUnit> | <DEFTExtractCode> | <CustomerOwner> |
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Reporting Group" tab on Siebel Home Page
    And I query the reporting group details and I push the details to pivot
    And I click on Reporting Group tab on home page
    And I verify the Reporting Group  details in Pivot database
    Then I Logout of the Siebel Application


    Examples:
      | Type    | SubType        | GroupType   | Status   | Sector     | Source             | SalesBusinessUnit | ServicePlan | ABClassification | PivotBusinessUnit  | DEFTExtractCode | CustomerOwner |
      | Contact | Account Holder | Sales Group | Inactive | Accounting | Clients Accountant | Sydney            | Unknown     | Unclassified     | Body Corp Serv-NSW | Daily           | Group Legal   |


    @relationshipContact
    Scenario Outline: Test contacts from relationship tab
      Given I Open the Siebel Application
      And I Login to Siebel Application  with username "mpfuser8" and password "password"
      And I navigate to the "Reporting Group" tab on Siebel Home Page
      And I create a new Reporting Group
        | Type   | SubType   | GroupType   | Status   | Sector   | Source   | SalesBusinessUnit   | ServicePlan   | ABClassification   | PivotBusinessUnit   | DEFTExtractCode   | CustomerOwner   | Title   |
        | <Type> | <SubType> | <GroupType> | <Status> | <Sector> | <Source> | <SalesBusinessUnit> | <ServicePlan> | <ABClassification> | <PivotBusinessUnit> | <DEFTExtractCode> | <CustomerOwner> | <Title> |
      And I get new reporting group details
      Then I add a new contact details
      Then I add new company details
      And I navigate to the "Reporting Group" tab on Siebel Home Page
      And I query the reporting group details
      Then I click on contacts name to get  details from contacts page
      Then I verify the contacts details in contacts page with the contacts in relationship tab
      Then I Logout of the Siebel Application

      Examples:
        | Type    | SubType        | GroupType   | Status   | Sector     | Source             | SalesBusinessUnit | ServicePlan | ABClassification | PivotBusinessUnit  | DEFTExtractCode | CustomerOwner | Title |
        | Contact | Account Holder | Sales Group | Inactive | Accounting | Clients Accountant | Sydney            | Unknown     | Unclassified     | Body Corp Serv-NSW | Daily           | Group Legal   | Mr    |


    @relationshipCompany
    Scenario Outline: Test contacts from relationship tab
      Given I Open the Siebel Application
      And I Login to Siebel Application  with username "mpfuser8" and password "password"
      And I navigate to the "Reporting Group" tab on Siebel Home Page
      And I create a new Reporting Group
        | Type   | SubType   | GroupType   | Status   | Sector   | Source   | SalesBusinessUnit   | ServicePlan   | ABClassification   | PivotBusinessUnit   | DEFTExtractCode   | CustomerOwner   | Title   |
        | <Type> | <SubType> | <GroupType> | <Status> | <Sector> | <Source> | <SalesBusinessUnit> | <ServicePlan> | <ABClassification> | <PivotBusinessUnit> | <DEFTExtractCode> | <CustomerOwner> | <Title> |
      And I get new reporting group details
      Then I add new company details
      And I navigate to the "Reporting Group" tab on Siebel Home Page
      And I query the reporting group details
      Then I click on company name to get  details from company page
      Then I verify the company details in company page with the company in relationship tab
      Then I Logout of the Siebel Application

      Examples:
        | Type    | SubType        | GroupType   | Status   | Sector     | Source             | SalesBusinessUnit | ServicePlan | ABClassification | PivotBusinessUnit  | DEFTExtractCode | CustomerOwner | Title |
        | Contact | Account Holder | Sales Group | Inactive | Accounting | Clients Accountant | Sydney            | Unknown     | Unclassified     | Body Corp Serv-NSW | Daily           | Group Legal   | Mr    |
