Feature: Siebel Automation Companies tab
  In Order to test the Siebel Companies Functionality
  As an Admin User
  I want to verify the Siebel All Companies pages

  @companies
  Scenario Outline: Test Companies tab
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I create a new Company
      | Type   | SubType   | BusinessType   | Status   | Sector   | Source   | IndustryCodes   | IndustryCodeValues   | SalesBusinessUnit   | ReportingGroupId   | ARBN   | ABSCode   | ABN   | ACN   | IndustrySoftware   | AccountingSoftware   | BusinessCode   | BusinessSize   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | LegislationType   | LegislationLoc   | TrustType   |
      | <Type> | <SubType> | <BusinessType> | <Status> | <Sector> | <Source> | <IndustryCodes> | <IndustryCodeValues> | <SalesBusinessUnit> | <ReportingGroupId> | <ARBN> | <ABSCode> | <ABN> | <ACN> | <IndustrySoftware> | <AccountingSoftware> | <BusinessCode> | <BusinessSize> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <LegislationType> | <LegislationLoc> | <TrustType> |
    And I click on Companies tab on home page
    And I create Address and Contact Information of the Company
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I click on Companies tab on home page
    And I click on more info options and fill the details
    Then I push the details to Pivot
    And I verify Company details in Siebel and Pivot database
    Then I Logout of the Siebel Application

    Examples:
      | Type   | SubType        | BusinessType | Status   | Sector     | Source             | IndustryCodes | IndustryCodeValues | SalesBusinessUnit | ReportingGroupId | ARBN      | ABSCode     | ABN         | ACN       | IndustrySoftware | AccountingSoftware | BusinessCode        | BusinessSize | ABClassification | CustomerCategory | PivotBusinessUnit         | LegislationType | LegislationLoc | TrustType          |
      | Client | Account Holder | Company      | Inactive | Accounting | Clients Accountant | APRA,ANZSIC   | 3010,2761          | National          | 78201113         | 133221084 | Other Banks | 53004085616 | 133221084 | ADEPT            | CMS                | Banks - State Banks | 3,000,000    | Accounting       | Bank             | Internal Group A/cs - Syd | State/Territory | NSW            | Testamentary Trust |

  @editCompanyEmail
  Scenario Outline: Edit Company Email
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I click on Companies tab on home page for editing email
      | SiebelId   | CompanyName   |
      | <SiebelId> | <CompanyName> |
    And I edit email details of the company
    Then I push the details to Pivot
    Then I Logout of the Siebel Application

    Examples:
      | SiebelId | CompanyName       |
      | 78788564 | Test_Automation_3 |

  @test
  Scenario: Create a demo test
    Given I Open Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Activities" tab on Siebel Home Page
    And I click on new button on the activities
    Then I Logout of the Siebel Application

  @mergeCompanies
  Scenario Outline: Test Companies tab
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I create  new Companies to test merge scenario
      | MergeContact   | Type   | SubType   | BusinessType   | Status   | Sector   | Source   | IndustryCodes   | IndustryCodeValues   | SalesBusinessUnit   | ReportingGroupId   | ARBN   | ABSCode   | ABN   | ACN   | IndustrySoftware   | AccountingSoftware   | BusinessCode   | BusinessSize   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | LegislationType   | LegislationLoc   | TrustType   |
      | <MergeContact> | <Type> | <SubType> | <BusinessType> | <Status> | <Sector> | <Source> | <IndustryCodes> | <IndustryCodeValues> | <SalesBusinessUnit> | <ReportingGroupId> | <ARBN> | <ABSCode> | <ABN> | <ACN> | <IndustrySoftware> | <AccountingSoftware> | <BusinessCode> | <BusinessSize> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <LegislationType> | <LegislationLoc> | <TrustType> |
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I search for new Companies and Push the details to Pivot
    Then I select the Companies and merge the records
    Then I validate the merged company details
    Then I Logout of the Siebel Application


    Examples:
      | MergeContact | Type   | SubType                    | BusinessType | Status   | Sector                | Source             | IndustryCodes | IndustryCodeValues | SalesBusinessUnit | ReportingGroupId | ARBN      | ABSCode                  | ABN         | ACN       | IndustrySoftware | AccountingSoftware | BusinessCode        | BusinessSize | ABClassification | CustomerCategory | PivotBusinessUnit | LegislationType | LegislationLoc | TrustType          |
      | Y            | Client | Participant,Account Holder | Company      | Inactive | Child Care,Accounting | Clients Accountant | APRA,ANZSIC   | 3010,2761          | Adelaide,Sydney   | 78201113         | 133221084 | Other Banks,Federal Govt | 53004085616 | 133221084 | ADEPT            | CMS                | Banks - State Banks | 3,000,000    | Councils,House   | Bank             | Adelaide,Sydney   | State/Territory | NSW            | Testamentary Trust |
