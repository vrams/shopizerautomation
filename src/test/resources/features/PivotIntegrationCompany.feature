Feature: Pivot Automation
  In Order to test the Pivot Application

  @pivotCompanyFacility
  Scenario Outline: Create a Facility in Pivot for a company and verify Facility in Siebel
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I create a new Company
      | Type   | SubType   | BusinessType   | Status   | Sector   | Source   | IndustryCodes   | IndustryCodeValues   | SalesBusinessUnit   | ReportingGroupId   | ARBN   | ABSCode   | ABN   | ACN   | IndustrySoftware   | AccountingSoftware   | BusinessCode   | BusinessSize   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | LegislationType   | LegislationLoc   | TrustType   | IsFacility   |
      | <Type> | <SubType> | <BusinessType> | <Status> | <Sector> | <Source> | <IndustryCodes> | <IndustryCodeValues> | <SalesBusinessUnit> | <ReportingGroupId> | <ARBN> | <ABSCode> | <ABN> | <ACN> | <IndustrySoftware> | <AccountingSoftware> | <BusinessCode> | <BusinessSize> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <LegislationType> | <LegislationLoc> | <TrustType> | <IsFacility> |
    And I click on Companies tab on home page
    And I create Address and Contact Information of the Company
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I click on Companies tab on home page
    And I click on more info options and fill the details
    Then I push the details to Pivot
    And I verify Company details in Siebel and Pivot database
    Then I Logout of the Siebel Application
    And I close browser session
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    Then I search for pivot customer number created in Siebel
    And I create a facility for the pivot customer number
    And I open a "ie" browser session
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    Then I search for Company facility and verify the facility number in Siebel

    Examples:
      | Type   | SubType        | BusinessType | Status   | Sector     | Source             | IndustryCodes | IndustryCodeValues | SalesBusinessUnit | ReportingGroupId | ARBN      | ABSCode     | ABN         | ACN       | IndustrySoftware | AccountingSoftware | BusinessCode        | BusinessSize | ABClassification | CustomerCategory | PivotBusinessUnit         | LegislationType | LegislationLoc | TrustType          | IsFacility |
      | Client | Account Holder | Company      | Inactive | Accounting | Clients Accountant | APRA,ANZSIC   | 3010,2761          | National          | 78201113         | 133221084 | Other Banks | 53004085616 | 133221084 | ADEPT            | CMS                | Banks - State Banks | 3,000,000    | Accounting       | Bank             | Internal Group A/cs - Syd | State/Territory | NSW            | Testamentary Trust | Y          |

  @pivotCompanyAccount
  Scenario Outline: Create an account in Pivot for a company and verify account id in Siebel
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I create a new Company
      | Type   | SubType   | BusinessType   | Status   | Sector   | Source   | IndustryCodes   | IndustryCodeValues   | SalesBusinessUnit   | ReportingGroupId   | ARBN   | ABSCode   | ABN   | ACN   | IndustrySoftware   | AccountingSoftware   | BusinessCode   | BusinessSize   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | LegislationType   | LegislationLoc   | TrustType   | IsFacility   | AdditionalAccount   |
      | <Type> | <SubType> | <BusinessType> | <Status> | <Sector> | <Source> | <IndustryCodes> | <IndustryCodeValues> | <SalesBusinessUnit> | <ReportingGroupId> | <ARBN> | <ABSCode> | <ABN> | <ACN> | <IndustrySoftware> | <AccountingSoftware> | <BusinessCode> | <BusinessSize> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <LegislationType> | <LegislationLoc> | <TrustType> | <IsFacility> | <AdditionalAccount> |
    And I click on Companies tab on home page
    And I create Address and Contact Information of the Company
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I click on Companies tab on home page
    And I click on more info options and fill the details
    Then I push the details to Pivot
    And I verify Company details in Siebel and Pivot database
    Then I Logout of the Siebel Application
    And I close browser session
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    Then I search for pivot customer number created in Siebel
    And I create a facility for the pivot customer number
    Then I create an Account for the Facility
    And I open a "ie" browser session
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    Then I search for Company facility and verify the facility number in Siebel
    Then I verify Company account id in Siebel

    Examples:
      | Type   | SubType        | BusinessType | Status   | Sector     | Source             | IndustryCodes | IndustryCodeValues | SalesBusinessUnit | ReportingGroupId | ARBN      | ABSCode     | ABN         | ACN       | IndustrySoftware | AccountingSoftware | BusinessCode        | BusinessSize | ABClassification | CustomerCategory | PivotBusinessUnit         | LegislationType | LegislationLoc | TrustType          | IsFacility | AdditionalAccount |
      | Client | Account Holder | Company      | Inactive | Accounting | Clients Accountant | APRA,ANZSIC   | 3010,2761          | National          | 78201113         | 133221084 | Other Banks | 53004085616 | 133221084 | ADEPT            | CMS                | Banks - State Banks | 3,000,000    | Accounting       | Bank             | Internal Group A/cs - Syd | State/Territory | NSW            | Testamentary Trust | N          | N                 |

  @updateCompanyFacility
  Scenario Outline: Update an existing Company Facility and verify in Siebel
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I create a new Company
      | Type   | SubType   | BusinessType   | Status   | Sector   | Source   | IndustryCodes   | IndustryCodeValues   | SalesBusinessUnit   | ReportingGroupId   | ARBN   | ABSCode   | ABN   | ACN   | IndustrySoftware   | AccountingSoftware   | BusinessCode   | BusinessSize   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | LegislationType   | LegislationLoc   | TrustType   | IsFacility   |
      | <Type> | <SubType> | <BusinessType> | <Status> | <Sector> | <Source> | <IndustryCodes> | <IndustryCodeValues> | <SalesBusinessUnit> | <ReportingGroupId> | <ARBN> | <ABSCode> | <ABN> | <ACN> | <IndustrySoftware> | <AccountingSoftware> | <BusinessCode> | <BusinessSize> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <LegislationType> | <LegislationLoc> | <TrustType> | <IsFacility> |
    And I click on Companies tab on home page
    And I create Address and Contact Information of the Company
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I click on Companies tab on home page
    And I click on more info options and fill the details
    Then I push the details to Pivot
    And I verify Company details in Siebel and Pivot database
    Then I Logout of the Siebel Application
    And I close browser session
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    Then I search for pivot customer number created in Siebel
    And I create a facility for the pivot customer number
    Then I search for customer facility created in Pivot
    And I update and save facility values
    And I open a "ie" browser session
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    Then I verify updated facility status in Siebel

    Examples:
      | Type   | SubType        | BusinessType | Status   | Sector     | Source             | IndustryCodes | IndustryCodeValues | SalesBusinessUnit | ReportingGroupId | ARBN      | ABSCode     | ABN         | ACN       | IndustrySoftware | AccountingSoftware | BusinessCode        | BusinessSize | ABClassification | CustomerCategory | PivotBusinessUnit         | LegislationType | LegislationLoc | TrustType          | IsFacility |
      | Client | Account Holder | Company      | Inactive | Accounting | Clients Accountant | APRA,ANZSIC   | 3010,2761          | National          | 78201113         | 133221084 | Other Banks | 53004085616 | 133221084 | ADEPT            | CMS                | Banks - State Banks | 3,000,000    | Accounting       | Bank             | Internal Group A/cs - Syd | State/Territory | NSW            | Testamentary Trust | Y          |

  @updateCompanyAccount
  Scenario Outline: Update an existing Company Facility and verify in Siebel
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I create a new Company
      | Type   | SubType   | BusinessType   | Status   | Sector   | Source   | IndustryCodes   | IndustryCodeValues   | SalesBusinessUnit   | ReportingGroupId   | ARBN   | ABSCode   | ABN   | ACN   | IndustrySoftware   | AccountingSoftware   | BusinessCode   | BusinessSize   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | LegislationType   | LegislationLoc   | TrustType   | IsFacility   | AdditionalAccount   |
      | <Type> | <SubType> | <BusinessType> | <Status> | <Sector> | <Source> | <IndustryCodes> | <IndustryCodeValues> | <SalesBusinessUnit> | <ReportingGroupId> | <ARBN> | <ABSCode> | <ABN> | <ACN> | <IndustrySoftware> | <AccountingSoftware> | <BusinessCode> | <BusinessSize> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <LegislationType> | <LegislationLoc> | <TrustType> | <IsFacility> | <AdditionalAccount> |
    And I click on Companies tab on home page
    And I create Address and Contact Information of the Company
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I click on Companies tab on home page
    And I click on more info options and fill the details
    Then I push the details to Pivot
    And I verify Company details in Siebel and Pivot database
    Then I Logout of the Siebel Application
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    And I create a facility for the pivot customer number
    Then I create an Account for the Facility
    Then I search for pivot customer account created in Pivot
    And I update and save the account values
    And I open a "ie" browser session
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    Then I verify updated account in Siebel

    Examples:
      | Type   | SubType        | BusinessType | Status   | Sector     | Source             | IndustryCodes | IndustryCodeValues | SalesBusinessUnit | ReportingGroupId | ARBN      | ABSCode     | ABN         | ACN       | IndustrySoftware | AccountingSoftware | BusinessCode        | BusinessSize | ABClassification | CustomerCategory | PivotBusinessUnit         | LegislationType | LegislationLoc | TrustType          | IsFacility | AdditionalAccount |
      | Client | Account Holder | Company      | Inactive | Accounting | Clients Accountant | APRA,ANZSIC   | 3010,2761          | National          | 78201113         | 133221084 | Other Banks | 53004085616 | 133221084 | ADEPT            | CMS                | Banks - State Banks | 3,000,000    | Accounting       | Bank             | Internal Group A/cs - Syd | State/Territory | NSW            | Testamentary Trust | N          | N                 |


  @linkAdditionalAccountsToCompany
  Scenario Outline: Create additional accounts for a company in Pivot and verify account id in Siebel
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I create a new Company
      | Type   | SubType   | BusinessType   | Status   | Sector   | Source   | IndustryCodes   | IndustryCodeValues   | SalesBusinessUnit   | ReportingGroupId   | ARBN   | ABSCode   | ABN   | ACN   | IndustrySoftware   | AccountingSoftware   | BusinessCode   | BusinessSize   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | LegislationType   | LegislationLoc   | TrustType   | IsFacility   | AdditionalAccount   |
      | <Type> | <SubType> | <BusinessType> | <Status> | <Sector> | <Source> | <IndustryCodes> | <IndustryCodeValues> | <SalesBusinessUnit> | <ReportingGroupId> | <ARBN> | <ABSCode> | <ABN> | <ACN> | <IndustrySoftware> | <AccountingSoftware> | <BusinessCode> | <BusinessSize> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <LegislationType> | <LegislationLoc> | <TrustType> | <IsFacility> | <AdditionalAccount> |
    And I click on Companies tab on home page
    And I create Address and Contact Information of the Company
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    And I click on Companies tab on home page
    And I click on more info options and fill the details
    Then I push the details to Pivot
    And I verify Company details in Siebel and Pivot database
    Then I Logout of the Siebel Application
    And I close browser session
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    And I create a facility for creating a customer account
    Then I create an Account for the Facility
    Then I create additional Account for the Facility
    And I open a "ie" browser session
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Companies" tab on Siebel Home Page
    Then I search for Company facility and verify the facility number in Siebel
    Then I verify account numbers in Siebel
    Then I Logout of the Siebel Application


    Examples:
      | Type   | SubType        | BusinessType | Status   | Sector     | Source             | IndustryCodes | IndustryCodeValues | SalesBusinessUnit | ReportingGroupId | ARBN      | ABSCode     | ABN         | ACN       | IndustrySoftware | AccountingSoftware | BusinessCode        | BusinessSize | ABClassification | CustomerCategory | PivotBusinessUnit         | LegislationType | LegislationLoc | TrustType          | IsFacility | AdditionalAccount |
      | Client | Account Holder | Company     | Inactive | Accounting | Clients Accountant | APRA,ANZSIC   | 3010,2761          | National          | 78201113         | 133221084 | Other Banks | 53004085616 | 133221084 | ADEPT            | CMS                | Banks - State Banks | 3,000,000    | Accounting       | Bank             | Internal Group A/cs - Syd | State/Territory | NSW            | Testamentary Trust | N          | Y                 |

