Feature: Pivot Automation
  In Order to test the Pivot Application

  @createContact
  Scenario Outline: Create Contact in Siebel and push to pivot
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contact
      | Type   | SubType   | Title   | Gender   | Sector   | Source   | BusinessUnit   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | IndustryCodes   | IndustryCodeValues   | IsFacility   |
      | <Type> | <SubType> | <Title> | <Gender> | <Sector> | <Source> | <BusinessUnit> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <IndustryCodes> | <IndustryCodeValues> | <IsFacility> |
    Then I get the Siebel Id for the new contact
    And I create Address and Contact Information
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I search for new Contact and Push the details to Pivot
    Then I verify the Contact details in Pivot
    Then I Logout of the Siebel Application
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    Then I search for pivot customer number created in Siebel
    And I create a facility for the pivot customer number
    Then I search for contact facility and verify the facility number in Siebel
    Then I close the Pivot Application

    Examples:
      | Type    | SubType        | Title | Gender | Sector   | Source | BusinessUnit | ABClassification | CustomerCategory | PivotBusinessUnit  | IndustryCodes | IndustryCodeValues | IsFacility |
      | Contact | Account Holder | Mr    | M      | Councils | Other  | Sydney       | Councils         | Bank             | Body Corp Serv-NSW | APRA,ANZSIC   | 4235,9511          | Y          |


  @pivotContactAccount
  Scenario Outline: Create an account for a contact in Pivot and verify account id in Siebel
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contact
      | Type   | SubType   | Title   | Gender   | Sector   | Source   | BusinessUnit   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | IndustryCodes   | IndustryCodeValues   | IsFacility   | AdditionalAccount   |
      | <Type> | <SubType> | <Title> | <Gender> | <Sector> | <Source> | <BusinessUnit> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <IndustryCodes> | <IndustryCodeValues> | <IsFacility> | <AdditionalAccount> |
    Then I get the Siebel Id for the new contact
    And I create Address and Contact Information
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I search for new Contact and Push the details to Pivot
    Then I verify the Contact details in Pivot
    Then I Logout of the Siebel Application
    And I close browser session
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    Then I search for pivot customer number created in Siebel
    And I create a facility for the pivot customer number
    Then I create an Account for the Facility
    And I open a "ie" browser session
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I search for contact facility and verify the facility number in Siebel
    Then I verify the account id in Siebel

    Examples:
      | Type    | SubType        | Title | Gender | Sector   | Source | BusinessUnit | ABClassification | CustomerCategory | PivotBusinessUnit  | IndustryCodes | IndustryCodeValues | IsFacility | AdditionalAccount |
      | Contact | Account Holder | Mr    | M      | Councils | Other  | Sydney       | Councils         | Bank             | Body Corp Serv-NSW | APRA,ANZSIC   | 4235,9511          | N          | N                 |


  @updateContactFacilityAccount
  Scenario Outline: Update an existing Contact Facility and verify in Siebel
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contact
      | Type   | SubType   | Title   | Gender   | Sector   | Source   | BusinessUnit   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | IndustryCodes   | IndustryCodeValues   | IsFacility   | AdditionalAccount   |
      | <Type> | <SubType> | <Title> | <Gender> | <Sector> | <Source> | <BusinessUnit> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <IndustryCodes> | <IndustryCodeValues> | <IsFacility> | <AdditionalAccount> |
    Then I get the Siebel Id for the new contact
    And I create Address and Contact Information
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I search for new Contact and Push the details to Pivot
    Then I verify the Contact details in Pivot
    Then I Logout of the Siebel Application
    Then I close browser session
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    Then I search for pivot customer number created in Siebel
    Then I create and save a facility and then I modify the facility status
    Then I create and save account and then I modify the account status
    Then I close the Pivot Application
    And I open a "ie" browser session
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Contacts" tab on Siebel Home Page
    Then I search for siebel Id and verify facility and account status
    Then I Logout of the Siebel Application

    Examples:
      | Type    | SubType        | Title | Gender | Sector   | Source | BusinessUnit | ABClassification | CustomerCategory | PivotBusinessUnit  | IndustryCodes | IndustryCodeValues | IsFacility | AdditionalAccount |
      | Contact | Account Holder | Mr    | M      | Councils | Other  | Sydney       | Councils         | Bank             | Body Corp Serv-NSW | APRA,ANZSIC   | 4235,9511          | N          | N                 |


  @linkAdditionalAccountsToContact
  Scenario Outline: Create additional accounts for a contact in Pivot and verify account id in Siebel
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser8" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    Then I create and save a new contact
      | Type   | SubType   | Title   | Gender   | Sector   | Source   | BusinessUnit   | ABClassification   | CustomerCategory   | PivotBusinessUnit   | IndustryCodes   | IndustryCodeValues   | IsFacility   | AdditionalAccount   |
      | <Type> | <SubType> | <Title> | <Gender> | <Sector> | <Source> | <BusinessUnit> | <ABClassification> | <CustomerCategory> | <PivotBusinessUnit> | <IndustryCodes> | <IndustryCodeValues> | <IsFacility> | <AdditionalAccount> |
    Then I get the Siebel Id for the new contact
     And I create Address and Contact Information
    Then I Logout of the Siebel Application
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to "Contacts" tab on the Home Page
    And I search for new Contact and Push the details to Pivot
    Then I verify the Contact details in Pivot
    Then I Logout of the Siebel Application
    And I close browser session
    When I Open Pivot Application
    And I login to Pivot Application with username and password
    Then I search for pivot customer number created in Siebel
    And I create a facility for creating a customer account
    Then I create an Account for the Facility
    Then I create additional Account for the Facility
    And I open a "ie" browser session
    Given I Open the Siebel Application
    And I Login to Siebel Application  with username "mpfuser1" and password "password"
    And I navigate to the "Contacts" tab on Siebel Home Page
    Then I search for accounts created in Pivot and verify it in Siebel
    Then I Logout of the Siebel Application

    Examples:
      | Type    | SubType        | Title | Gender | Sector   | Source | BusinessUnit | ABClassification | CustomerCategory | PivotBusinessUnit  | IndustryCodes | IndustryCodeValues | IsFacility | AdditionalAccount |
      | Contact | Account Holder | Mr    | M      | Councils | Other  | Sydney       | Councils         | Bank             | Body Corp Serv-NSW | APRA,ANZSIC   | 4235,9511          | N          | Y                 |

