package com.macquarie.bfs.siebel;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by vthaduri on 27/01/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        features = "@target/FailedScenarios.txt",
        format = {"pretty", "html:target/site/cucumber-pretty",
                "json:target/cucumber.json"}
        //,tags = "@sample"
)
public class FailedScenarios {

}
