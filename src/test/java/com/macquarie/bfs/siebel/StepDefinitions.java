package com.macquarie.bfs.siebel;

import com.macquarie.bfs.siebel.WebServices.WebServiceRequests;
import com.macquarie.bfs.siebel.WebServices.XMLParser;
import com.macquarie.bfs.siebel.commons.*;
import com.macquarie.bfs.siebel.pages.*;
import com.macquarie.bfs.siebel.pivot.PivotAccountWindow;
import com.macquarie.bfs.siebel.pivot.PivotFacilityWindow;
import com.macquarie.bfs.siebel.pivot.PivotHomeWindow;
import com.macquarie.bfs.siebel.pivot.PivotLoginWindow;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.winium.WiniumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vthaduri on 16/01/2017.
 */
public class StepDefinitions {

    private static Logger logger = LoggerFactory.getLogger(StepDefinitions.class);
    public PropertiesConfiguration pc;
    public List<HashMap<String, String>> testData;
    private Container container;
    private WebDriver driver;
    private WiniumDriver windowDriver;
    private SiebelLoginPage siebelLoginPage;
    private SiebelHomePage siebelHomePage;
    private SiebelContactsPage siebelContactsPage;
    private WebServiceRequests webServiceRequests;
    private SiebelCompaniesPage siebelCompaniesPage;
    private SiebelActivitiesPage siebelActivitiesPage;
    private SiebelReportingGroup siebelReportingGroup;

    private String pwd = "TWlnMjlzbXQ=";
    private String siebelId = null;
    private String companyName = null;
    private List<Map<String, String>> data = null;
    private String groupName = null;
    private String pivotId = null;
    private String firstName = null;
    private String lastName = null;
    private String shortName = null;
    private String dob = null;
    private HashMap<String, String> siebelDetailsMap = new HashMap<String, String>();
    private String businessPhone = null;
    private String companyBusinessPhone = null;
    private String contactId = null;
    private String companyId = null;
    private String reportGroupId = null;
    private List<String> addressList = null;
    private String oldAddressNumber = null;
    private String oldAddressOne = null;
    private String newAddressNumber = null;
    private String newAddressOne = null;
    private String middleName = null;
    private String destinationSiebelId = null;
    private String mergeContactFlag = null;

    private PivotLoginWindow pivotLoginWindow;
    private PivotHomeWindow pivotHomeWindow;
    private PivotFacilityWindow pivotFacilityWindow;
    private String[] subTypes = null;
    private String[] sector = null;
    private String[] salesBU = null;
    private String[] absCode = null;
    private String[] abClassification = null;
    private String[] pivotBU = null;
    private String facilityId = null;
    private String accountNumber = null;
    private PivotAccountWindow pivotAccountWindow;
    private String additionalAccNumber = null;


    public StepDefinitions(Container container) {
        this.container = container;
        this.driver = container.getDriver();
        this.windowDriver = container.getWindowDriver();
        try {
            pc = new PropertiesConfiguration("objectRepository.properties");
        } catch (Exception e) {
            logger.info("Unable to get the properties file");
            e.printStackTrace();
        }
    }

    private void assertField(String fieldName, String expected, String actual) {
        logger.info(fieldName + " in Siebel: " + expected + " - " + fieldName + " in SOAP: " + actual);
        Assert.assertEquals(fieldName + " Doesn't match for ", expected, actual);
    }

    @Given("^I Open Siebel Application$")
    public void i_Open_Siebel_Application() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        siebelLoginPage = new SiebelLoginPage(driver);
        siebelLoginPage.openApplication();


    }

    @Given("^I Login with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void i_Login_with_username_and_password(String username, String password) throws Throwable {
        password = pc.getString("Siebel");
        siebelLoginPage = new SiebelLoginPage(driver);
        siebelLoginPage.loginApplication(username, new String(Base64.decodeBase64(password.getBytes())));
        siebelLoginPage.inputUsername(username);
        siebelLoginPage.inputPassword(password);
    }

    @Then("^I Logout of the Siebel Application$")
    public void i_Logout_of_the_Siebel_Application() throws Throwable {
        siebelHomePage = new SiebelHomePage(driver);
        siebelHomePage.clickLogout();
        logger.info("*** Logged out of Siebel ***");
        siebelLoginPage = new SiebelLoginPage(driver);
    }

    @Given("^I navigate to \"([^\"]*)\" tab on the Home Page$")
    public void i_navigate_to_tab_on_the_Home_Page(String tab) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        siebelHomePage = new SiebelHomePage(driver);
        siebelHomePage.waitForPageToLoad();
        siebelHomePage.clickTab(tab);
    }

    @Given("^I search for new Contact and Push the details to Pivot$")
    public void i_search_for_new_Contact_and_Push_the_details_to_Pivot() throws Throwable {
        //For Testing . Remove hardcoded value while end to end testing
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelContactsPage.clickContactsQueryButton();
        if (mergeContactFlag != null) {
            siebelContactsPage.inputLastNameForContactSearch(lastName);
        } else {
            siebelContactsPage.clickSiebelIdColumn();

            siebelContactsPage.inputSiebelId(siebelId);
        }
        siebelContactsPage.clickContactsGoBtn();
        siebelContactsPage.clickPushToPivotBtn();
        Thread.sleep(2000);
    }

    @Then("^I verify the Contact details in Pivot$")
    public void i_verify_the_Contact_details_in_Pivot() throws Throwable {

        Thread.sleep(6000);
        siebelContactsPage.clickContactsQueryButton();
        siebelContactsPage.clickSiebelIdColumn();
        siebelContactsPage.inputSiebelId(siebelId);
        siebelContactsPage.clickContactsGoBtn();
        siebelContactsPage.clickLastName();
        siebelContactsPage.clickSystemLinks();
        siebelContactsPage.scrollDown();
        Thread.sleep(3000);
        pivotId = siebelContactsPage.getPivotId();
        logger.info("**** Pivot Id ----" + pivotId);

        addressList = JDBCConnection.getAddressNumbersFromPivot(pivotId);
        oldAddressNumber = addressList.get(0);
        oldAddressOne = JDBCConnection.getAddressDetailsFromPivot(oldAddressNumber);

        logger.info("**** oldAddressNumber ----" + oldAddressNumber + "---- oldAddressOne ----" + oldAddressOne);

        HashMap<String, String> pivotDetails = JDBCConnection.getContactDetailsFromPivot(pivotId);
        Assert.assertEquals(firstName, pivotDetails.get("FIRST_NAME"));
        Assert.assertEquals(lastName, pivotDetails.get("LAST_NAME"));
        dob = siebelContactsPage.formatDOB(dob);
        Assert.assertEquals(dob, SiebelUtil.formatDate(pivotDetails.get("DOB"), "yyyy-MM-dd HH:mm:ss.S", "dd/MM/yyyy"));
        //Assert.assertEquals(SiebelUtil.formatDate(dob,"dd/MM/yyyy","dd/MM/yyyy"),SiebelUtil.formatDate(pivotDetails.get("DOB"),"yyyy-MM-dd HH:mm:ss.S","dd/MM/yyyy"));
    }

    @Then("^I search for contact facility and verify the facility number in Siebel$")
    public void i_search_for_contact_facility_and_verify_the_facility_number_in_Siebel() throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelContactsPage.clickContactsQueryButton();
        siebelContactsPage.clickSiebelIdColumn();
        //Hardcode Siebel Id - 78843652
        //siebelId = "78843652";
        siebelContactsPage.inputSiebelId(siebelId);
        siebelContactsPage.clickContactsGoBtn();
        siebelContactsPage.clickLastName();
        siebelContactsPage.clickFinancialAccount();
        //Assert.assertEquals(facilityId, siebelContactsPage.getFacilityId());
    }

    @Then("^I search account and verify account status in Siebel$")
    public void i_search_account_and_verify_account_status_in_Siebel() throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelContactsPage.clickContactsQueryButton();
        siebelContactsPage.clickSiebelIdColumn();
        //Hardcode Siebel Id - 78843652
        //siebelId = "78843652";
        siebelContactsPage.inputSiebelId(siebelId);
        siebelContactsPage.clickContactsGoBtn();
        siebelContactsPage.clickLastName();
        siebelContactsPage.clickFinancialAccount();
        String accountStatus = siebelContactsPage.getAccountStatus();
        Assert.assertEquals("**** Pivot and Siebel account status does not match ----","Open",accountStatus);
    }


    @Then("^I verify the account id in Siebel$")
    public void i_verify_the_account_id_in_Siebel() throws Throwable {
        String siebelAccNum = siebelContactsPage.getAccountNumber();
        logger.info("**** Account Number ----"+accountNumber+"---- Siebel Account Number ----"+siebelAccNum);
        Assert.assertEquals("Account Number in Pivot and Siebel doesnot match",accountNumber,siebelAccNum);
    }

    @Then("^I edit the address and push the updated details to Pivot$")
    public void i_edit_the_address_and_push_the_updated_details_to_Pivot() throws Throwable {
        logger.info("**** Pivot Id ----" + pivotId);
        siebelContactsPage.clickAddress();
        siebelContactsPage.clickAddOrEditAddress("Shop 1a  1 Shelley Street", false, null);
        siebelContactsPage.clickPushToPivotBtn();
        siebelContactsPage.waitForLoadingImageDisappear();
        siebelContactsPage.waitForSaveImageDisappear();
        siebelContactsPage.clickSystemLinks();
        Thread.sleep(3000);
    }

    @Then("^I verify the new address details in Pivot$")
    public void i_verify_the_new_address_details_in_Pivot() throws Throwable {
        addressList = JDBCConnection.getAddressNumbersFromPivot(pivotId);
        logger.info("**** Address List ----" + addressList.size());
        Assert.assertEquals("*** Edit Address scenario failed ---", 2, addressList.size());

        for (String addressNumber : addressList) {
            logger.info("**** Address Numbers ---" + addressNumber);
            if (!addressNumber.equalsIgnoreCase(oldAddressNumber)) {
                newAddressOne = JDBCConnection.getAddressDetailsFromPivot(addressNumber);
                logger.info("**** New Address Selected is ----" + newAddressOne);
                break;
            }
        }
        Assert.assertNotEquals("**** Old and New Addressess are the same ", oldAddressOne, newAddressOne);
    }


    @Then("^I create and save a new contact$")
    public void i_create_and_save_new_contact(DataTable dataTable) throws Throwable {
        data = dataTable.asMaps(String.class, String.class);
        String[] industryCodes = data.get(0).get(SiebelConstants.INDUSTRY_CODES).split(",");
        String[] industryCdValues = data.get(0).get(SiebelConstants.INDUSTRY_CODES_VALUES).split(",");
        firstName = SiebelUtil.generateRandomText(6);
        lastName = SiebelUtil.generateRandomText(6);
        dob = SiebelUtil.generateRandonDOB();
        shortName = lastName + "" + firstName;

        siebelContactsPage = new SiebelContactsPage(driver);
        siebelCompaniesPage = new SiebelCompaniesPage(driver);
        siebelContactsPage.clickButtonInBottomTable("New");
        siebelContactsPage.inputBottomTableField("Type", data.get(0).get("Type"));
        siebelContactsPage.inputBottomTableField("Title", data.get(0).get("Title"));

        siebelContactsPage.inputFirstName(firstName);
        siebelContactsPage.inputLastName(lastName);
        siebelContactsPage.inputSubType(data.get(0).get(SiebelConstants.SUB_TYPE));
        siebelContactsPage.inputBottomTableField("Gender", data.get(0).get("Gender"));
        siebelContactsPage.inputBottomTableField("DOB", dob);
        siebelContactsPage.inputBottomTableField("Mobile", SiebelUtil.generatePhoneNumber());
        siebelContactsPage.inputSector(data.get(0).get("Sector"));
        siebelContactsPage.inputSource(data.get(0).get("Source"));
        siebelCompaniesPage.inputABClassification(data.get(0).get(SiebelConstants.AB_CLASSIFICATION));
        siebelCompaniesPage.inputCustomerCategory(data.get(0).get(SiebelConstants.CUSTOMER_CATEGORY));
        siebelContactsPage.inputBusinessUnit(data.get(0).get("BusinessUnit"));
        siebelCompaniesPage.inputPivotBU(data.get(0).get(SiebelConstants.PIVOT_BU));
        dob = siebelContactsPage.getDateOfBirth();

        siebelContactsPage.clickIndustryCodeIcon();
        siebelCompaniesPage.createIndustryCode(industryCodes[0], industryCdValues[0]);
        siebelCompaniesPage.createIndustryCode(industryCodes[1], industryCdValues[1]);
        siebelCompaniesPage.saveIndustryCodes();
        //Add a wait after saving industry codes
        siebelContactsPage.waitTillMainPageLoads();
        siebelContactsPage.clickReportingGrpIcon(); //
        siebelContactsPage.addNewReportGrp();
        siebelContactsPage.inputReportGrpName(SiebelUtil.generateRandomText(6));
        siebelContactsPage.inputReportGrpSector(data.get(0).get("Sector"));
        siebelContactsPage.inputReportGrpSource(data.get(0).get("Source"));
        siebelContactsPage.inputReportGrpSalesBU(data.get(0).get("BusinessUnit"));
        siebelContactsPage.clickReportGrpRelManagerIcon();
        siebelContactsPage.clickRelManagerOwnerAdviserBtn();
        siebelContactsPage.saveReportingGroup();
        siebelContactsPage.clickReportGroupOkButton();
        siebelContactsPage.waitForPageToLoad();
        // Select existing sales rep.
        siebelContactsPage.clickSalesRepIcon();
        siebelContactsPage.clickShowAvailableButton();
        siebelContactsPage.inputSalesRepName("Neeson");
        siebelContactsPage.searchSalesRep();
        siebelContactsPage.clickAddSaleRepButton();
        siebelContactsPage.selectPrimarySalesRep();
        siebelContactsPage.clickSalesRepOkButton();
        siebelContactsPage.waitForPageToLoad();
        Thread.sleep(2000);
        //siebelReportingGroup.saveReportingGroup();
        siebelContactsPage.clickSalesRepIcon();
        siebelContactsPage.removeNonPrimaryField();
        siebelContactsPage.clickSalesRepOkButton();
        siebelContactsPage.waitForPageToLoad();
        Thread.sleep(2000);
        siebelContactsPage.clickButtonInBottomTable("Save");
    }

    @Then("^I create and save a new contacts for merge$")
    public void i_create_and_save_a_new_contacts_for_merge(DataTable dataTable) throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelCompaniesPage = new SiebelCompaniesPage(driver);

        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        if (data.get(0).get(SiebelConstants.MERGE_CONTACT) != null) {
            mergeContactFlag = data.get(0).get(SiebelConstants.MERGE_CONTACT);
        }
        String[] industryCodes = data.get(0).get(SiebelConstants.INDUSTRY_CODES).split(",");
        String[] industryCdValues = data.get(0).get(SiebelConstants.INDUSTRY_CODES_VALUES).split(",");
        firstName = SiebelUtil.generateRandomText(6);
        lastName = SiebelUtil.generateRandomText(6);
        middleName = SiebelUtil.generateRandomText(6);

        dob = SiebelUtil.generateRandonDOB();
        shortName = lastName + "" + firstName;

        subTypes = data.get(0).get(SiebelConstants.SUB_TYPE).split(",");
        sector = data.get(0).get(SiebelConstants.SECTOR).split(",");
        String[] source = data.get(0).get(SiebelConstants.SOURCE).split(",");
        String[] businessUnit = data.get(0).get(SiebelConstants.BUSINESS_UNIT).split(",");
        String[] abClassification = data.get(0).get(SiebelConstants.AB_CLASSIFICATION).split(",");
        String[] customerCategory = data.get(0).get(SiebelConstants.CUSTOMER_CATEGORY).split(",");

        //create first contact.
        siebelContactsPage.clickButtonInBottomTable("New");
        siebelContactsPage.waitForPageToLoad();
        siebelContactsPage.waitForLoadingImageDisappear();
        siebelContactsPage.inputBottomTableField(SiebelConstants.TYPE, data.get(0).get(SiebelConstants.TYPE)); //Status
        siebelContactsPage.inputBottomTableField(SiebelConstants.STATUS, data.get(0).get(SiebelConstants.STATUS));
        siebelContactsPage.inputBottomTableField("Title", data.get(0).get("Title"));

        siebelContactsPage.inputFirstName(firstName);
        siebelContactsPage.inputLastName(lastName);
        siebelContactsPage.inputBottomTableField("Middle<br>Name", middleName);
        siebelContactsPage.inputSubType(subTypes[0]);
        siebelContactsPage.inputBottomTableField("Gender", data.get(0).get("Gender"));
        siebelContactsPage.inputBottomTableField("DOB", dob);
        siebelContactsPage.inputBottomTableField("Mobile", SiebelUtil.generatePhoneNumber());
        siebelContactsPage.inputSector(sector[0]);
        siebelContactsPage.inputSource(source[0]);
        siebelCompaniesPage.inputABClassification(abClassification[0]);
        siebelCompaniesPage.inputCustomerCategory(customerCategory[0]);
        siebelContactsPage.inputBusinessUnit(businessUnit[0]);
        siebelCompaniesPage.inputPivotBU(data.get(0).get(SiebelConstants.PIVOT_BU));
        Thread.sleep(2000);
        dob = siebelContactsPage.getDateOfBirth();

        siebelContactsPage.clickIndustryCodeIcon();
        siebelCompaniesPage.createIndustryCode(industryCodes[0], industryCdValues[0]);
        siebelCompaniesPage.createIndustryCode(industryCodes[1], industryCdValues[1]);
        siebelCompaniesPage.saveIndustryCodes();
        //Add a wait after saving industry codes
        siebelContactsPage.waitTillMainPageLoads();
        siebelContactsPage.clickReportingGrpIcon(); //
        siebelContactsPage.addNewReportGrp();
        siebelContactsPage.inputReportGrpName(SiebelUtil.generateRandomText(6));
        siebelContactsPage.inputReportGrpSector(sector[0]);
        siebelContactsPage.inputReportGrpSource(source[0]);
        siebelContactsPage.inputReportGrpSalesBU(businessUnit[0]);
        siebelContactsPage.clickReportGrpRelManagerIcon();
        siebelContactsPage.clickRelManagerOwnerAdviserBtn();
        siebelContactsPage.saveReportingGroup();
        siebelContactsPage.clickReportGroupOkButton();
        siebelContactsPage.waitForPageToLoad();
        // Select existing sales rep.
        siebelContactsPage.clickSalesRepIcon();
        siebelContactsPage.clickShowAvailableButton();
        siebelContactsPage.inputSalesRepName("Neeson");
        siebelContactsPage.searchSalesRep();
        siebelContactsPage.clickAddSaleRepButton();
        siebelContactsPage.selectPrimarySalesRep();
        siebelContactsPage.clickSalesRepOkButton();
        siebelContactsPage.waitForPageToLoad();
        Thread.sleep(2000);
        //siebelReportingGroup.saveReportingGroup();
        siebelContactsPage.clickSalesRepIcon();
        siebelContactsPage.removeNonPrimaryField();
        siebelContactsPage.clickSalesRepOkButton();
        siebelContactsPage.waitForPageToLoad();
        Thread.sleep(2000);
        siebelContactsPage.clickButtonInBottomTable("Save");
        //End
        siebelContactsPage.clickLastName();
        siebelId = siebelCompaniesPage.getSiebelId();
        logger.info("Source Siebel Id generated: " + siebelId);

        siebelContactsPage.clickAddress();
        siebelContactsPage.clickAddAddress("L 1  1 Shelley Street", "Mailing");
        siebelCompaniesPage.clickContactInformation();
        siebelCompaniesPage.clickAddNewPhoneButton();
        siebelCompaniesPage.addPhoneType("Home");
        siebelCompaniesPage.addPhoneNumber("+61468300100");

        siebelContactsPage.waitForPageToLoad();
        siebelContactsPage.waitForSaveImageDisappear();

        //Click the Companies Tab
        siebelHomePage.clickTab("Contacts");
        siebelHomePage.waitForLoadingImageDisappear();

        //Create destination Contact

        siebelContactsPage.clickButtonInBottomTable("New");
        siebelContactsPage.waitForPageToLoad();
        siebelContactsPage.waitForLoadingImageDisappear();
        siebelContactsPage.inputBottomTableField(SiebelConstants.TYPE, data.get(0).get(SiebelConstants.TYPE)); //Status
        siebelContactsPage.inputBottomTableField(SiebelConstants.STATUS, data.get(0).get(SiebelConstants.STATUS));
        siebelContactsPage.inputBottomTableField("Title", data.get(0).get("Title"));

        siebelContactsPage.inputFirstName(firstName);
        siebelContactsPage.inputLastName(lastName);
        siebelContactsPage.inputSubType(subTypes[1]);
        siebelContactsPage.inputBottomTableField("Gender", data.get(0).get("Gender"));
        siebelContactsPage.inputBottomTableField("DOB", dob);
        siebelContactsPage.inputBottomTableField("Mobile", SiebelUtil.generatePhoneNumber());
        siebelContactsPage.inputSector(sector[1]);
        siebelContactsPage.inputSource(source[1]);
        siebelCompaniesPage.inputABClassification(abClassification[1]);
        siebelCompaniesPage.inputCustomerCategory(customerCategory[1]);
        siebelContactsPage.inputBusinessUnit(businessUnit[1]);

        siebelContactsPage.clickReportingGrpIcon(); //
        siebelContactsPage.addNewReportGrp();
        siebelContactsPage.inputReportGrpName(SiebelUtil.generateRandomText(6));
        siebelContactsPage.inputReportGrpSector(sector[1]);
        siebelContactsPage.inputReportGrpSource(source[1]);
        siebelContactsPage.inputReportGrpSalesBU(businessUnit[1]);
        siebelContactsPage.clickReportGrpRelManagerIcon();
        siebelContactsPage.clickRelManagerOwnerAdviserBtn();
        siebelContactsPage.saveReportingGroup();
        siebelContactsPage.clickReportGroupOkButton();
        siebelContactsPage.waitForPageToLoad();
        // Select existing sales rep.
        siebelContactsPage.clickSalesRepIcon();
        siebelContactsPage.clickShowAvailableButton();
        siebelContactsPage.inputSalesRepName("Neeson");
        siebelContactsPage.searchSalesRep();
        siebelContactsPage.clickAddSaleRepButton();
        siebelContactsPage.selectPrimarySalesRep();
        siebelContactsPage.clickSalesRepOkButton();
        siebelContactsPage.waitForPageToLoad();
        Thread.sleep(2000);
        siebelContactsPage.clickSalesRepIcon();
        siebelContactsPage.removeNonPrimaryField();
        siebelContactsPage.clickSalesRepOkButton();
        siebelContactsPage.waitForPageToLoad();
        Thread.sleep(2000);
        siebelContactsPage.clickButtonInBottomTable("Save");
        //End
        siebelContactsPage.clickLastName();
        destinationSiebelId = siebelCompaniesPage.getSiebelId();
        logger.info("Destination Contact Siebel Id generated: " + destinationSiebelId);

        //destinationSiebelId
        siebelContactsPage.clickAddress();
        siebelContactsPage.clickAddAddress("L 1  1 Shelley Street", "Business");
        siebelCompaniesPage.clickContactInformation();
        siebelCompaniesPage.clickAddNewPhoneButton();
        siebelCompaniesPage.addPhoneType("Business");
        siebelCompaniesPage.addPhoneNumber("+61468300111");
        siebelContactsPage.waitForPageToLoad();
        siebelContactsPage.waitForSaveImageDisappear();
    }

    @Then("^I select the contacts and merge the records$")
    public void i_select_the_contacts_and_merge_the_records() throws Throwable {
        siebelContactsPage.selectRecordsToMerge();
        siebelContactsPage.waitForLoadingImageDisappear();
        siebelContactsPage.clickContactsMenu();
        siebelContactsPage.mergeContacts();
        siebelCompaniesPage.waitForLoadingImageDisappear();
        siebelCompaniesPage.waitForSaveImageDisappear();
    }

    @Then("^I validate the merged record details for Contacts$")
    public void i_validate_the_merged_record_details_for_Contacts() throws Throwable {
        siebelContactsPage.clickLastName();
        siebelContactsPage.waitForLoadingImageDisappear();
        String contactSubType = siebelContactsPage.getSubtypeValue();
        String contactSectorValue = siebelContactsPage.getSectorValue();
        logger.info("*** contactSubType "+contactSubType);
        logger.info("*** contactSectorValue "+contactSectorValue);
        Assert.assertEquals("*** Contact SubTypes does not match ---",subTypes[1].toUpperCase(),contactSubType.toUpperCase());
        Assert.assertEquals("*** Contact Sector does not match ---",sector[1].toUpperCase(),contactSectorValue.toUpperCase());
    }


    @Then("^I get the Siebel Id for the new contact$")
    public void i_get_the_MDM_Id_for_the_new_contact() throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelContactsPage.clickLastName();
        siebelId = siebelCompaniesPage.getSiebelId();
        logger.info("Siebel Id generated: " + siebelId);
    }

    @Then("^I create Address and Contact Information$")
    public void i_create_Address_and_Contact_Information() throws Throwable {
        siebelContactsPage.clickAddress();
        siebelContactsPage.clickAddOrEditAddress();
        siebelCompaniesPage.clickContactInformation();
        siebelCompaniesPage.clickAddNewPhoneButton();
        siebelCompaniesPage.addPhoneType("Business");
        siebelCompaniesPage.addPhoneNumber("+61468300100");
/*
        siebelCompaniesPage.clickAddNewPhoneButton();
        siebelCompaniesPage.addAnotherPhoneType("Mobile");
        siebelCompaniesPage.addPhoneNumber("+61468311111");
        siebelCompaniesPage.scrollPageDown();
        siebelContactsPage.waitForFaxTab();
        siebelContactsPage.clickAddNewFax();
        siebelContactsPage.clickFaxNumberColumn();
        siebelContactsPage.inputFaxNumber("+61444555777");
        siebelContactsPage.clickAddNewFax();
        siebelContactsPage.addSecondaryFaxNumber();
        siebelContactsPage.inputFaxNumber("+61444777888");
        siebelCompaniesPage.scrollPageDown();
        siebelContactsPage.waitForEmailTab();
        siebelContactsPage.clickAddNewEmail();
        siebelContactsPage.clickEmailAddrColumn();
        siebelContactsPage.inputEmailAddress(SiebelUtil.generateEmail());
        siebelContactsPage.clickAddNewEmail();
        siebelContactsPage.addSecondaryEmail();
        siebelContactsPage.inputEmailAddress(SiebelUtil.generateEmail());
        siebelCompaniesPage.addEmailAddress();
*/
    }

    @Then("^I verify the details using GetAdminclient SOAP request$")
    public void i_verify_the_details_using_getAdminClient_SOAP_request() throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        webServiceRequests = new WebServiceRequests();
        XMLParser xmlParser = new XMLParser();
        container.siebelId = "78684620";
        String response = webServiceRequests.getAdminClientRequest(container.siebelId, "1000001");
        assertField("FirstName", siebelContactsPage.getContactFirstName(), xmlParser.getNodeElement(response, pc.getString("getPartyFirstName")));
        assertField("LastName", siebelContactsPage.getContactLastName(), xmlParser.getNodeElement(response, pc.getString("getPartyLastName")));
        assertField("RoleValue", siebelContactsPage.getContactType(), xmlParser.getNodeElement(response, pc.getString("getPartyRoleValue")));
        assertField("Title", siebelContactsPage.getContactTitle(), xmlParser.getNodeElement(response, pc.getString("getPartyPrefixValue")));

    }

    @Given("^I Open the Siebel Application$")
    public void i_Open_the_Siebel_Application() throws Throwable {
        siebelLoginPage = new SiebelLoginPage(driver);
        siebelLoginPage.openApplication();
    }

    @Given("^I Login to Siebel Application  with username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void i_Login_to_Siebel_Application_with_username_and_password(String username, String password) throws Throwable {
        password = pc.getString(username);
        if (username.equalsIgnoreCase("mpfuser1")) {
            if (siebelLoginPage == null) {
                siebelLoginPage = new SiebelLoginPage(driver);
            }
            siebelLoginPage.closePerformanceDashBoard();
        }
        Thread.sleep(2000);
        siebelLoginPage = new SiebelLoginPage(driver);
        //siebelLoginPage.loginApplication(username, new String(Base64.decodeBase64(password.getBytes())));
        siebelLoginPage.loginApplication(username, password);
    }

    @Given("^I navigate to the \"([^\"]*)\" tab on Siebel Home Page$")
    public void i_navigate_to_the_tab_on_Siebel_Home_Page(String tab) throws Throwable {
        try {
            siebelHomePage = new SiebelHomePage(driver);
            siebelCompaniesPage = new SiebelCompaniesPage(driver);
            siebelHomePage.clickTab(tab);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Given("^I create a new Company$")
    public void i_create_a_new_Company(DataTable dataTable) throws Throwable {
        data = dataTable.asMaps(String.class, String.class);
        String[] industryCodes = data.get(0).get(SiebelConstants.INDUSTRY_CODES).split(",");
        String[] industryCdValues = data.get(0).get(SiebelConstants.INDUSTRY_CODES_VALUES).split(",");
        companyName = siebelCompaniesPage.generateCompanyName().toUpperCase(); // name
        //name = "Test_Automation_3";
        siebelCompaniesPage.waitForHomePageToLoad();
        //siebelCompaniesPage.changePosition();
        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickNewCompanyBtn();
        siebelCompaniesPage.inputCompanyName(companyName);
        siebelCompaniesPage.inputCompanyType(data.get(0).get(SiebelConstants.TYPE));
        siebelCompaniesPage.inputCompanySubType(data.get(0).get(SiebelConstants.SUB_TYPE));
        siebelCompaniesPage.inputDear("Company");
        siebelCompaniesPage.inputCompanyBusinessType(data.get(0).get(SiebelConstants.BUSINESS_TYPE));
        siebelCompaniesPage.inputStatus(data.get(0).get(SiebelConstants.STATUS));
        siebelCompaniesPage.inputCompanySector(data.get(0).get(SiebelConstants.SECTOR));
        siebelCompaniesPage.inputCompanySource(data.get(0).get(SiebelConstants.SOURCE));
        siebelCompaniesPage.inputArbn(data.get(0).get(SiebelConstants.ARBN));
        siebelCompaniesPage.inputAbsCode(data.get(0).get(SiebelConstants.ABS_CODE));
        siebelCompaniesPage.inputAbn(data.get(0).get(SiebelConstants.ABN));

        siebelCompaniesPage.inputAcn(data.get(0).get(SiebelConstants.ACN));
        siebelCompaniesPage.inputIndustrySoftware(data.get(0).get(SiebelConstants.INDUSTRY_SW));
        siebelCompaniesPage.inputaccSoftware(data.get(0).get(SiebelConstants.ACCOUNT_SW));
        //Industry code-
        siebelCompaniesPage.getIndustryCodePopupWindow();
        siebelCompaniesPage.createIndustryCode(industryCodes[0], industryCdValues[0]);
        siebelCompaniesPage.createIndustryCode(industryCodes[1], industryCdValues[1]);
        siebelCompaniesPage.saveIndustryCodes();

        siebelCompaniesPage.inputABClassification(data.get(0).get(SiebelConstants.AB_CLASSIFICATION));
        siebelCompaniesPage.inputCustomerCategory(data.get(0).get(SiebelConstants.CUSTOMER_CATEGORY));
        siebelCompaniesPage.inputBusinessCode(data.get(0).get(SiebelConstants.BUSINESS_CODE));
        siebelCompaniesPage.inputBusinessSize(data.get(0).get(SiebelConstants.BUSINESS_SIZE));
        siebelCompaniesPage.checkTermAgreement();

        // Select existing sales rep.
        siebelCompaniesPage.clickSalesRepIcon();
        siebelCompaniesPage.clickShowAvailableButton();
        siebelCompaniesPage.inputSalesRepName("Neeson");
        siebelCompaniesPage.searchSalesRep();
        siebelCompaniesPage.clickAddSaleRepButton();
        siebelCompaniesPage.selectPrimarySalesRep();
        siebelCompaniesPage.clickSalesRepOkButton();
        siebelCompaniesPage.waitForPageToLoad();
        Thread.sleep(2000);

        siebelCompaniesPage.clickSalesRepIcon();
        siebelCompaniesPage.removeNonPrimaryField();
        siebelCompaniesPage.clickSalesRepOkButton();
        siebelCompaniesPage.waitForPageToLoad();
        Thread.sleep(2000);

        siebelCompaniesPage.inputCompanySalesBU(data.get(0).get(SiebelConstants.SALES_BU));
        siebelCompaniesPage.inputPivotBU(data.get(0).get(SiebelConstants.PIVOT_BU));

        siebelCompaniesPage.getReportingGroupPopupWindow();
        siebelCompaniesPage.getAvailableReportingGroup();
        siebelCompaniesPage.searchAndAddReportingGroup(data.get(0).get(SiebelConstants.REPORTING_GROUP_ID));
        siebelCompaniesPage.waitForPageToLoad();
        siebelCompaniesPage.scrollPageUp();
        siebelCompaniesPage.saveCompany();
        siebelCompaniesPage.waitForLoadingImageDisappear();
        siebelCompaniesPage.waitForSaveImageDisappear();
        Thread.sleep(2000);
        siebelId = siebelCompaniesPage.getSiebelId();
        logger.info("**** Siebel Id ---" + siebelId);
        //companyName = siebelCompaniesPage.getCompanyName();
        logger.info("**** Company Name ---" + companyName);
    }

    @Given("^I create  new Companies to test merge scenario$")
    public void i_create_new_Companies_to_test_merge_scenario(DataTable dataTable) throws Throwable {
        data = dataTable.asMaps(String.class, String.class);

        String[] industryCodes = data.get(0).get(SiebelConstants.INDUSTRY_CODES).split(",");
        String[] industryCdValues = data.get(0).get(SiebelConstants.INDUSTRY_CODES_VALUES).split(",");

        subTypes = data.get(0).get(SiebelConstants.SUB_TYPE).split(",");
        sector = data.get(0).get(SiebelConstants.SECTOR).split(",");
        salesBU = data.get(0).get(SiebelConstants.SALES_BU).split(",");
        absCode = data.get(0).get(SiebelConstants.ABS_CODE).split(",");
        abClassification = data.get(0).get(SiebelConstants.AB_CLASSIFICATION).split(",");
        pivotBU = data.get(0).get(SiebelConstants.PIVOT_BU).split(",");

        companyName = siebelCompaniesPage.generateCompanyName().toUpperCase(); // name
        //name = "Test_Automation_3";
        siebelCompaniesPage.waitForHomePageToLoad();
        //siebelCompaniesPage.changePosition();
        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickNewCompanyBtn();
        siebelCompaniesPage.inputCompanyName(companyName);
        siebelCompaniesPage.inputCompanyType(data.get(0).get(SiebelConstants.TYPE));
        siebelCompaniesPage.inputCompanySubType(subTypes[0]);
        siebelCompaniesPage.inputDear("Company");
        siebelCompaniesPage.inputCompanyBusinessType(data.get(0).get(SiebelConstants.BUSINESS_TYPE));
        siebelCompaniesPage.inputStatus(data.get(0).get(SiebelConstants.STATUS));
        siebelCompaniesPage.inputCompanySector(sector[0]);
        siebelCompaniesPage.inputCompanySource(data.get(0).get(SiebelConstants.SOURCE));
        siebelCompaniesPage.inputArbn(data.get(0).get(SiebelConstants.ARBN));
        siebelCompaniesPage.inputAbsCode(absCode[0]);
        siebelCompaniesPage.inputAbn(data.get(0).get(SiebelConstants.ABN));

        siebelCompaniesPage.inputAcn(data.get(0).get(SiebelConstants.ACN));
        siebelCompaniesPage.inputIndustrySoftware(data.get(0).get(SiebelConstants.INDUSTRY_SW));
        siebelCompaniesPage.inputaccSoftware(data.get(0).get(SiebelConstants.ACCOUNT_SW));
        //Industry code-
        siebelCompaniesPage.getIndustryCodePopupWindow();
        siebelCompaniesPage.createIndustryCode(industryCodes[0], industryCdValues[0]);
        siebelCompaniesPage.createIndustryCode(industryCodes[1], industryCdValues[1]);
        siebelCompaniesPage.saveIndustryCodes();

        siebelCompaniesPage.inputABClassification(abClassification[0]);
        siebelCompaniesPage.inputCustomerCategory(data.get(0).get(SiebelConstants.CUSTOMER_CATEGORY));
        siebelCompaniesPage.inputBusinessCode(data.get(0).get(SiebelConstants.BUSINESS_CODE));
        siebelCompaniesPage.inputBusinessSize(data.get(0).get(SiebelConstants.BUSINESS_SIZE));
        siebelCompaniesPage.checkTermAgreement();

        // Select existing sales rep.
        siebelCompaniesPage.clickSalesRepIcon();
        siebelCompaniesPage.clickShowAvailableButton();
        siebelCompaniesPage.inputSalesRepName("Neeson");
        siebelCompaniesPage.searchSalesRep();
        siebelCompaniesPage.clickAddSaleRepButton();
        siebelCompaniesPage.selectPrimarySalesRep();
        siebelCompaniesPage.clickSalesRepOkButton();
        siebelCompaniesPage.waitForPageToLoad();
        Thread.sleep(2000);

        siebelCompaniesPage.clickSalesRepIcon();
        siebelCompaniesPage.removeNonPrimaryField();
        siebelCompaniesPage.clickSalesRepOkButton();
        siebelCompaniesPage.waitForPageToLoad();
        Thread.sleep(2000);

        siebelCompaniesPage.inputCompanySalesBU(salesBU[0]);
        siebelCompaniesPage.inputPivotBU(pivotBU[0]);

        siebelCompaniesPage.getReportingGroupPopupWindow();
        siebelCompaniesPage.getAvailableReportingGroup();
        siebelCompaniesPage.searchAndAddReportingGroup(data.get(0).get(SiebelConstants.REPORTING_GROUP_ID));
        siebelCompaniesPage.waitForPageToLoad();
        siebelCompaniesPage.scrollPageUp();
        siebelCompaniesPage.saveCompany();
        siebelCompaniesPage.waitForLoadingImageDisappear();
        siebelCompaniesPage.waitForSaveImageDisappear();
        Thread.sleep(2000);
        siebelId = siebelCompaniesPage.getSiebelId();
        logger.info("**** Siebel Id ---" + siebelId);
        //companyName = siebelCompaniesPage.getCompanyName();
        logger.info("**** Company Name ---" + companyName);

        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickCompanyQueryBtn();
        siebelCompaniesPage.inputSiebelId(siebelId);
        siebelCompaniesPage.getDetails();
        siebelCompaniesPage.getCompanyDetails(companyName);

        siebelCompaniesPage.clickGetAddresses();
        siebelCompaniesPage.clickAddAddress("L 1  1 Shelley Street", "Mailing");
        siebelCompaniesPage.waitForPageToLoad();
        siebelCompaniesPage.clickContactInformation();
        siebelCompaniesPage.clickAddNewPhoneButton();
        siebelCompaniesPage.addPhoneType("Home");
        siebelCompaniesPage.addPhoneNumber("+61468300100");

        siebelCompaniesPage.waitForPageToLoad();
        siebelCompaniesPage.waitForSaveImageDisappear();

        //destination company
        siebelCompaniesPage.waitForHomePageToLoad();
        //siebelCompaniesPage.changePosition();
        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickNewCompanyBtn();
        siebelCompaniesPage.inputCompanyName(companyName);
        siebelCompaniesPage.inputCompanyType(data.get(0).get(SiebelConstants.TYPE));
        siebelCompaniesPage.inputCompanySubType(subTypes[1]);
        siebelCompaniesPage.inputDear("Company");
        siebelCompaniesPage.inputCompanyBusinessType(data.get(0).get(SiebelConstants.BUSINESS_TYPE));
        siebelCompaniesPage.inputStatus(data.get(0).get(SiebelConstants.STATUS));
        siebelCompaniesPage.inputCompanySector(sector[1]);
        siebelCompaniesPage.inputCompanySource(data.get(0).get(SiebelConstants.SOURCE));
        siebelCompaniesPage.inputArbn(data.get(0).get(SiebelConstants.ARBN));
        siebelCompaniesPage.inputAbsCode(absCode[1]);
        siebelCompaniesPage.inputAbn(data.get(0).get(SiebelConstants.ABN));

        siebelCompaniesPage.inputAcn(data.get(0).get(SiebelConstants.ACN));
        siebelCompaniesPage.inputIndustrySoftware(data.get(0).get(SiebelConstants.INDUSTRY_SW));
        siebelCompaniesPage.inputaccSoftware(data.get(0).get(SiebelConstants.ACCOUNT_SW));

        siebelCompaniesPage.inputABClassification(abClassification[1]);
        siebelCompaniesPage.inputCustomerCategory(data.get(0).get(SiebelConstants.CUSTOMER_CATEGORY));
        siebelCompaniesPage.inputBusinessCode(data.get(0).get(SiebelConstants.BUSINESS_CODE));
        siebelCompaniesPage.inputBusinessSize(data.get(0).get(SiebelConstants.BUSINESS_SIZE));
        siebelCompaniesPage.checkTermAgreement();

        // Select existing sales rep.
        siebelCompaniesPage.clickSalesRepIcon();
        siebelCompaniesPage.clickShowAvailableButton();
        siebelCompaniesPage.inputSalesRepName("Neeson");
        siebelCompaniesPage.searchSalesRep();
        siebelCompaniesPage.clickAddSaleRepButton();
        siebelCompaniesPage.selectPrimarySalesRep();
        siebelCompaniesPage.clickSalesRepOkButton();
        siebelCompaniesPage.waitForPageToLoad();
        Thread.sleep(2000);

        siebelCompaniesPage.clickSalesRepIcon();
        siebelCompaniesPage.removeNonPrimaryField();
        siebelCompaniesPage.clickSalesRepOkButton();
        siebelCompaniesPage.waitForPageToLoad();
        Thread.sleep(2000);

        siebelCompaniesPage.inputCompanySalesBU(salesBU[1]);
        siebelCompaniesPage.inputPivotBU(pivotBU[1]);

        siebelCompaniesPage.getReportingGroupPopupWindow();
        siebelCompaniesPage.getAvailableReportingGroup();
        siebelCompaniesPage.searchAndAddReportingGroup(data.get(0).get(SiebelConstants.REPORTING_GROUP_ID));
        siebelCompaniesPage.waitForPageToLoad();
        siebelCompaniesPage.scrollPageUp();
        siebelCompaniesPage.saveCompany();
        siebelCompaniesPage.waitForLoadingImageDisappear();
        siebelCompaniesPage.waitForSaveImageDisappear();
        Thread.sleep(2000);
        destinationSiebelId = siebelCompaniesPage.getSiebelId();
        logger.info("**** destinationSiebel Id  ---" + destinationSiebelId);
        //companyName = siebelCompaniesPage.getCompanyName();
        logger.info("**** Company Name ---" + companyName);

        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickCompanyQueryBtn();
        siebelCompaniesPage.inputSiebelId(destinationSiebelId);
        siebelCompaniesPage.getDetails();
        siebelCompaniesPage.getCompanyDetails(companyName);

        siebelCompaniesPage.clickGetAddresses();
        siebelCompaniesPage.clickAddAddress("L 1  1 Shelley Street", "Business");
        siebelCompaniesPage.waitForPageToLoad();
        siebelCompaniesPage.clickContactInformation();
        siebelCompaniesPage.clickAddNewPhoneButton();
        siebelCompaniesPage.addPhoneType("Business");
        siebelCompaniesPage.addPhoneNumber("+61468300111");

        siebelCompaniesPage.waitForPageToLoad();
        siebelCompaniesPage.waitForSaveImageDisappear();
        //end
    }

    @Given("^I search for new Companies and Push the details to Pivot$")
    public void i_search_for_new_Companies_and_Push_the_details_to_Pivot() throws Throwable {
        //companyName = "COMPANY_ZZ_1729";
        siebelCompaniesPage.clickCompanyQueryBtn();
        siebelCompaniesPage.searchCompanyByName(companyName);
        siebelCompaniesPage.clickCompanyGoBtn();
        siebelCompaniesPage.pushToPivot();
    }

    @Then("^I select the Companies and merge the records$")
    public void i_select_the_Companies_and_merge_the_records() throws Throwable {
        siebelCompaniesPage.selectRecordsToMerge();
        siebelCompaniesPage.waitForLoadingImageDisappear();
        siebelCompaniesPage.clickCompaniesMenu();
        siebelCompaniesPage.mergeCompanies();
        siebelCompaniesPage.waitForLoadingImageDisappear();
        siebelCompaniesPage.waitForSaveImageDisappear();
    }

    @Then("^I validate the merged company details$")
    public void i_validate_the_merged_company_details() throws Throwable {
        siebelCompaniesPage.clickPrimaryOrgId();
        siebelCompaniesPage.getCompanyDetails(companyName);
        logger.info("SubType ---Expected---" + subTypes[1] + "---Actual---" + siebelCompaniesPage.getSubTypeValue());
        logger.info("Sector ---Expected---" + sector[1] + "---Actual---" + siebelCompaniesPage.getSectorValue());
        logger.info("Sales Business Unit ---Expected---" + salesBU[1] + "---Actual---" + siebelCompaniesPage.getSalesBUValue());
        logger.info("ABS Code ---Expected---" + absCode[1] + "---Actual---" + siebelCompaniesPage.getABSCodeValue());
        logger.info("AB Classification ---Expected---" + abClassification[1] + "---Actual---" + siebelCompaniesPage.getABClassificationUnit());

        Assert.assertEquals("SubType", subTypes[1], siebelCompaniesPage.getSubTypeValue());
        Assert.assertEquals("Sector", sector[1], siebelCompaniesPage.getSectorValue());
        Assert.assertEquals("Sales Business Unit", salesBU[1], siebelCompaniesPage.getSalesBUValue());
        Assert.assertEquals("ABS Code", absCode[1], siebelCompaniesPage.getABSCodeValue());
        Assert.assertEquals("AB Classification", abClassification[1], siebelCompaniesPage.getABClassificationUnit());
    }

    @Given("^I click on Companies tab on home page$")
    public void i_click_on_Companies_tab_on_home_page() throws Throwable {
        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickCompanyQueryBtn();
        siebelCompaniesPage.inputSiebelId(siebelId);
        siebelCompaniesPage.getDetails();
        siebelCompaniesPage.getCompanyDetails(companyName);
    }

    @Given("^I create Address and Contact Information of the Company$")
    public void i_create_Address_and_Contact_Information_of_the_Company() throws Throwable {
        siebelCompaniesPage.clickGetAddresses();
        siebelCompaniesPage.clickAddAddress();
        siebelCompaniesPage.waitForPageToLoad();
        siebelCompaniesPage.clickContactInformation();
        siebelCompaniesPage.clickAddNewPhoneButton();
        siebelCompaniesPage.addPhoneType("Business");
        siebelCompaniesPage.addPhoneNumber("+61468300100");
/*
        siebelCompaniesPage.clickAddNewPhoneButton();
        siebelCompaniesPage.addAnotherPhoneType("Mobile");
        siebelCompaniesPage.addPhoneNumber("+61468311111");
        siebelCompaniesPage.scrollPageDown();
        siebelCompaniesPage.waitForEmailTab();
        siebelCompaniesPage.clickAddNewEmail();
        siebelCompaniesPage.clickEmailAddrColumn();
        siebelCompaniesPage.inputEmailAddress(SiebelUtil.generateEmail());
        siebelCompaniesPage.clickAddNewEmail();
        siebelCompaniesPage.addSecondaryEmail();
        siebelCompaniesPage.inputEmailAddress(SiebelUtil.generateEmail());
        //siebelCompaniesPage.
        siebelCompaniesPage.addEmailAddress();
        siebelCompaniesPage.scrollPageDown();
        siebelCompaniesPage.waitForFaxTab();
        siebelCompaniesPage.clickAddNewFax();
        siebelCompaniesPage.clickFaxNumberColumn();
        siebelCompaniesPage.inputFaxNumber("+61444555777");
        siebelCompaniesPage.clickAddNewFax();
        siebelCompaniesPage.addSecondaryFaxNumber();
        siebelCompaniesPage.inputFaxNumber("+61444777888");
*/
    }

    @Then("^I push the details to Pivot$")
    public void i_push_the_details_to_Pivot() throws Throwable {
        siebelCompaniesPage.scrollPageUp();
        Thread.sleep(2000);
        siebelCompaniesPage.scrollPageLeft();
        siebelCompaniesPage.pushToPivot();
        siebelCompaniesPage.scrollPageUp();
        Thread.sleep(2000);
    }

    @Then("^I verify Company details in Siebel and Pivot database$")
    public void I_verify_Company_details_in_Siebel_and_Pivot_database() throws Throwable {
        logger.info("**** Click System Link after a waiting for few seconds ----");
        Thread.sleep(10000);
        siebelCompaniesPage.clickSystemLinks();
        siebelCompaniesPage.scrollToBottom();
        pivotId = siebelCompaniesPage.getPivotId();
        logger.info("****** Companies Pivot Id is ----" + pivotId);
        Thread.sleep(10000);
        HashMap<String, String> companyDetailsMap = JDBCConnection.getCompanyDetailsFromPivot(pivotId);
        Assert.assertEquals("Company Name", companyName.trim(), (companyDetailsMap.get("SHORT_NAME")).trim());
        Assert.assertEquals("Legal Name", companyName.trim(), (companyDetailsMap.get("LEGAL_NAME")).trim());
        Assert.assertEquals("ACN", (data.get(0).get(SiebelConstants.ACN)).trim(), (companyDetailsMap.get("ACN")).trim());
        Assert.assertEquals("ARBN", (data.get(0).get(SiebelConstants.ARBN)).trim(), (companyDetailsMap.get("ARBN")).trim());
    }

    @Then("^I click on more info options and fill the details$")
    public void i_click_on_more_info_options_and_fill_the_details() throws Throwable {
        siebelCompaniesPage.clickMoreInfo();
        siebelCompaniesPage.scrollPageDown();
        Thread.sleep(1000);
        siebelCompaniesPage.inputlegislationType(data.get(0).get(SiebelConstants.LEGISLATION_TYPE));
        siebelCompaniesPage.inputlegislationLoc(data.get(0).get(SiebelConstants.LEGISLATION_LOC));
        siebelCompaniesPage.inputTrustType(data.get(0).get(SiebelConstants.TRUST_TYPE));
        siebelCompaniesPage.inputParentCompany("Macquarie");
        siebelCompaniesPage.inputParentExchange("AAAA");
        siebelCompaniesPage.inputExchange("EXCHNG");
        siebelCompaniesPage.inputMembershipBody("MacquarieMB");
        siebelCompaniesPage.inputRegulatorName("MacquarieRN");
        siebelCompaniesPage.inputRegulatorId("MacquarieRID");
        //siebelCompaniesPage.saveRecord();

    }

    @Given("^I click on Companies tab on home page for editing email$")
    public void i_click_on_Companies_tab_on_home_page_for_editing_email(DataTable dataTable) throws Throwable {
        data = dataTable.asMaps(String.class, String.class);
        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickCompanyQueryBtn();
        siebelId = data.get(0).get("SiebelId");
        companyName = data.get(0).get("CompanyName");
        siebelCompaniesPage.inputSiebelId(siebelId);
        siebelCompaniesPage.getDetails();
        siebelCompaniesPage.getCompanyDetails(companyName);
    }

    @Given("^I edit email details of the company$")
    public void i_edit_email_details_of_the_company() throws Throwable {
        siebelCompaniesPage.clickContactInformation();
        //siebelCompaniesPage.waitTillPrimaryEmailAppear();
        siebelCompaniesPage.scrollPageDown();
        siebelCompaniesPage.updatePrimaryEmail(SiebelUtil.generateEmail());
        Thread.sleep(5000);
    }

    @Given("^I query the contact details with SiebelId$")
    public void i_query_the_contact_details_with_SiebelId(DataTable dataTable) throws Throwable {
        data = dataTable.asMaps(String.class, String.class);
        siebelId = data.get(0).get("SiebelId");
        logger.info("Siebel Id is --" + siebelId);
    }

    @Given("^I query the contact details and update the email$")
    public void i_query_the_contact_details_and_update_the_email() throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelContactsPage.clickContactsQueryButton();
        siebelContactsPage.clickSiebelIdColumn();
        siebelContactsPage.inputSiebelId(siebelId);
        siebelContactsPage.clickContactsGoBtn();
        siebelContactsPage.clickLastName();
        siebelContactsPage.clickContactInformation();
        siebelContactsPage.scrollPageDown();
        siebelContactsPage.waitForEmailTab();
        siebelContactsPage.updatePrimaryEmail(SiebelUtil.generateEmail());
        Thread.sleep(8000);
    }

    @Given("^I push the Contact details to Pivot$")
    public void i_push_the_Contact_details_to_Pivot() throws Throwable {
        siebelContactsPage.clickPushToPivotBtn();
    }

    @Given("^I click on new button on the activities$")
    public void i_click_on_new_button_on_the_activities() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        siebelActivitiesPage = new SiebelActivitiesPage(driver);
        siebelActivitiesPage.clickActivitiesNewButton();
    }

    @Given("^I query the contact details and update the primary phone$")
    public void i_query_the_contact_details_and_update_the_primary_phone() throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelContactsPage.clickContactsQueryButton();
        siebelContactsPage.clickSiebelIdColumn();
        siebelContactsPage.inputSiebelId(siebelId);
        siebelContactsPage.clickContactsGoBtn();
        siebelContactsPage.clickLastName();
        siebelContactsPage.clickContactInformation();
        siebelContactsPage.scrollPageDown();
        siebelContactsPage.getPhoneList();
        siebelContactsPage.checkPrimaryContact();
    }

    @Given("^I create a new Reporting Group$")
    public void i_create_a_new_Reporting_Group(DataTable dataTable) throws Throwable {
        //JDBCConnection jdbc = new JDBCConnection();
        siebelReportingGroup = new SiebelReportingGroup(driver);
        data = dataTable.asMaps(String.class, String.class);
        siebelReportingGroup.clickReportGroupButton();
        siebelReportingGroup.waitForPageToLoad();
        groupName = (SiebelUtil.generateRandomText(5) + "_ReportGroup").toUpperCase();
        siebelReportingGroup.inputReportingGroupName(groupName);
        siebelReportingGroup.inputSector(data.get(0).get(SiebelConstants.SECTOR));
        siebelReportingGroup.inputSource(data.get(0).get(SiebelConstants.SOURCE));
        siebelReportingGroup.inputABClassification(data.get(0).get(SiebelConstants.AB_CLASSIFICATION));
        siebelReportingGroup.inputCustomerOwner(data.get(0).get(SiebelConstants.CUSTOMER_OWNER));
        siebelReportingGroup.inputType(data.get(0).get(SiebelConstants.TYPE));
        siebelReportingGroup.inputStatus(data.get(0).get(SiebelConstants.STATUS));
        siebelReportingGroup.inputGroupType(data.get(0).get(SiebelConstants.GROUP_TYPE));
        siebelReportingGroup.inputDeftCode(data.get(0).get(SiebelConstants.DEFT_CODE));
        siebelReportingGroup.inputSalesBU(data.get(0).get(SiebelConstants.SALES_BU));
        siebelReportingGroup.inputPivotBU(data.get(0).get(SiebelConstants.PIVOT_BU));
        siebelReportingGroup.clickSalesRepIcon();
        siebelReportingGroup.clickShowAvailableButton();
        siebelReportingGroup.inputSalesRepName("Neeson");
        siebelReportingGroup.searchSalesRep();
        siebelReportingGroup.clickAddSaleRepButton();
        siebelReportingGroup.selectPrimarySalesRep();

        siebelReportingGroup.clickSalesRepOkButton();
        siebelReportingGroup.waitForPageToLoad();
        Thread.sleep(2000);
        //siebelReportingGroup.inputPivotBU(data.get(0).get(SiebelConstants.PIVOT_BU));
        siebelReportingGroup.inputServicePlan(data.get(0).get(SiebelConstants.SERVICE_PLAN));
        siebelReportingGroup.waitForPageToLoad();
        siebelReportingGroup.inputPivotBU(data.get(0).get(SiebelConstants.PIVOT_BU));
        siebelReportingGroup.clickDeftChecbox();
        siebelReportingGroup.clickSalesRepIcon();
        siebelReportingGroup.removeNonPrimaryField();
        siebelReportingGroup.clickSalesRepOkButton();
        siebelReportingGroup.saveReportingGroup();
        logger.info("**** Reporting group saved ----");

    }

    @Given("^I click on Reporting Group tab on home page$")
    public void i_click_on_Reporting_Group_tab_on_home_page() throws Throwable {

    }

    @Given("^I verify the Reporting Group  details in Pivot database$")
    public void i_verify_the_Reporting_Group_details_in_Pivot_database() throws Throwable {
        pivotId = siebelReportingGroup.getPivotId();
        logger.info("**** Reporting Group Pivot Id ----" + pivotId);
        HashMap<String, String> reportGroupMap = JDBCConnection.getReportGrpDetailsFromPivot(pivotId);
        Assert.assertEquals("NAME", groupName.trim(), (reportGroupMap.get(SiebelConstants.SHORT_NAME)).trim());
    }

    @Given("^I query the reporting group details and I push the details to pivot$")
    public void i_query_the_reporting_group_details_and_I_push_the_details_to_pivot() throws Throwable {
        siebelReportingGroup.clickReportGrpQuery();
        siebelReportingGroup.searchReportingGroup(groupName);
        siebelReportingGroup.clickReportGrpGoButton();
        siebelReportingGroup.waitForPageToLoad();
        siebelReportingGroup.checkForAlerts();
        siebelReportingGroup.clickPushToPivotButton();
        siebelReportingGroup.waitForPageRefresh();
        siebelReportingGroup.getReportingGroupDetails(groupName);
        siebelReportingGroup.clickSystemLinks();
    }

    @Given("^I get new reporting group details$")
    public void i_get_new_reporting_group_details() throws Throwable {
        siebelReportingGroup.waitForPageToLoad();
        siebelReportingGroup.getReportingGroupDetails(groupName);
        //Not able to get the details from the first click.
        reportGroupId = siebelReportingGroup.getReportGroupId();
        logger.info("**** Report Group ID ---" + reportGroupId);
        Thread.sleep(2000);
        siebelReportingGroup.getReportingGroupDetails(groupName);
    }

    @Then("^I add a new contact details$")
    public void i_add_a_new_contact_details() throws Throwable {
        //siebelReportingGroup.selectPrimaryContact();
        siebelReportingGroup.clickNewContactsButton();
        siebelReportingGroup.clickPrimaryGroupFlag();
        lastName = SiebelUtil.generateRandomText(5);
        firstName = SiebelUtil.generateRandomText(5);
        siebelReportingGroup.inputLastName(lastName);
        siebelReportingGroup.inputFirstName(firstName);
        siebelReportingGroup.inputTitle(data.get(0).get("Title"));
        businessPhone = SiebelUtil.generateRandomNumbers();
        siebelReportingGroup.inputBusinessPhone(businessPhone);
        contactId = siebelReportingGroup.getContactId();
        logger.info("**** Contact ID ----" + contactId);
    }

    @Then("^I add new company details$")
    public void i_add_new_company_details() throws Throwable {
        siebelReportingGroup.scrollPageDown();
        siebelReportingGroup.clickNewCompanyButton();
        companyName = SiebelUtil.generateRandomText(8);
        siebelReportingGroup.inputCompanyName(companyName);
        companyBusinessPhone = SiebelUtil.generateRandomNumbers();
        siebelReportingGroup.inputCompanyBusinessPhone(companyBusinessPhone);
        companyId = siebelReportingGroup.getCompanyId();
        logger.info("**** Company ID ----" + companyId);
        Thread.sleep(5000);
    }

    @Given("^I query the reporting group details$")
    public void i_query_the_reporting_group_details() throws Throwable {
        siebelReportingGroup.clickReportGrpQuery();
        siebelReportingGroup.searchReportingGroup(groupName);
        siebelReportingGroup.clickReportGrpGoButton();
        siebelReportingGroup.waitForPageToLoad();
        siebelReportingGroup.waitForLoadingImageDisappear();
        siebelReportingGroup.waitForSaveImageDisappear();
        siebelReportingGroup.clickReportGroupIdColumm();
        siebelReportingGroup.getReportingGroupDetails(groupName);
        //siebelReportingGroup.checkForAlerts();
    }

    @Then("^I click on contacts name to get  details from contacts page$")
    public void i_click_on_contacts_name_to_get_details_from_contacts_page() throws Throwable {
        siebelReportingGroup.getContactsDetails(lastName);
    }

    @Then("^I verify the contacts details in contacts page with the contacts in relationship tab$")
    public void i_verify_the_contacts_details_in_contacts_page_with_the_contacts_in_relationship_tab() throws Throwable {
        String conFirstName = siebelReportingGroup.getFirstName();
        String conLastName = siebelReportingGroup.getLastName();
        String phoneNumber = siebelReportingGroup.getContactPhoneNumber();
        String reportingGrp = siebelReportingGroup.getReportingGroupName();
        logger.info("**** phone number ----" + phoneNumber + "---- Reporting Group Name ---" + reportingGrp);
        Assert.assertEquals("First Name", firstName, conFirstName);
        Assert.assertEquals("Last Name", lastName, conLastName);
        Assert.assertEquals("Phone Number", businessPhone, phoneNumber);
        Assert.assertEquals("Reporting Group Name ", groupName, reportingGrp);
    }

    @Then("^I click on company name to get  details from company page$")
    public void i_click_on_company_name_to_get_details_from_company_page() throws Throwable {
        siebelReportingGroup.getCompanyDetails(companyName);
    }

    @Then("^I verify the company details in company page with the company in relationship tab$")
    public void i_verify_the_company_details_in_company_page_with_the_company_in_relationship_tab() throws Throwable {
        String company = siebelReportingGroup.getCompanyName();
        logger.info("**** Company created from Reporting Group  is  ---" + company);
        String phone = siebelReportingGroup.getCompanyPhone();
        String compReportingGroup = siebelReportingGroup.getCompanyReportingGroup();
        Assert.assertEquals("Company Name", companyName, company);
        Assert.assertEquals("Company Phone", companyBusinessPhone, phone);
        Assert.assertEquals("Company Reporting Group", groupName, compReportingGroup);
        Thread.sleep(2000);
    }

    @Given("^I Open Pivot Application$")
    public void i_Open_Pivot_Application() throws Throwable {
        windowDriver = new WindowDriver().getWiniumDriver();
        Thread.sleep(3000);

    }

    @Then("^I close the Pivot Application$")
    public void i_close_the_Pivot_Application() throws Throwable {
        pivotLoginWindow.closePivotApplication();
        windowDriver.close();
    }

    @When("^I login to Pivot Application with username and password$")
    public void i_login_to_Pivot_Application_with_username_and_password() throws Throwable {
        pivotLoginWindow = new PivotLoginWindow(windowDriver);
        pivotLoginWindow.clickLoginWindow();
        //pivotLoginWindow.inputUsername("acct_mnt");
        pivotLoginWindow.inputPassword("acct_mnt");
        pivotLoginWindow.selectDatasource("pivot_ext_salmon");
        pivotLoginWindow.clickLoginButton();
        pivotLoginWindow.clickSecurityYesButton();
        //pivotLoginWindow.maximiseWindow();
    }

    @When("^I search for pivot customer number created in Siebel$")
    public void i_search_for_pivot_customer_number_created_in_Siebel() throws Throwable {
        pivotHomeWindow = new PivotHomeWindow(windowDriver);
        pivotHomeWindow.clickToolsMenu();
        pivotHomeWindow.clickFindMenu();
        pivotHomeWindow.clickCustomerNumberRadioButton();
        pivotId ="53945628";
        pivotHomeWindow.setSearchForTextBox(pivotId);//Change the PIVOT ID    50772021
        pivotHomeWindow.clickFindButton();
        pivotHomeWindow.clickSearchResult();
    }

    @Then("^I create a facility for the pivot customer number$")
    public void i_create_a_facility_for_the_pivot_customer_number() throws Throwable {
        pivotHomeWindow = new PivotHomeWindow(windowDriver);
        pivotHomeWindow.clickFileMenu();
        pivotHomeWindow.clickNewMenu();
        pivotHomeWindow.clickFacilityMenu();
        pivotFacilityWindow = new PivotFacilityWindow(windowDriver);
        pivotFacilityWindow.setReportingName("TEST NAME");
        pivotFacilityWindow.setExpiryDate("20/05/25");
        pivotFacilityWindow.clickLoanTab();
        pivotFacilityWindow.selectFundingOptions();
        pivotFacilityWindow.selectInterestRateOptions();
        pivotFacilityWindow.clickLimitsTab();
        pivotFacilityWindow.selectLoanPurposeGroup();
        pivotFacilityWindow.selectLoanPurposeCode();
        pivotFacilityWindow.saveFacility();
        facilityId = pivotFacilityWindow.getFacilityNumber();

        // Following code is for testing 'ONLY' Pivot test steps.
        /*
        pivotFacilityWindow.setCloseWindow();
        //close Pivot Main window.
        pivotFacilityWindow.setCloseWindow();
        */

    /*    if(data.get(0).get(SiebelConstants.IS_FACILITY).equalsIgnoreCase("Y")){
            pivotFacilityWindow.setCloseWindow();
        }*/
    }

    @Then("^I create an Account for the Facility$")
    public void i_create_an_Account_for_the_Facility() throws Throwable {
        pivotAccountWindow = new PivotAccountWindow(windowDriver);
        pivotFacilityWindow.clickFacilityTitleBar();
        pivotFacilityWindow.clickFacilityFileMenu();
        pivotFacilityWindow.clickNewAccountOption();
        pivotAccountWindow.clickAccounts();
        pivotAccountWindow.selectLoanPurposeCode();
        pivotAccountWindow.inputAccountLimit("1000");
        pivotAccountWindow.clickAccountFileMenu();
        pivotAccountWindow.saveAccount();
        pivotAccountWindow.nominateAccountOk();
        pivotAccountWindow.accountLimitValue();
        accountNumber = pivotAccountWindow.getAccountNumber();
        //pivotAccountWindow.closeAccountWindow();

        // For testing Only ,Pivot account creation - Start

         /*   pivotAccountWindow.closeAccountWindow();
         pivotFacilityWindow.setCloseWindow();//Close Facility Window
         pivotFacilityWindow.setCloseWindow();// Close Pivot Main Window*/

        // For testing Pivot account creation - End
        if(data.get(0).get(SiebelConstants.IS_FACILITY).equalsIgnoreCase("N")){
            pivotAccountWindow.closeAccountWindow();//For Account Window
            //Close Facility window only if the scenario is not Additional Account creation
            if(data.get(0).get(SiebelConstants.IS_ADDITIONAL_ACCOUNT).equalsIgnoreCase("N")){
                pivotFacilityWindow.setCloseWindow();//For Facility Window
                pivotFacilityWindow.setCloseWindow();// Pivot Main Window
            }

        }
    }

    @Then("^I create additional Account for the Facility$")
    public void i_create_additional_Account_for_the_Facility() throws Throwable {
        pivotAccountWindow = new PivotAccountWindow(windowDriver);
        pivotFacilityWindow.waitFor(2000);
        pivotFacilityWindow.clickFacilityTitleBar();
        pivotFacilityWindow.clickFacilityFileMenu();
        pivotFacilityWindow.clickNewAccountOption();
        pivotAccountWindow.clickAccounts();
        pivotAccountWindow.selectLoanPurposeCode();
        pivotAccountWindow.inputAccountLimit("9999");
        pivotAccountWindow.clickAccountFileMenu();
        pivotAccountWindow.saveAccount();
        pivotAccountWindow.nominateAccountOk();
        //pivotAccountWindow.accountLimitValue();
        additionalAccNumber = pivotAccountWindow.getAccountNumber();
        if(data.get(0).get(SiebelConstants.IS_FACILITY).equalsIgnoreCase("N")){
            pivotFacilityWindow.setCloseWindow();//For Account Window
        }
        //Close Facility window only if the scenario is not Additional Account creation
        pivotFacilityWindow.setCloseWindow();//For Facility Window
        pivotFacilityWindow.setCloseWindow();// Pivot Main Window
        //}


    }

    @Then("^I search for Company facility and verify the facility number in Siebel$")
    public void i_search_for_Company_facility_and_verify_the_facility_number_in_Siebel() throws Throwable {
        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickCompanyQueryBtn();
        // Only for testing - 78843877
        //siebelId = "78843877";
        siebelCompaniesPage.inputSiebelId(siebelId);
        siebelCompaniesPage.getDetails();
        siebelCompaniesPage.getCompanyDetails(companyName);
        siebelCompaniesPage.clickFinancialAccount();
        String siebelFacilityId = siebelCompaniesPage.getFacilityId();
        Assert.assertEquals("Facility Id in Pivot and Siebel does not match ---",facilityId, siebelFacilityId);
    }

    @Then("^I verify updated facility status in Siebel$")
    public void i_verify_updated_facility_status_in_Siebel() throws Throwable {
        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickCompanyQueryBtn();
        //For Testing
/*        siebelId = "78843877";
        companyName = "COMPANY_UH_4706";*/
        //For Testing
        siebelCompaniesPage.inputSiebelId(siebelId);
        siebelCompaniesPage.getDetails();
        siebelCompaniesPage.getCompanyDetails(companyName);
        siebelCompaniesPage.waitForLoadingImageDisappear();
        siebelCompaniesPage.clickFinancialAccount();
        siebelCompaniesPage.scrollPageDown();
        String status = siebelCompaniesPage.getFacilityStatus();
        Assert.assertEquals("**** Facility Status in Pivot and Siebel does not match ----", "Open" , status);
    }

    @Then("^I verify Company account id in Siebel$")
    public void i_verify_Company_account_id_in_Siebel() throws Throwable {
        String siebelAccNumber = siebelCompaniesPage.getAccountNumber();
        Assert.assertEquals("Account Number in Pivot and Siebel does not match ---",accountNumber, siebelAccNumber);
    }

    @Then("^I verify account number in Siebel$")
    public void i_verify_account_number_in_Siebel() throws Throwable {

    }

    @Then("^I search for accounts created in Pivot and verify it in Siebel$")
    public void i_search_for_accounts_created_in_Pivot_and_verify_it_in_Siebel() throws Throwable {


    }

    @Then("^I search for customer facility created in Pivot$")
    public void i_search_for_customer_facility_created_in_Pivot() throws Throwable {
        pivotHomeWindow = new PivotHomeWindow(windowDriver);
        pivotHomeWindow.clickToolsMenu();
        pivotFacilityWindow = new PivotFacilityWindow(windowDriver);
        pivotFacilityWindow.clickFacilityMenu();
        // remove the hardcorded value and give facility Id to inputFacility Method.
        //facilityId = "206867";
        pivotFacilityWindow.inputFacilityId(facilityId);
        pivotFacilityWindow.clickFacilityOkButton();
    }

    @Then("^I verify updated Company facility status in Siebel$")
    public void i_verify_updated_Company_facility_status_in_Siebel() throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelContactsPage.clickContactsQueryButton();
        siebelContactsPage.clickSiebelIdColumn();
        //Hardcode Siebel Id - 78843652
        //siebelId = "78843908";
        siebelContactsPage.inputSiebelId(siebelId);
        siebelContactsPage.clickContactsGoBtn();
        siebelContactsPage.clickLastName();
        siebelContactsPage.clickFinancialAccount();
        siebelContactsPage.getFacilityStatus();
    }

    @Given("^I update and save facility values$")
    public void i_update_and_save_facility_values() throws Throwable {
        pivotFacilityWindow.selectModifyFacilityOption();
        pivotFacilityWindow.editFacility();
        pivotFacilityWindow.saveFacilityValues();
        pivotFacilityWindow.clickAccountLimitBtn();
        pivotFacilityWindow.closeFacilityEditWindow();
        pivotFacilityWindow.setCloseWindow();
    }

    @Then("^I search for pivot customer account created in Pivot$")
    public void i_search_for_pivot_customer_account_created_in_Pivot() throws Throwable {
        pivotHomeWindow = new PivotHomeWindow(windowDriver);
        pivotHomeWindow.clickToolsMenu();
        pivotAccountWindow = new PivotAccountWindow(windowDriver);
        pivotAccountWindow.clickAccountMenu();
        //replace the hardcoded value with accountId. //287736383
        //accountNumber = "214065583";
        pivotAccountWindow.inputAccountId(accountNumber);
        pivotAccountWindow.clickAccountOk();
    }

    @Then("^I update and save the account values$")
    public void i_update_and_save_the_account_values() throws Throwable {
        pivotAccountWindow.selectModifyAccountOption();
        pivotAccountWindow.editAccount();
        pivotAccountWindow.saveAccountValues();
        pivotAccountWindow.clickAccountLimitBtn();
        pivotAccountWindow.closeAccountEditWindow();
        pivotAccountWindow.setCloseWindow();
    }
    @Given("^I close browser session$")
    public void i_close_browser_session() throws Throwable {
        driver.close();
    }

    @Given("^I open a \"([^\"]*)\" browser session$")
    public void i_open_a_browser_session(String browser) throws Throwable {
        driver = new DownloadDrivers().getDriver("ie");
    }

    @Then("^I verify updated account in Siebel$")
    public void i_verify_updated_account_in_Siebel() throws Throwable {
        siebelCompaniesPage.clickTab("Companies");
        siebelCompaniesPage.clickCompanyQueryBtn();
        //For Testing
        /*  siebelId = "78843877";
        companyName = "COMPANY_UH_4706";*/
        //For Testing
        siebelCompaniesPage.inputSiebelId(siebelId);
        siebelCompaniesPage.getDetails();
        siebelCompaniesPage.getCompanyDetails(companyName);
        siebelCompaniesPage.waitForLoadingImageDisappear();
        siebelCompaniesPage.clickFinancialAccount();
        siebelCompaniesPage.scrollPageDown();
        String status = siebelCompaniesPage.getAccountStatus();
        Assert.assertEquals("**** Account Status in Pivot and Siebel does not match ----", "Open" , status);
    }

    @Then("^I navigate to relationship tab to get Reporting Group details$")
    public void i_navigate_to_relationship_tab_to_get_Reporting_Group_details() throws Throwable {
        siebelContactsPage.clickLastName();
        siebelContactsPage.clickRelationshipTab();
        siebelContactsPage.waitForLoadingImageDisappear();
        siebelContactsPage.scrollPageDown();
    }

    @Then("^I navigate to reporting group home page to push Group to Pivot$")
    public void i_navigate_to_reporting_group_home_page_to_push_Group_to_Pivot() throws Throwable {
        siebelContactsPage.clickReportGroup();
        siebelContactsPage.waitForLoadingImageDisappear();
        siebelContactsPage.scrollPageUp();
    }

    @Then("^I click on Group System Links to get Report Group Pivot Id$")
    public void i_click_on_Group_System_Links_to_get_Report_Group_Pivot_Id() throws Throwable {
        //siebelContactsPage.clickRGSalesRep();
        siebelReportingGroup = new SiebelReportingGroup(driver);
        siebelReportingGroup.clickSalesRepIcon();
        siebelReportingGroup.clickShowAvailableButton();
        siebelReportingGroup.inputSalesRepName("Neeson");
        siebelReportingGroup.searchSalesRep();
        siebelReportingGroup.clickAddSaleRepButton();
        siebelReportingGroup.selectPrimarySalesRep();

        siebelReportingGroup.clickSalesRepOkButton();
        siebelReportingGroup.waitForPageToLoad();
        siebelReportingGroup.waitForLoadingImageDisappear();
        siebelReportingGroup.waitForSaveImageDisappear();
        Thread.sleep(2000);

        siebelReportingGroup.clickSalesRepIcon();
        siebelReportingGroup.removeNonPrimaryField();
        siebelReportingGroup.clickSalesRepOkButton();
        siebelReportingGroup.waitForPageRefresh();
        siebelReportingGroup.waitForPageToLoad();
        siebelReportingGroup.clickPushToPivotButton();
        siebelReportingGroup.clickSystemLinks();
        siebelReportingGroup.getReportGroupPivotId();
    }

    @Then("^I create and save a facility and then I modify the facility status$")
    public void i_create_save_facility_then_modify_facility_status() throws Throwable
    {
        //Create a new facility
        pivotHomeWindow = new PivotHomeWindow(windowDriver);
        pivotHomeWindow.clickFileMenu();
        pivotHomeWindow.clickNewMenu();
        pivotHomeWindow.clickFacilityMenu();
        pivotFacilityWindow = new PivotFacilityWindow(windowDriver);
        pivotFacilityWindow.setReportingName("TEST NAME");
        pivotFacilityWindow.setExpiryDate("20/05/25");
        pivotFacilityWindow.clickLoanTab();
        pivotFacilityWindow.selectFundingOptions();
        pivotFacilityWindow.selectInterestRateOptions();
        pivotFacilityWindow.clickLimitsTab();
        pivotFacilityWindow.selectLoanPurposeGroup();
        pivotFacilityWindow.selectLoanPurposeCode();
        pivotFacilityWindow.saveFacility();
        facilityId = pivotFacilityWindow.getFacilityNumber();
        //Modify the facility
        pivotFacilityWindow.clickModifyFacility();
        pivotFacilityWindow.selectFacilityStatus();
        pivotFacilityWindow.changeFacilityValues();
    }

    @Then("^I create and save account and then I modify the account status$")
    public void i_create_save_account_then_modify_account_status() throws Throwable
    {
        //Create an account
        pivotAccountWindow = new PivotAccountWindow(windowDriver);
        pivotFacilityWindow.clickFacilityTitleBar();
        pivotFacilityWindow.clickFacilityFileMenu();
        pivotFacilityWindow.clickNewAccountOption();
        pivotAccountWindow.clickAccounts();
        pivotAccountWindow.selectLoanPurposeCode();
        pivotAccountWindow.inputAccountLimit("1000");
        pivotAccountWindow.saveAccount();
        pivotAccountWindow.nominateAccountOk();
        pivotAccountWindow.accountLimitValue();
        accountNumber = pivotAccountWindow.getAccountNumber();
        //Close windows
        pivotAccountWindow.closeAccountWindow();
        pivotFacilityWindow.closeFacilityWindow();
        //Search Account and modify it.
        pivotAccountWindow.gotoAccountNumberSearch();
        pivotAccountWindow.inputAccountId(accountNumber);
        pivotAccountWindow.clickAccountOk();
        pivotAccountWindow.clickAccountModify();
        pivotAccountWindow.changeAccountStatus();
        pivotAccountWindow.closeAccountWindow();

    }

    @Then("^I search for siebel Id and verify facility and account status$")
    public void i_search_siebelId_and_verify_facility_and_account_status() throws Throwable {
        siebelContactsPage = new SiebelContactsPage(driver);
        siebelContactsPage.clickContactsQueryButton();
        siebelContactsPage.clickSiebelIdColumn();
        siebelContactsPage.inputSiebelId(siebelId);
        siebelContactsPage.clickContactsGoBtn();
        siebelContactsPage.clickLastName();
        siebelContactsPage.clickFinancialAccount();
        siebelContactsPage.clickFacilityQueryButton();
        siebelContactsPage.clickFacilityNumberCell();
        siebelContactsPage.inputFacilityNumber(facilityId);
        siebelContactsPage.clickFacilityGoButton();
        Assert.assertEquals("Open",siebelContactsPage.getFacilityStatus());
        Assert.assertEquals("Open",siebelContactsPage.getAccountStatus());

    }

    @Then("^I create a facility for creating a customer account$")
    public void i_create_a_facility_for_creating_a_customer_account() throws Throwable {
        pivotHomeWindow = new PivotHomeWindow(windowDriver);
        pivotHomeWindow.clickFileMenu();
        pivotHomeWindow.clickNewMenu();
        pivotHomeWindow.clickFacilityMenu();
        pivotFacilityWindow = new PivotFacilityWindow(windowDriver);
        pivotFacilityWindow.setReportingName("TEST NAME");
        pivotFacilityWindow.setExpiryDate("20/05/25");
        pivotFacilityWindow.clickLoanTab();
        pivotFacilityWindow.selectFundingOptions();
        pivotFacilityWindow.selectInterestRateOptions();
        pivotFacilityWindow.clickLimitsTab();
        pivotFacilityWindow.selectLoanPurposeGroup();
        pivotFacilityWindow.selectLoanPurposeCode();
        pivotFacilityWindow.saveFacility();
        facilityId = pivotFacilityWindow.getFacilityNumber();
    }

    @Then("^I verify account numbers in Siebel$")
    public void i_verify_account_numbers_in_Siebel() throws Throwable {
        String siebelAccNumber = siebelCompaniesPage.getAccountNumber();
        String additionalAccNum = siebelCompaniesPage.getAdditionalAccountNumber();
        Assert.assertEquals("Account Number in Pivot and Siebel does not match ---",accountNumber, siebelAccNumber);
        Assert.assertEquals("Additional Account Number in Pivot and Siebel does not match ---",accountNumber, additionalAccNum);
    }
}