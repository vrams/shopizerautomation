package com.macquarie.bfs.siebel;

import com.macquarie.bfs.siebel.commons.*;
import com.macquarie.bfs.siebel.commons.SiebelException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.winium.WiniumDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.management.ManagementFactory;

/**
 * Created by vikram on 16/01/17.
 */
public class Container {
    private static Logger logger = LoggerFactory.getLogger(Container.class);

    public String siebelId = "";
    private WebDriver driver;
    private WiniumDriver windowDriver;
    private String soapResponse = "";
    private String browser;
    @Before
    public void setUpWebDriver(Scenario scenario)
    {
        System.setProperty("environment","IA2");
        logger.info("Settting the webdriver......");
        /* Initialize the browser - Default will be used as chrome if the browser type is not passed as system variable*/
        driver = new DownloadDrivers().getDriver("ie");
    }

    @After
    public void killWebdriver(Scenario scenario)
    {
        // If the environment is null run the test cases in DEV environment
        String environment = System.getProperty("environment")!= null ? System.getProperty("environment") : "IA2";
        // If the scenario is failed capture the screenshot
        if (scenario.isFailed())
        {
            if(driver != null){
                try
                {
                    byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                    scenario.embed(screenshot, "image/png");
                } catch (WebDriverException wde) {
                    System.err.println(wde.getMessage());
                } catch (ClassCastException cce) {
                    cce.printStackTrace();
                }
            }

            // Kill the windowDriver instance when the test is failed
            quitDriver();
        }
        else {
            quitDriver();
        }
        logger.info("*** Finished Scenario: " + scenario.getName() + " ***");
    }
    /*
      Method for killing the WebDriver instance */
    private void quitDriver() {
        if (driver != null) {
            driver.quit();
            logger.info("Killed webdriver instance");
        }
    }

    /* Return web windowDriver instance */
    public WebDriver getDriver() {
        return this.driver;
    }

    public WiniumDriver getWindowDriver()
    {
        return this.windowDriver;
    }
}
